const mix = require('laravel-mix');
require('laravel-mix-svg-sprite');


mix
    .js('resources/js/app.js', 'js')
    .sass('resources/scss/app.scss', 'css')
    .svgSprite('resources/svg', 'sprite.svg')

  .version();


mix.babelConfig({
    presets: ['@babel/preset-env'],
});
