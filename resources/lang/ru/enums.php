<?php

use App\Enums\CoachClass;
use App\Enums\CoachType;
use App\Enums\CompetitionType;
use App\Enums\TimeUnitForPrice;

return [
    CoachClass::class => [
        CoachClass::KMS => 'КМС',
        CoachClass::MS => 'МС',
        CoachClass::MSMK => 'МСМК',
        CoachClass::Unknown => 'Неизвестен',
        CoachClass::ZMS => 'ЗМС'
    ],
    CoachType::class => [
        CoachType::Senior => 'Старший',
        CoachType::Ordinary => 'Обычный'
    ],
    TimeUnitForPrice::class => [
        TimeUnitForPrice::MONTH => 'в месяц',
        TimeUnitForPrice::LESSON => 'за занятие'
    ],
    CompetitionType::class => [
        CompetitionType::UNKNOWN => 'Не известно',
        CompetitionType::COMMERCIAL => 'Коммерческое',
        CompetitionType::RANKED => 'Разрядное'
    ]
];
