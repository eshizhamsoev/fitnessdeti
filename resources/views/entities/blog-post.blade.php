<?php
/**
 * @var \App\Models\Website\BlogPost $blogPost
 */
?>
<x-layouts.breadcrumbs :url="$blogPost->url"></x-layouts.breadcrumbs>
<div class="container typography blog-post">
    <div class="blog-post__image">
        @if($blogPost->hasMedia(\App\Models\Website\BlogPost::IMAGE_COLLECTION_MAIN))
            {{ $blogPost->getFirstMedia(\App\Models\Website\BlogPost::IMAGE_COLLECTION_MAIN)(\App\Models\Website\BlogPost::IMAGE_CONVERSION_MAIN) }}
        @endif
    </div>
    <h1 class="blog-post__heading">{{ $blogPost->name }}</h1>
    @markdown($blogPost->text)
</div>
