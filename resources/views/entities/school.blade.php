<x-layouts.breadcrumbs :url="$school->url"></x-layouts.breadcrumbs>
<x-content.school-info :school="$school" class="margin margin_top"/>
{{--@if($school->coaches->isNotEmpty())--}}
{{--    <x-content.school-coaches :school="$school" />--}}
{{--@endif--}}

<x-block.timetable :source="$source" class="wrapper"/>

@if($coaches)
    <x-content.department-coaches :coaches="$coaches" class="margin" />
@endif
@if($insideGallery)
    <x-common.gallery
        :galleryData="$insideGallery"
        class="margin"
    />
@endif

@if($blocks)
    <x-layouts.blocks :blocks="$blocks"/>
@endif
