<x-layouts.breadcrumbs :url="$coach->url"></x-layouts.breadcrumbs>
<x-content.trainer-profile :coach="$coach"></x-content.trainer-profile>
@if($kpi)
    <x-common.kpi class="desktop-hide">
        @foreach($kpi as $kpiItem)
            <x-common.kpi-item :data="$kpiItem" />
        @endforeach
    </x-common.kpi>
@endif
<x-content.coach-resume
    :coach="$coach"
    class="margin"
/>
@if($coach->schools->isNotEmpty())
    <x-content.coach-schools
        :coach="$coach"
        class="wrapper wrapper_white-grey"
    />
@endif
@if($coachWorkGallery)
    <x-common.gallery
        :galleryData="$coachWorkGallery"
        class="margin"
    />
@endif
@if($coach->students->isNotEmpty())
    <x-content.students
        :students="$coach->students->all()"
        class="margin"
    >
        <x-slot name="title">
            Ученики тренера
        </x-slot>
        <x-slot name="button">
            Стать воспитанником
        </x-slot>
    </x-content.students>
@endif
