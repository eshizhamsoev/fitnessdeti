@extends('layouts.page')
@section('title', $seoData->getMetaTitle())
@push('head')
    @if($seoData->getMetaDescription())
        <meta name="description" content="{{ $seoData->getMetaDescription() }}"/>
    @endif
    @if($seoData->getMetaKeywords())
        <meta name="keywords" content="{{ $seoData->getMetaKeywords() }}"/>
    @endif
    @if($seoData->getOgTitle())
        <meta property="og:title" content="{{ $seoData->getOgTitle() }}"/>
    @endif
    @if($seoData->getOgImage())
        <meta property="og:image" content="{{ $seoData->getOgImage() }}"/>
    @endif
    @if($seoData->getOgDescription())
        <meta property="og:description" content="{{ $seoData->getOgDescription() }}"/>
    @endif
    @if($seoData->getOgUrl())
        <meta property="og:url" content="{{ $seoData->getOgUrl() }}"/>
    @endif
    @if($seoData->getCanonical())
        <link rel="canonical" href="{{ $seoData->getCanonical() }}" />
    @endif
@endpush
@section('html_class', 'page')
@section('page-content')
{!! $content !!}
@endsection
