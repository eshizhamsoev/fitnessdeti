@extends('layouts.base')
@section('html_class', 'page')
@section('content')
    <div>
        <x-layouts.header/>
        <main>
            @yield('page-content')
        </main>
        <x-layouts.footer/>
    </div>
    <button class='btn btn_type_primary up'>
        <x-base.svg-icon width='10' height='17' :icon="\App\Enums\Website\SvgIconName::ARROW()"/>
    </button>
    <x-layouts.mobile-menu/>
@endsection
