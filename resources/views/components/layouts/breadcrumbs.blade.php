<div class='bread-crumbs'>
    <div class='container'>
        <ul class='ul-clear breadcrumbs__inner'>
            @foreach($links as $link)
                <li class='breadcrumbs__item'>
                    <a href='{{ $link->getLink() }}' class='a-clear'>{{ $link->getName() }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
