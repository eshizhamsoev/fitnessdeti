<header class='header'>
    <div class='header__top'>
        <div class='header__container container'>
            <x-base.link href='/' class='header__logo'>
                <picture>
                    <source media='(max-width: 599px)' srcset='{{ asset('static/images/common/logo-un-small.svg') }}'/>
                    <source media='(min-width: 600px)' srcset='{{ asset('static/images/common/logo-un.svg') }}'/>
                    <img width='172' height='50' loading="lazy" src='{{ asset('static/images/common/logo-un.svg') }}'
                         alt='ФД - Всероссийская сеть детских спортивных школ по художественной гимнастике и спортивной акробатике'/>
                </picture>
            </x-base.link>
            <div class='header__region'>
                <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::NAVIGATION()" class='header__region-icon'
                                 width='18' height='18'/>
                <button class='header__region-city a-clear'
                        type="button"
                        data-fancybox
                        data-src='#fancy-cities'
                >{{ $cityName }}</button>
            </div>
            <div class='header__socials'>
                @foreach($contact_links as $link)
                    @if($link['link'])
                        <a href='{{ $link['link'] }}' target='_blank' class='header__social a-clear'>
                            <x-base.svg-icon width='18' height='18' :icon="$link['icon']"/>
                        </a>
                    @endif
                @endforeach
            </div>
            @if($phone)
                <div class='header__tel'>
                    <x-base.phone-link :phone="$phone" class="a-clear"/>
                </div>
            @endif
            <div class='header__button'>
                <x-common.form-modal-trigger
                    class="btn btn_type_accent header__button"
                    source="Запись в шапке"
                >
                    Позвоните мне
                </x-common.form-modal-trigger>
            </div>
        </div>
    </div>
    <div class='header__bottom'>
        <div class='navigation'>
            <div class='navigation__container container'>
                <nav>
                    <ul class='navigation__menu ul-clear'>
                        @foreach($links as $link)
                            <li class='navigation__menu-item'>
                                <x-base.link :href='$link["link"]'>
                                    {{ $link['name'] }}
                                </x-base.link>
                                @isset($link['children'])
                                    <ul class="navigation__submenu  ul-clear">
                                        @foreach($link['children'] as $child)
                                            <li class="navigation__submenu-item">
                                                <x-base.link :href="$child['link']">
                                                    {{ $child['name'] }}
                                                </x-base.link>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endisset
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

@push('body_end')
    <div id='fancy-cities' class='fancy-cities' hidden>
        <div class='fancy-cities__inner'>
            @foreach($cities as $city)
                <a href='//{{ $city->domain }}' class='fancy-cities__item a-clear'>
                    @if($city->icon)
                        <svg width="44" height="44">
                            <use xlink:href='/city-sprite.svg#{{ $city->icon }}'></use>
                        </svg>
                    @endif
                    {{ $city->name }}</a>
            @endforeach
        </div>
    </div>
@endpush
