<div class="bottom-bar fixed">
    <div class="bottom-bar__container container">
        <x-base.link href="/school-addresses" class="bottom-bar__item a-clear">
            <x-base.svg-icon class="bottom-bar__icon" width='22' height='22'
                             :icon="\App\Enums\Website\SvgIconName::PLACEHOLDER()"/>
            <div class="bottom-bar__title">Адреса</div>
        </x-base.link>
        <x-base.link href="/sales" class="bottom-bar__item a-clear">
            <x-base.svg-icon class="bottom-bar__icon" width='22' height='22'
                             :icon="\App\Enums\Website\SvgIconName::RIBBON()"/>
            <div class="bottom-bar__title">Акции</div>
        </x-base.link>
        <div class="bottom-bar__item bottom-bar__menu a-clear">
            <div class="bottom-bar__menu-inner">
                <a href="#my-menu" class="bottom-bar__menu-btn btn a-clear">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <div class="bottom-bar__title">Меню</div>
            </div>
        </div>
        <x-base.phone-link :phone="$phone" class="bottom-bar__item a-clear">
            <x-base.svg-icon class="bottom-bar__icon" width='22' height='22'
                             :icon="\App\Enums\Website\SvgIconName::TELEPHONE()"/>
            <div class="bottom-bar__title">Позвонить</div>
        </x-base.phone-link>
        <x-common.form-modal-trigger class="bottom-bar__item a-clear" source="Запись мобильный футер">
            <x-base.svg-icon class="bottom-bar__icon" width='22' height='22'
                             :icon="\App\Enums\Website\SvgIconName::EDIT()"/>
            <div class="bottom-bar__title">Запись</div>
        </x-common.form-modal-trigger>
    </div>
</div>

<nav id='my-menu' hidden>
    <ul>
        @foreach($links as $link)
            <li>
                <x-base.link :href="$link['link']">{{ $link['name'] }}</x-base.link>
                @if($link['children'] ?? false)
                    <ul>
                        @foreach($link['children'] as $child)
                            <li>
                                <x-base.link :href="$child['link']">{{ $child['name'] }}</x-base.link>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
</nav>

<template data-mmenu>
    <div>
        <div class='mmenu'>
            <x-base.link href="/" class='a-clear mmenu__image'>
                <img loading="lazy" width='280' height='60' src='{{ asset('static/images/common/logo-full.svg') }}'
                     alt=''>
            </x-base.link>
            @if($phone)
                <x-base.phone-link :phone="$phone" class='mmenu__tel'/>
            @endif
            <div class='mmenu__socials'>
                @foreach($socials as $social)
                    @if($social['link'])
                        <a href='{{ $social['link'] }}' class='a-clear mmenu__socials-item'>
                            <x-base.svg-icon width='20' height='20' :icon="$social['icon']"/>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</template>
