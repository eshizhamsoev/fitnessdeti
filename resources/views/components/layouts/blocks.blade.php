@foreach($blocks as $block)
    @php($prev = !$loop->first ? $blocks[$loop->index - 1] : null)
    @php($next = !$loop->last ? $blocks[$loop->index + 1] : null)

    @if($block['is_grey'] && !($prev && $prev['is_grey']))
        <div class="grey-wrapper">
            @endif
            <x-dynamic-component :component="$block['type']" :data="$block['data']"
                                 class="margin"/>

            @if($block['is_grey'] && !($next && $next['is_grey']))
        </div>
    @endif
@endforeach
