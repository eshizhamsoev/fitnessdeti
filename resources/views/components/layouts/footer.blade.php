<footer class='footer'>
    <div class='footer__up'>
        <div class='footer__up-container container'>
            <div class='footer__column footer__fitness'>
                <x-base.link href='/' class='footer__logo'>
                    <picture>
                        <source media='(max-width: 599px)' srcset='{{ asset('static/images/common/logo-un-small.svg') }}'/>
                        <source media='(min-width: 600px)' srcset='{{ asset('static/images/common/logo-un.svg') }}'/>
                        <img width='172' height='50' loading="lazy" src='{{ asset('static/images/common/logo-un.svg') }}'
                             alt='ФД - Всероссийская сеть детских спортивных школ по художественной гимнастике и спортивной акробатике'/>
                    </picture>
                </x-base.link>
                @if($schools_page_url)
                    <div>
                        <x-base.link :href='$schools_page_url' class='footer__address'>
                            <span class="footer__icon">
                                <x-base.svg-icon width='17' height='17' :icon="\App\Enums\Website\SvgIconName::PIN()"/>
                            </span>

                            <span class="footer__icon footer__icon_navigation">
                                <x-base.svg-icon width='14' height='14'
                                                 :icon="\App\Enums\Website\SvgIconName::NAVIGATION()"/>
                            </span>
                            <span class='footer__address-dotted'>Адреса школ</span>
                        </x-base.link>
                    </div>
                @endif
                @if($phone)
                    <div class='footer__telephone'>
                        <x-base.phone-link :phone='$phone' class='a-clear footer__icon'>
                            <x-base.svg-icon width='17' height='17'
                                             :icon="\App\Enums\Website\SvgIconName::TELEPHONE()"/>
                        </x-base.phone-link>
                        <x-base.phone-link class='a-clear' :phone="$phone"/>
                    </div>
                @endif
                @if($email)
                    <div class='footer__email'>
                        <a href='mailto:{{ $email }}' class='a-clear footer__icon'>
                            <x-base.svg-icon width='17' height='17' :icon="\App\Enums\Website\SvgIconName::EMAIL()"/>
                        </a>
                        <a href='mailto:{{ $email }}' class='a-clear'>{{ $email }}</a>
                    </div>
                @endif
                <div class='footer__socials'>
                    @foreach($socials as $social)
                        @if($social['link'])
                            <a href='{{ $social['link'] }}' aria-label="{{ $social['name'] }}"
                               target="_blank"
                               rel="nofollow"
                               class='footer__social a-clear'>
                                <x-base.svg-icon width='21' height='21' :icon="$social['icon']"/>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class='footer__column footer__services'>
                <div class='footer__title js-footer__title'>
                    <x-base.link :href="$menu['services']['link']">{{ $menu['services']['name'] }}</x-base.link>
                    <x-base.svg-icon width='7' height='12' :icon="\App\Enums\Website\SvgIconName::ARROW()"
                                     class='footer__title-icon'/>
                </div>
                <nav class='footer__submenu'>
                    <ul class='footer__content footer__services-content ul-clear'>
                        @foreach($menu['services']['children'] as $item)
                            <li class='footer__item'>
                                <x-base.link :href="$item['link']">{{ $item['name'] }}</x-base.link>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class='footer__column'>
                <div class='footer__title js-footer__title'>
                    <x-base.link :href="$menu['about']['link']">{{ $menu['about']['name'] }}</x-base.link>
                    <x-base.svg-icon width='7' height='12' :icon="\App\Enums\Website\SvgIconName::ARROW()"
                                     class='footer__title-icon'/>
                </div>
                <nav class='footer__submenu'>
                    <ul class='footer__content ul-clear'>
                        @foreach($menu['about']['children'] as $item)
                            <li class='footer__item'>
                                <x-base.link :href="$item['link']">{{ $item['name'] }}</x-base.link>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class='footer__column'>
                <div class='footer__title js-footer__title'>
                    <x-base.link :href="$menu['events']['link']">{{ $menu['events']['name'] }}</x-base.link>
                    <x-base.svg-icon width='7' height='12' :icon="\App\Enums\Website\SvgIconName::ARROW()"
                                     class='footer__title-icon'/>
                </div>
                <nav class='footer__submenu'>
                    <ul class='footer__content ul-clear'>
                        @foreach($menu['events']['children'] as $item)
                            <li class='footer__item'>
                                <x-base.link :href="$item['link']">{{ $item['name'] }}</x-base.link>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class='footer__down'>
        <div class='footer__down-container container'>
            <div class='footer__down-company'>Фитнес Дети, 2013 - {{ date('Y') }} ©</div>
            @if($privacy_page_url)
                <x-base.link :href='$privacy_page_url' class='footer__down-policy'>
                    Политика конфиденциальности
                </x-base.link>
            @endif
            @if($safety_rules_page_url)
                <x-base.link :href='$safety_rules_page_url' class='footer__down-policy'>
                    Правила безопасности
                </x-base.link>
            @endif
            @if($offer_and_acceptance_url)
                <x-base.link href='https://cdn.fitnessdeti.ru/fd/docs/dogovor-ip-2022.pdf' class='footer__down-policy'>
                    Договор Москва
                </x-base.link>
            @endif
            @if($llc)
                <x-base.link href='https://cdn.fitnessdeti.ru/fd/docs/dogovor-ooo-2022.pdf' class='footer__down-policy'>
                    Договор Видное и регионы
                </x-base.link>
            @endif
            @if($user_agreement)
                <x-base.link :href='$user_agreement' class='footer__down-policy'>
                    Пользовательское соглашение
                </x-base.link>
            @endif
        </div>
    </div>
</footer>
