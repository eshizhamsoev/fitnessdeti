<div {{ $attributes->merge([
    'class' => 'fieldset'
]) }}>
    @if($title)
        <div class='fieldset__title'>{{ $title }}</div>
    @endif
    <div class='fieldset__items {{ $column ?? 'fieldset__items_column' }}'>
        {{ $slot }}
    </div>
</div>



