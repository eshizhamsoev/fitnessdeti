<button {{ $attributes }} data-fancybox data-src="{{ $link }}">
    {{ $slot }}
</button>
