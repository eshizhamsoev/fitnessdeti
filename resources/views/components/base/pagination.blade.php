@if ($paginator->hasPages())
    <nav class='pagination'>
        <ul class="pagination__pages ul-clear">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="btn pagination__item pagination__item_type_prev pagination__item_current">
                        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width='10' height='16' />
                    </span>
                </li>
            @else
                <li>
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')" class='btn pagination__item pagination__item_type_prev'>
                        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width='10' height='16' />
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span class="btn pagination__item pagination__item_type_dots">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page"><span class="btn pagination__item pagination__item_type_page pagination__item_current">{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}" class="btn pagination__item pagination__item_type_page">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a class="btn pagination__item pagination__item_type_next" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width='10' height='16' />
                    </a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="btn pagination__item pagination__item_type_next pagination__item_current" aria-hidden="true">
                        <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::ARROW()" width='10' height='16' />
                    </span>
                </li>
            @endif
        </ul>
    </nav>
@endif
