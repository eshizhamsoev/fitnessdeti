<a href='tel:{{ $phoneNumber }}' {{ $attributes }}>{{ $slot->isNotEmpty() ? $slot : $phoneFormatted }}</a>
