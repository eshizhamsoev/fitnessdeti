@if($isCurrent)
    <span {{ $attributes->merge(['class' => $currentLinkClass]) }}>
        {{ $slot }}
    </span>
@else
    <a {{ $attributes->merge(['class' => 'a-clear'])  }} target="{{$inNewTab ? '_blank' : ''}}" href="{{ $href }}">
        {{ $slot }}
    </a>
@endif
