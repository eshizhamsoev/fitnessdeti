<label {{ $attributes->only(['class'])->merge([
    'class' => 'form-input'
]) }}>
    <input
        {{ $attributes->except(['class']) }}
        {{ $isChecked ? 'checked' : '' }}
        {{ $isDisabled ? 'disabled' : '' }}
        name='{{ $name}}'
        value='{{ $value}}'
        type='{{ $type->value }}'
    >
    @if($slot && $slot->toHtml())
        <span>{{ $slot }}</span>
    @else
        <span>{{ $label }}</span>
    @endif
</label>
