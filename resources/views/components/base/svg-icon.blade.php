<svg width="{{ $width }}" height="{{ $height }}" {{ $attributes }}>
    <use xlink:href='{{ $href }}'></use>
</svg>
