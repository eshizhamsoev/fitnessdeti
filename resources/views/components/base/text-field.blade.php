<div {{ $attributes->only(['class'])->merge([
    'class' => 'form-text'
]) }}>
    <input
        {{ $attributes->except(['class']) }}
        {{ $isDisabled ? 'disabled' : '' }}
        name='{{ $name}}'
        value='{{ $value}}'
        type='{{ $type->value }}'
        placeholder='{{ $placeholder }}'
        aria-label="{{ $label }}"
    >
</div>
