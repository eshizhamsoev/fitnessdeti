<div {{ $attributes->merge(['class' => 'questions']) }}>
    <div class="questions__container container typography">
        <h2 class="questions__title">Вопросы и ответы</h2>
        <div class="questions__inner">
            <div class="questions__column">
                @foreach($questions as $question)
                    <details class="questions__details">
                        <summary class='questions__summary'>
                            {{$question->question}}
                        </summary>
                        {{$question->answer}}
                    </details>
                @endforeach
            </div>
            <div class="questions__image"><img width="451" height="554" src="{{asset('static/images/components/blocks/faq/questions-image.png')}}" alt="" /></div>
        </div>
    </div>
</div>
