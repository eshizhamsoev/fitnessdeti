<div {{ $attributes->merge(['class' => 'moneybox-medals']) }}>
    <div class="moneybox-medals__container container typography">
        <h2 class="moneybox-medals__title">Копилка медалей FD</h2>
        <div class="moneybox-medals__awards">
            <div class='moneybox-medals__award-inner'>
                <div class='moneybox-medals__award'>
                    <div class='moneybox-medals__award-cup'>
                        <img width='150' height='317' loading="lazy"
                             src='{{ asset('/static/images/components/blocks/medals/silver.png') }}'
                             alt=''/>
                    </div>
                    <div class='moneybox-medals__award-bottom moneybox-medals__award-bottom_silver'>
                        <div class='moneybox-medals__award-amount'>{{ $data->getSilver() }}</div>
                        <div class='moneybox-medals__award-title'>Серебра</div>
                    </div>
                </div>
                <div class='moneybox-medals__award'>
                    <div class='moneybox-medals__award-cup'>
                        <img width='150' height='376' loading="lazy"
                             src='{{ asset('/static/images/components/blocks/medals/gold.png') }}'
                             alt=''/></div>
                    <div class='moneybox-medals__award-bottom moneybox-medals__award-bottom_gold'>
                        <div class='moneybox-medals__award-amount'>{{ $data->getGold() }}</div>
                        <div class='moneybox-medals__award-title'>Золота</div>
                    </div>
                </div>
                <div class='moneybox-medals__award'>
                    <div class='moneybox-medals__award-cup'><img width='150' height='278' loading="lazy"
                                                                 src='{{ asset('/static/images/components/blocks/medals/bronze.png') }}'
                                                                 alt=''/></div>
                    <div class='moneybox-medals__award-bottom moneybox-medals__award-bottom_bronze'>
                        <div class='moneybox-medals__award-amount'>{{ $data->getBronze() }}</div>
                        <div class='moneybox-medals__award-title'>Бронзы</div>
                    </div>
                </div>
            </div>
            <div class="moneybox-medals__image">
                <img loading="lazy" width="688" height="445"
                     src="{{ asset('/static/images/components/blocks/medals/girl.png') }}" alt=""/>
            </div>
        </div>
        @if($data->getButtonText())
            <div class='moneybox-medals__btn-wrapper'>
                <x-common.form-modal-trigger
                    class='moneybox-medals__btn btn btn_type_accent'
                    source="Хочу медали">{{ $data->getButtonText() }}</x-common.form-modal-trigger>
            </div>
        @endif
    </div>
</div>
