<div class="banner__wrapper" data-swiper-type='main'>
    <div class='banner-gymnastic__slider swiper'>
        <div class='banner-gymnastic__wrapper swiper-wrapper'>
            @foreach($gallery->images as $image)
                <x-content.main-banner-slide
                    :image="$image"
                >
                </x-content.main-banner-slide>
            @endforeach
            <div class="banner swiper-slide">
                <div class="banner__bg">
                    <img width="1920" height="500" loading="lazy"
                         src="{{asset('static/images/components/blocks/main-banner/banner.jpg')}}" alt="">
                </div>
                <div class="banner__container container typography">
                    <div class="banner__content">
                        <h1 class="banner__title">Всероссийская сеть детских спортивных школ FD</h1>
                        <div class="banner__text"></div>
                        <x-common.form-modal-trigger
                            class='btn btn_type_accent'
                            source="Запись на пробное занятие в шапке"
                            heading="Запись на пробное занятие"
                            leading="Оставьте заявку и запишитесь на пробное занятие"
                        >
                            Пробное занятие
                        </x-common.form-modal-trigger>
                    </div>
                    <div class="banner__watch">
                        <div class="watch-btn">
                            <div class="watch-btn__circle watch-btn__circle_first">
                                <div class="watch-btn__circle watch-btn__circle_second">
                                    <div class="watch-btn__circle watch-btn__circle_third"></div>
                                </div>
                            </div>
                            <x-base.youtube-link class='watch-btn__play' code="8ExCb8rQ5jI">
                                <x-base.svg-icon width="27" height="30" :icon="$playIcon"/>
                            </x-base.youtube-link>
                        </div>
                    </div>
                    <div class="banner__icon">
                        <x-base.svg-icon width="175" height="115" :icon="$watchUpIcon"/>
                    </div>
                </div>
            </div>
        </div>
        <div class='swiper-pagination'></div>
    </div>
    <button class='btn banner__arrow-button banner__arrow-button_left carousel-arrow_prev'>
        <x-base.svg-icon width="10" height="16" :icon="$arrowIcon"/>
    </button>
    <button class='btn banner__arrow-button banner__arrow-button_right carousel-arrow_next'>
        <x-base.svg-icon width="10" height="16" :icon="$arrowIcon"/>
    </button>
</div>
