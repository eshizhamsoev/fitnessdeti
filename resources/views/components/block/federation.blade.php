<div {{ $attributes->merge(['class' => 'part-federations']) }}>
    <div class='container typography'>
        <h2 class='part-federations__title'>Входим в состав федераций</h2>
        <div class='part-federations__inner'>
            <div class='part-federations__item'>
                <div class='part-federations__image'>
                    <img loading="lazy" width='173' height='174' src='{{ asset('static/images/components/blocks/federation/federation-gymnastic.png') }}' alt=''/>
                </div>
                <div class='part-federations__content'>
                    <div class='part-federations__item-title'>{{ $federationData->getGymnasticName() }}</div>
                    <div class='part-federations__text'>
                        {{ $federationData->getGymnasticText() }}
                    </div>
                </div>
            </div>
            <div class='part-federations__item'>
                <div class='part-federations__image'>
                    <img loading="lazy" width='173' height='174' src='{{ asset('static/images/components/blocks/federation/federation-acrobatic.png') }}' alt=''/>
                </div>
                <div class='part-federations__content'>
                    <div class='part-federations__item-title'>{{ $federationData->getAcrobaticName() }}</div>
                    <div class='part-federations__text'>
                        {{ $federationData->getAcrobaticText() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
