<div class='reviews-block'>
    <div class='container typography'>
        <div class='reviews-block__title'>
            <h1>Отзывы о нашей школе</h1>
            <div class='reviews-block__buttons'>
                <x-common.tabs color="white" :tabs="$tabs" />
            </div>
        </div>

        <div class='reviews-block__inner'>
            @foreach($reviews as $review)

                <x-common.reviews-block-item :review="$review" />

            @endforeach
        </div>
{{--        <div class='reviews-block__btn'><button class='btn btn_type_secondary'>Показать еще</button></div>--}}
        {!! $links !!}
    </div>
</div>
