<div {{ $attributes->class(['class' => 'timetable']) }}>
    <div class="timetable__container container typography">
        <div class="timetable__title h2">Расписание занятий</div>
        <div class="timetable__timetable-inner">
            <div class="timetable__content">
                <div class="timetable__row">
                    <div class="timetable__row-text">
                        <div class="timetable__row-title">Удобное расписание</div>
                        <div class="timetable__row-subtitle">Вечерние или утренние группы</div>
                    </div>
                    <div class="timetable__row-icon">
                        <x-base.svg-icon width="30" height="30" :icon="\App\Enums\Website\SvgIconName::CALENDAR()"/>
                    </div>
                </div>
                <div class="timetable__row">
                    <div class="timetable__row-text">
                        <div class="timetable__row-title">Различные уровни групп</div>
                        <div class="timetable__row-subtitle">Занятия от 2 до 5 раз в неделю</div>
                    </div>
                    <div class="timetable__row-icon">
                        <x-base.svg-icon width="30" height="30"
                                         :icon="\App\Enums\Website\SvgIconName::GRAPH()"/>
                    </div>
                </div>
                <div class="timetable__row">
                    <div class="timetable__row-text">
                        <div class="timetable__row-title">Группы комфортной численности</div>
                        <div class="timetable__row-subtitle">Группы до 12 детей</div>
                    </div>
                    <div class="timetable__row-icon">
                        <x-base.svg-icon width="30" height="30" :icon="\App\Enums\Website\SvgIconName::GROUP()"/>
                    </div>
                </div>
            </div>
            <div class="timetable__image">
                <img width='688' height='375' loading="lazy"
                     src="{{ asset('static/images/components/common/timetable/classes-image.png') }}" alt=""/>
                <x-common.form-modal-trigger
                    class="timetable__image-btn btn btn_type_accent"
                    :source="$source"
                    heading="Узнать расписание"
                    leading="Оставьте заявку и мы с вами свяжемся"
                >
                    Узнать подробное расписание
                </x-common.form-modal-trigger>
            </div>
        </div>
        <x-common.form-modal-trigger
            class="timetable__btn btn btn_type_accent"
            :source="$source"
            heading="Узнать расписание"
            leading="Оставьте заявку и мы с вами свяжемся"
        >
            Узнать подробное расписание
        </x-common.form-modal-trigger>
    </div>
</div>
