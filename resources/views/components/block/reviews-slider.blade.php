<div {{ $attributes->merge(['class' => 'reviews']) }}>
    <div class='reviews__container container typography'>
        <h2 class='reviews__title'>{{ $header }}</h2>
        <div class='reviews__all'><x-base.link :href="$allReviewsLink" class='btn btn_type_secondary'>Посмотреть все отзывы</x-base.link></div>
        <x-common.carousel class="reviews__slider-container" slider-name="reviews">
            <x-slot name="slides">
                @foreach($reviews as $review)
                    <div class="reviews__slider-item swiper-slide">
                        @if($review->hasMedia(\App\Models\Data\Review::IMAGE_COLLECTION_AVATAR))
                            <div class="reviews__avatar">
                                {{ $review->getFirstMedia(\App\Models\Data\Review::IMAGE_COLLECTION_AVATAR)(\App\Models\Data\Review::IMAGE_CONVERSION_AVATAR) }}
                            </div>
                        @endif
                        <div class="reviews__content">
                            <div class="reviews__name">{{$review->name}}</div>
                            <div class="reviews__activity">{{$review->discipline->name}}</div>
                            <div class="reviews__text">
                                {{$review->text}}
                            </div>
                            <button class="btn reviews__more active">Читать далее</button>
                            <button class="btn reviews__more">Скрыть</button>
                        </div>
                    </div>
                @endforeach
            </x-slot>
        </x-common.carousel>


        @if($buttonText)
            <div class='reviews__like-wrapper'>
                <x-common.form-modal-trigger class='btn btn_type_accent' source="Отзыв">
                    {{$buttonText}}
                </x-common.form-modal-trigger>
            </div>
        @endif
    </div>
</div>
