<div {{$attributes->merge(['class' => 'classes'])}}>
    <div class="classes__container container typography">
        <h2 class="classes__title">Как проходит занятие</h2>
        <div class='classes__slider-container' data-swiper-type="classes">
            <div class="classes__slider swiper" >
                <div class="classes__wrapper swiper-wrapper">
                    @foreach($images as $index => $image)
                        <x-common.classes-item :image="$image" :count="$index" />
                    @endforeach
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <button class='btn carousel-arrow {{ $prevClass ?? 'carousel-arrow_prev' }}'>
                <x-base.svg-icon width="10" height="16" :icon="$arrowIcon" />
            </button>
            <button class='btn carousel-arrow {{ $nextClass ?? 'carousel-arrow_next' }}'>
                <x-base.svg-icon width="10" height="16" :icon="$arrowIcon" />
            </button>
        </div>
    </div>
</div>
