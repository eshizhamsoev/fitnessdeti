<div {{ $attributes->merge(['class' => 'trainers js-trainers']) }}>
    <div class='trainers__container container typography'>
        <h2 class='trainers__title'>Тренеры</h2>

        <label class='trainers__search search'>
            <input class='js-trainers-input' type='text' placeholder='Введите имя тренера'>
            <button class='btn btn_type_accent '>Поиск</button>
        </label>
        <div class='trainers__filter'>
            @foreach($disciplines as $discipline)
                <div class='trainers__filter-item'>
                    <x-base.checking-input-field
                        name="filter"
                        :type="\App\Enums\Website\Forms\CheckingFieldType::CHECKBOX()"
                        :value="$discipline->id"
                    >
                        <x-slot name="label">
                            {{ $discipline->name }}
                        </x-slot>
                    </x-base.checking-input-field>
                </div>
            @endforeach
        </div>
    </div>
    <div class='trainers__catalog'>
        <div class='catalog'>
            <div class='container typography'>
                <div class='catalog__inner'>
                    @foreach($coaches as $coach)
                        <x-content.coach-small-card :coach="$coach" :data-name="$coach->name"
                                                    :data-discipline="$coach->discipline->id"/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
