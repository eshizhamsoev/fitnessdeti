<div {{ $attributes->merge(['class' => 'department-trainers js-department-trainers']) }}>
    <div class='container typography'>
        <h2 class='department-trainers__title'>Сертифицированные тренеры</h2>
        <x-common.coaches-carousel :coaches="$coaches" class='department-trainers__inner' />
        <div class='department-trainers__btn-group'>
            <x-common.form-modal-trigger class='department-trainers__btn btn btn_type_accent' source="Сертифицированные тренеры">
                Записаться на занятие
            </x-common.form-modal-trigger>
        </div>
    </div>
</div>
