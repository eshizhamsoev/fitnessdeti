<div {{$attributes->merge(['class' => 'additional-service'])->except(['images', 'header']) }}>
    <div class='additional-service__container container typography'>
        <h2 class='additional-service__title'>Дополнительные услуги</h2>
        <x-common.carousel sliderName="general" :hasShadow="true">
            <x-slot name="slides">
                @foreach($images as $image)
                    <x-common.additional-service-slide :image="$image" />
                @endforeach
            </x-slot>
        </x-common.carousel>
    </div>
</div>
