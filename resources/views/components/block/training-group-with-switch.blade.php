<div {{ $attributes->merge(['class' =>  "teaching-groups"]) }} >
    <div class="teaching-groups__container container typography">
        <h2 class="teaching-groups__title">{{ $heading }}</h2>
        <div class="teaching-groups__subtitle">{{ $leading }}</div>

        <input class="teaching-groups__switch-input teaching-groups__switch-input_left" type='radio'
               name="training_switch"
               id='training-left' checked>
        <input class="teaching-groups__switch-input teaching-groups__switch-input_right" type='radio'
               name="training_switch"
               id='training-right'>
        <div class='teaching-groups__switch'>
            <label for='training-left'
                   class='teaching-groups__switch-label teaching-groups__switch-label_left'>{{ $left_name }}</label>
            <span class="teaching-groups__switch-slider js-switch-slider" data-for="training_switch"></span>
            <label for='training-right'
                   class='teaching-groups__switch-label teaching-groups__switch-label_right'>{{ $right_name }}</label>
        </div>


        <div class="teaching-groups__inner teaching-groups__inner_type_left">
            @foreach($left_groups as $group)
                <x-content.teaching-group-item :group="$group">
                    @if($loop->index === 1)
                        <x-slot name="label">
                            Хит
                        </x-slot>
                    @endif
                </x-content.teaching-group-item>
            @endforeach
        </div>

        <div class="teaching-groups__inner teaching-groups__inner_type_right">
            @foreach($right_groups as $group)
                <x-content.teaching-group-item :group="$group">
                    @if($loop->index === 1)
                        <x-slot name="label">
                            Хит
                        </x-slot>
                    @endif
                </x-content.teaching-group-item>
            @endforeach
        </div>
    </div>
</div>
