<div {{ $attributes->merge(['class' => 'block-three-card'])->except(['images', 'header']) }}>
    <div class='block-three-card__container container typography'>
        @foreach($images as $image)
            <x-common.card :image="$image"></x-common.card>
        @endforeach
    </div>
</div>
