<div {{ $attributes->merge(['class' => 'senior-trainers__container container typography']) }}>
    <div class="senior-trainers__title senior-trainers__title_second">
        <h2>Требования к нашим тренерам</h2>
    </div>
    <x-common.coaches-properties-list class="senior-trainers__inner" />

</div>
