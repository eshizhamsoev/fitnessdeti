<div {{ $attributes->merge(['class' => 'coaching-staff']) }}>
    <div class='container typography'>
        <h2>Тренерский состав</h2>
        <x-common.coaches-carousel :coaches="$coaches" class='coaching-staff__slider'/>
        <x-common.coaches-properties-list class='coaching-staff__properties' />
        <x-common.form-modal-trigger class='btn btn_type_accent coaching-staff__btn' source="Тренерский состав">
            Записать на персональную тренировку
        </x-common.form-modal-trigger>
    </div>
</div>
