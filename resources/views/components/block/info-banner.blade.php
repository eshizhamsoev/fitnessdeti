@if ($galleryItems->isNotEmpty())
    <div {{ $attributes->merge(['class' => 'sales-blocks']) }} >
        @foreach($galleryItems as $galleryItem)
            <div class="{{'banner-info banner-info_' . $side . ' ' . $galleryItem->getHiddenClass() }}">
                <div class='banner-info__container container'>
                    <picture class='banner-info__image'>
                        <source media='(max-width: 1024px)'
                                srcset={{ $galleryItem->getMobileImageUrl() }} />
                        <source media='(min-width: 1025px)'
                                srcset={{ $galleryItem->getDesktopImageUrl() }} />
                        <img width='1200' height='370' loading="lazy"
                             src={{ $galleryItem->getDesktopImageUrl() }} alt=''/>
                    </picture>
                    <div class='banner-info__content typography'>
                        <div class='h2 banner-info__title'>{{ $galleryItem->getName() }}</div>
                        <div class='banner-info__text'>
                            @markdown($galleryItem->getDescription())
                        </div>

                        @if ($galleryItem->getLink())

                            <x-base.link :href="$galleryItem->getLink()"
                                         class='a-clear btn {{$buttonType}} banner-info__btn'>{{ $buttonText }}</x-base.link>
                        @elseif ($formSource)
                            <x-common.form-modal-trigger
                                class='btn {{$buttonType}} banner-info__btn'
                                :source="$formSource"
                                :heading="$formHeading ?? ''"
                                :leading="$formLeading ?? ''"
                            >
                                {{$buttonText}}
                            </x-common.form-modal-trigger>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif
