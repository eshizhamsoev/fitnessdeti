<?php
/**
 * @var \App\Models\Data\TrainingGroup[] $training_groups
 */
?>
<div {{ $attributes->merge(['class' => 'teaching-groups']) }}>
    <div class="teaching-groups__container container typography">
        <h2 class="teaching-groups__title">
            {{ $heading }}
        </h2>
        <div class="teaching-groups__subtitle">
            {{ $leading }}
        </div>
        <div class="teaching-groups__inner">
            @foreach($training_groups as $group)
                <x-content.teaching-group-item :group="$group" />
            @endforeach
        </div>
    </div>
</div>
