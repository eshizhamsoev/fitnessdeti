<div {{ $attributes->merge(['class' => 'calendar']) }}>
    <div class='calendar__container container typography'>
        <h2 class='calendar__title'>
            Календарь соревнований
            <button class='btn carousel-arrow calendar__prev'>
                <x-base.svg-icon width="10" height="16" :icon="\App\Enums\Website\SvgIconName::ARROW()"/>
            </button>
            <button class='btn carousel-arrow calendar__next'>
                <x-base.svg-icon width="10" height="16" :icon="\App\Enums\Website\SvgIconName::ARROW()"/>
            </button>
        </h2>
        <div class='calendar__inner' data-source-name="{{ $jsVariableName }}">
            <div class=' calendar__full calendar__full_first'>
                <div class='calendar__first-inner'></div>
            </div>
            <div class=' calendar__full calendar__full_second'>
                <div class='calendar__second-inner'></div>
            </div>
            <div class=' calendar__full calendar__full_third'>
                <div class='calendar__third-inner'></div>
            </div>
            <button class='btn carousel-arrow calendar__prev'>
                <x-base.svg-icon width="10" height="16" :icon="\App\Enums\Website\SvgIconName::ARROW()"/>
            </button>
            <button class='btn carousel-arrow calendar__next'>
                <x-base.svg-icon width="10" height="16" :icon="\App\Enums\Website\SvgIconName::ARROW()"/>
            </button>
        </div>
    </div>
</div>

@push('body_end_before_script')
    <script>
        window['{{ $jsVariableName }}'] = @json($events);
    </script>
@endpush
