<div {{ $attributes->merge(['class' => 'about-company']) }}>
    <div class='container typography'>
        <h2 class='about-company__title'>О компании</h2>
        <div class='about-company__inner'>
            @foreach($images as $image)
                <x-common.about-company-item :image="$image" />
            @endforeach
        </div>
    </div>
</div>
