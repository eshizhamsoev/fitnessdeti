@if($students->isNotEmpty())
    <x-content.students
        :students="$students->all()"
        class="margin"
    >
        <x-slot name="title">
            Наши звёзды
        </x-slot>
        <x-slot name="button">
            Хочу стать звездой!
        </x-slot>
    </x-content.students>
@endif
