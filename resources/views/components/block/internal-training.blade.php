<div {{ $attributes->merge(['class' => 'internal-training']) }}>
    <div class='container typography'>
        <h2 class='internal-training__title internal-training__title_{{ $titlePosition }}'>{{ $headingText }}</h2>
        <div class='internal-training__inner'>
            @foreach ($blocks as $block)
            <x-common.internal-training-item
                :text="$block['text']"
                :icon="$block['icon']"
            ></x-common.internal-training-item>
            @endforeach
        </div>
        @if ($buttonText)
        <div class='internal-training__btn'>
            <x-common.form-modal-trigger
                class='btn btn_type_accent'
                source="Заявка на тренировку из блока сертификации"
            >
                {{$buttonText}}
            </x-common.form-modal-trigger>
        </div>
        @endif
    </div>
</div>
