<div {{ $attributes->merge(['class' => 'our-services'])->except(['images', 'header']) }}>
    <div class='container typography'>
        <h2 class='our-services__title'>{{ $header }}</h2>
        <div class='our-services__inner'>
            @foreach($images as $image)
                <x-common.card :image="$image"></x-common.card>
            @endforeach
        </div>
    </div>
</div>
