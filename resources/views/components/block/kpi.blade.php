<x-common.kpi {{ $attributes }}>
    @foreach($items as $item)
        <x-common.kpi-item :data="$item" />
    @endforeach
</x-common.kpi>
