<div class='catalog'>
    <div class='container typography'>
        <h2 class='catalog__title'>Состав сборной</h2>
        <div class='catalog__inner'>
            @foreach($students as $student)
                <x-content.team-card :student="$student" />
            @endforeach
        </div>
    </div>
</div>
