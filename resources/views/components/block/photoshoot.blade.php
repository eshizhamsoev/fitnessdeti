<div {{ $attributes->merge(['class' => 'photo-session']) }}>
    <div class='container typography'>
        <h2 class='title title_center'>{{$heading}}</h2>
        <div class='photo-session__subtitle'>{{$leading}}</div>
        <div class='photo-session__inner'>
            @foreach($gallery as $image)
                @if($image->hasMedia(\App\Models\Website\GalleryImage::IMAGE_COLLECTION_MAIN))
                    <div data-fancybox='demo'
                         data-src='{{$image->getFirstMedia(\App\Models\Website\GalleryImage::IMAGE_COLLECTION_MAIN)->getUrl('original')}}'
                         class='photo-session__item'>
                        {{$image->getFirstMedia(\App\Models\Website\GalleryImage::IMAGE_COLLECTION_MAIN)(\App\Models\Website\GalleryImage::IMAGE_MAIN_CONVERSION)}}
                    </div>
                @endif
            @endforeach
        </div>

        @if($formSource && $buttonText)
            <div class='photo-session__btn'>
                <x-common.form-modal-trigger
                    class='btn btn_type_primary a-clear'
                    :source="$formSource"
                    :heading="$formHeading ?? ''"
                    :leading="$formLeading ?? ''"
                >
                    {{$buttonText}}
                </x-common.form-modal-trigger>
            </div>
        @endif
    </div>
</div>
