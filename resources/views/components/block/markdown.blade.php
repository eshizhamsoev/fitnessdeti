<div {{ $attributes->merge(['class' => 'typography']) }}>
    <div class="container">
        @markdown($markdownText)
    </div>
</div>
