<div {{ $attributes->merge(['class' => 'senior-trainers']) }}>
    <div class='senior-trainers__container container typography'>
        @if($heading)
            <h2 class='senior-trainers__title'>{{ $heading }}</h2>
        @endif
        <div class='senior-trainers__trainers'>
            @if($firstCoach)
                <x-content.coach-main-card
                    :coach="$firstCoach"
                    :cardKpiSide="\App\Enums\Website\CoachCardKpiSide::RIGHT()"
                />
            @endif
            @if($secondCoach)
                <x-content.coach-main-card
                    :coach="$secondCoach"
                    :cardKpiSide="\App\Enums\Website\CoachCardKpiSide::RIGHT()"
                />
            @endif
        </div>
        <div class='senior-trainers__trainers senior-trainers__trainers_mobile'>
            @if($firstCoach)
                <x-content.coach-small-card
                    :coach="$firstCoach"
                />
            @endif
            @if($secondCoach)
                <x-content.coach-small-card
                    :coach="$secondCoach"
                />
            @endif
        </div>
    </div>
</div>
