<div {{$attributes->merge(['class' => 'services'])}}>
    <div class='container typography'>
        <h2>Наши услуги</h2>
        <div class='services__inner'>
            @foreach($images as $image)
                <x-common.services-item :image="$image" />
            @endforeach
        </div>
    </div>
</div>
