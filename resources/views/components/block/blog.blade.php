<?php
/**
 * @var \App\View\Data\Block\BlogItemData[] $blogs
 * @var \Illuminate\Contracts\Support\Htmlable $links
 */
?>
<div class="blog-items wrapper">
    <div class="container">
        <h1 class="blog-items__title">
            Жизнь FD
        </h1>
        <div class="blog-items__items">
            @foreach($blogs as $blog)
                <article class="blog-item">
                    @if($blog->hasMedia())
                        <x-base.link class="blog-item__image" :href="$blog->getLink()">
                            {{ $blog->getMedia() }}
                        </x-base.link>
                    @endif
                    <div class="blog-item__content">
                        <h2 class="blog-item__title">
                            <x-base.link :href="$blog->getLink()">
                                {{ $blog->getTitle() }}
                            </x-base.link>
                        </h2>
                        <div class="blog-item__text typography">
                            @markdown($blog->getAnnounce())
                        </div>
                        @if($blog->getText() && $blog->getLink())
                            <div>
                                <x-base.link :href="$blog->getLink()" class="blog-item__more">
                                    Читать далее
                                </x-base.link>
                            </div>
                        @endif
                    </div>
                </article>
            @endforeach
        </div>
        {!! $links !!}
    </div>
</div>
