<div {{ $attributes->merge(['class' => 'camps']) }}>
    <div class='container typography'>
        <h2 class='camps__title'>Ближайшие сборы</h2>
        <div class='camps__inner'>
            @foreach($camps as $camp)
                <x-content.event-item
                    :data="$camp"
                    form-source="Сбор"
                />
            @endforeach
        </div>
    </div>
</div>
