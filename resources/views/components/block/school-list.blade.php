<div {{ $attributes->merge(['class' => 'find-us js-find-us']) }}  data-source-name="{{ $jsVariableName }}">
    <div class='container typography'>
        <h2 class='find-us__title'>{{ $heading }}</h2>
        <div class='find-us__inner'>
            <div class='find-us__left'>
                <form class='js-find-us-form'>
                    <label class='find-us__search search'>
                        <input type='text' class='find-us__search-input js-find-us__input' placeholder='Метро Беляево'/>
                        <button class='find-us__search-btn btn btn_type_accent js-find-us__btn'>Поиск</button>
                    </label>
                </form>
                <div class='find-us__items js-find-us__items'>
                    @foreach($schools as $school)
                        <x-common.school-card :school="$school" class="js-school-card" :data-find-id="$school->id"/>
                    @endforeach
                </div>

                @if($schoolsPageUrl && !$shouldShowAll)
                    <x-base.link :href="$schoolsPageUrl" class='btn btn_type_primary find-us__button '>Найдите
                        ближайшее отделение
                    </x-base.link>

                @endif
            </div>
            <div class='find-us__right js-find-us__map'>
                <div class='loader js-loader'></div>
            </div>
        </div>
    </div>
</div>


@once
    @push('body_end_before_script')
        <template data-find-item-template>
            <div class='school-card js-school-card' data-find-item='id' data-find-id="">
                <a data-template-action="main-link" data-find-item='icon' class='a-clear school-card__icon'>
                    <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::BUILDING()" width="21" height="14"
                                     data-find-item='icon-image'/>
                </a>
                <div class='school-card__address'>
                    <a  data-template-action="main-link" data-find-item='name' class='a-clear school-card__name'></a>
                    <a  data-template-action="main-link" data-find-item='address' class='a-clear school-card__street'></a>
                </div>
                <div class='school-card__contacts'>
                    <a data-find-item='tel' href='' class='school-card__tel a-clear'></a>
                    <div class='school-card__socials'>
                        <a data-find-item='whatsapp' href=''
                           class='a-clear school-card__social school-card__social_whatsapp'>
                            <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::WHATSAPP()" width="14" height="14"/>
                        </a>
                        <a data-find-item='telegram' href=''
                           class='a-clear school-card__social school-card__social_telegram'>
                            <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::TELEGRAM()" width="14" height="14"/>
                        </a>
                    </div>
                </div>
            </div>
        </template>
        <template data-my-placemark>
            <div class='my-placemark'>
               <svg width="53" height="61"><use xlink:href='sprite.svg#map-marker'></use></svg>
            </div>
        </template>
    @endpush
@endonce

@push('body_end_before_script')
    <script>
    window['{{ $jsVariableName }}'] = @json($dataForMap)
    </script>
@endpush
