<div {{ $attributes->merge(['class' => 'camps']) }}>
    <div class='container typography'>
        <h2 class='camps__title'>Ближайшие мастер-классы</h2>
        <div class='camps__inner'>
            @foreach($masterClasses as $masterClass)
                <x-content.event-item
                    :data="$masterClass"
                    form-source="Мастер класс"
                />
            @endforeach
        </div>
    </div>
</div>
