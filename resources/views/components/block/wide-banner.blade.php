@if($desktopImages && $mobileImages)
    <div class="banner-gymnastic" data-swiper-type='bannerGymnastic'>
        <div class='banner-gymnastic__slider swiper'>
            <div class='banner-gymnastic__wrapper swiper-wrapper'>
                <x-common.wide-banner-slide
                    :buttonText="$buttonText"
                    :buttonType="$buttonType"
                    :desktopImage="$desktopImages[0]"
                    :mobileImage="$mobileImages[0]"
                    :formHeading="$formHeading"
                    :formLeading="$formLeading"
                    :formSource="$formSource"
                    :desktopImagePosition="$desktopImagePosition"
                >
                </x-common.wide-banner-slide>
            </div>
            <div class='swiper-pagination'></div>
        </div>
    </div>
@endif
