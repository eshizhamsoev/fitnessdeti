<div {{$attributes->merge(['class' => 'job-application'])}}>
    <div class='container typography'>
        <h2>Заявка на вакансию</h2>
        <form action="{{ route('vacancy.send') }}" class="js-form">
            <div class='job-application__inner'>
                <div class='job-application__radios'>
                    <div class='job-application__item'>
                        <x-base.fieldset title="Выберите ваш город: *">
                            @foreach($cities as $city)
                                <x-base.checking-input-field
                                    :type="$inputTypes['radio']"
                                    name="city"
                                    :value="$city['name']"
                                    :label="$city['name']"
                                    required
                                >
                                </x-base.checking-input-field>
                            @endforeach
                        </x-base.fieldset>
                    </div>
                    <div class='job-application__item'>
                        <x-base.fieldset title="Ваш спортивный разряд:">
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='1-взрослый'
                                name='grade'
                                value='1-взрослый'
                            >
                            </x-base.checking-input-field>
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='КМС'
                                name='grade'
                                value='КМС'
                            >
                            </x-base.checking-input-field>
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='МС'
                                name='grade'
                                value='МС'
                            >
                            </x-base.checking-input-field>
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='МСМК'
                                name='grade'
                                value='МСМК'
                            >
                            </x-base.checking-input-field>
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='ЗМС'
                                name='grade'
                                value='ЗМС'
                            >
                            </x-base.checking-input-field>
                        </x-base.fieldset>
                    </div>
                    <div class='job-application__item'>
                        <x-base.fieldset title="Вид спорта:">
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='Акробатика'
                                name='sport'
                                value='Акробатика'
                            >
                            </x-base.checking-input-field>
                            <x-base.checking-input-field
                                :type="$inputTypes['radio']"
                                label='Художественная гимнастика'
                                name='sport'
                                value='Художественная гимнастика'
                            >
                            </x-base.checking-input-field>
                        </x-base.fieldset>
                    </div>
                </div>
                <div class='job-application__form'>
        <span class='job-application__form-column'>
            <x-base.fieldset>
                <x-base.text-field
                    :type="$inputTypes['tel']"
                    placeholder="Телефон *"
                    required
                    name="telephone"
                    label="telephone"
                >
                </x-base.text-field>
                <x-base.text-field
                    :type="$inputTypes['text']"
                    placeholder="E-mail *"
                    required
                    name="email"
                    label="email"
                >
                </x-base.text-field>
                <x-base.text-field
                    :type="$inputTypes['text']"
                    placeholder="ФИО *"
                    required
                    name="fullname"
                    label="fullname"
                >
                </x-base.text-field>
                <x-base.text-field
                    :type="$inputTypes['number']"
                    placeholder="Возраст"
                    name="age"
                    label="age"
                >
                </x-base.text-field>
                <x-base.text-field
                    :type="$inputTypes['text']"
                    placeholder="Образование"
                    name="education"
                    label="education"
                >
                </x-base.text-field>
            </x-base.fieldset>
        </span>
                    <span class='job-application__form-column'>
                    <x-base.fieldset>
                        <x-base.text-field
                            :type="$inputTypes['text']"
                            placeholder="Опыт работы"
                            name="experience"
                            label="experience"
                        >
                        </x-base.text-field>
                        <div class='form-text'>
                          <textarea
                              name="aboutself"
                              placeholder="О себе"
                              aria-label="aboutself"
                          ></textarea>
                        </div>
                        <x-base.checking-input-field
                            :type="$inputTypes['checkbox']"
                            name=''
                            value=''
                            required
                        >
                            Я согласен с <x-base.link href="/privacy">договором-оферты</x-base.link>
                        </x-base.checking-input-field>
                    </x-base.fieldset>
          <button type="submit" class='btn btn_type_accent job-application__btn js-form-btn'>Отправить</button>
      </span>
                </div>
            </div>
        </form>
    </div>
</div>
