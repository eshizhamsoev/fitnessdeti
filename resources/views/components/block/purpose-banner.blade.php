<div class="purpose">
    <div class="container purpose__container typography">
        <div class="watch-card">
            <img
                alt="" class="watch-card__img" height="500" loading="lazy"
                src="{{asset('static/images/components/blocks/purpose-banner/banner.jpg')}}" width="1208"
            >
            <div class="watch-card__content">
                <div class="watch-card__watch">
                    <div class="watch-btn">
                        <div class="watch-btn__circle watch-btn__circle_first">
                            <div class="watch-btn__circle watch-btn__circle_second">
                                <div class="watch-btn__circle watch-btn__circle_third"></div>
                            </div>
                        </div>
                        <x-base.youtube-link code="8ExCb8rQ5jI"
                                             class='watch-btn__play a-clear'>
                            <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::PLAY()" width='27' height='30'/>
                        </x-base.youtube-link>
                    </div>
                </div>
                <h2>Всероссийская сеть детских спортивных школ</h2>
                <div class="watch-card__subtitle">по художественной гимнастике и&nbsp;спортивной&nbsp;акробатике</div>
            </div>
        </div>
    </div>
</div>
