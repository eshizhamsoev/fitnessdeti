<div class='camps-item'>
    @if($eventItem->getMedia())
        <div class='camps-item__image'>
            {{$eventItem->getMedia()($eventItem->getMediaConversion())}}
        </div>
    @endif
    <div class='camps-item__content'>
        <div class='camps-item__name'>{{ $eventItem->getName() }}</div>
        <div class='camps-item__info'>
            <div><span>Дата: </span>{{ $eventItem->getDate() }}</div>
            @if($eventItem->getLocation())
                <div><span>Место: </span>{{ $eventItem->getLocation() }}</div>
            @endif
        </div>
        <div class='camps-item__btn-wrapper'>
            <x-common.form-modal-trigger
                class="btn btn_type_accent-secondary btn_size_sm camps-item__btn"
                :source="$formSource"
            >
                Хочу участвовать
            </x-common.form-modal-trigger>
        </div>
    </div>
</div>
