<?php
/**
 * @var \App\Models\Data\Coach $coach
 */
?>
<div class='trainer-profile'>
    <div class='container typography trainer-profile__container'>
        <div class='trainer-profile__inner'>
            <div class='trainer-profile__image'>
                <x-content.coach-main-card :coach="$coach" />
            </div>
            <div class='trainer-profile__kpi'>
                <div class='trainer-profile__item trainer-profile__item_green'>
                    <div class='trainer-profile__count'>{{ $coach->medal_count }}</div>
                    <div class='trainer-profile__divide'></div>
                    <div class='trainer-profile__text'>Медалей</div>
                </div>
                <div class='trainer-profile__item'>
                    <img width='250' height='260' loading="lazy" class='trainer-profile__img' alt='' src='{{ asset('static/images/components/content/trainer-profile/1.jpg') }}'>
                </div>
                <div class='trainer-profile__item trainer-profile__item_green'>
                    <div class='trainer-profile__count'>{{ $coach->child_count }}</div>
                    <div class='trainer-profile__divide'></div>
                    <div class='trainer-profile__text'>Детей</div>
                </div>
                <div class='trainer-profile__item'>
                    <img width='250' height='260' loading="lazy" class='trainer-profile__img' alt=''
                         src='{{ asset('static/images/components/content/trainer-profile/2.jpg') }}'>
                </div>
                <div class='trainer-profile__item trainer-profile__item_red'>
                    <div class='trainer-profile__count'>{{ $coach->sport_category_count }}</div>
                    <div class='trainer-profile__divide'></div>
                    <div class='trainer-profile__text'>Разрядов</div>
                </div>
                <div class='trainer-profile__item'>
                    <img width='250' height='260' loading="lazy" class='trainer-profile__img' alt=''
                         src='{{ asset('static/images/components/content/trainer-profile/3.jpg') }}'>
                </div>
            </div>
        </div>
    </div>
</div>
