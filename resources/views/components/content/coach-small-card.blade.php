<x-base.link :href='$data->getUrl()' {{ $attributes->merge(['class' => 'catalog-item']) }}>
    <div class='catalog-item__image catalog-item__image_{{ $data->getDiscipline()->getSvgName() }}'>
        @if($data->getImage())
            {{$data->getImage()}}
        @else
            <img loading="lazy" width='318' height='424' src="{{ $data->getFallbackImage() }}" alt=""/>
        @endif
        <div class='catalog-item__icon catalog-item__icon_{{ $data->getDiscipline()->getSvgName() }}'>
            <x-base.svg-icon width="22" height="22" :icon="$data->getDiscipline()->getSvgName()"/>
        </div>

        <div class='catalog-item__certificate'>
            <x-base.svg-icon width="28" height="36" :icon="\App\Enums\Website\SvgIconName::LABEL_CERTIFICATE()"/>
        </div>
    </div>
    <div class='catalog-item__text'>
        <div class='catalog-item__name'>{{ $data->getName() }}</div>
        @if($data->getClass()->canBeShown())
            <div class='catalog-item__info'>{{ $data->getClass()->getTitle() }}</div>
        @endif
    </div>
</x-base.link>
