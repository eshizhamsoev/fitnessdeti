<div {{ $attributes->merge(['class' => 'resume']) }}>
    <div class='container'>
        <h1 class='resume__name'>{{ $coachName }}</h1>
        @if($resume)
            <div class='resume__inner'>
                @foreach($resume as $heading => $content)
                    <div class='resume__item'>
                        <div class='resume__title'>{{$heading }}:</div>
                        <div class='resume__text'>
                            {{ $content }}
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        <div class='resume__btn'>
            <x-common.form-modal-trigger class='btn btn_type_accent' :source="$source">
                Хочу к этому тренеру
            </x-common.form-modal-trigger>
        </div>
    </div>
</div>
