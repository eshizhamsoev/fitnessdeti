<div {{ $attributes->merge(['class' => 'trainer-offices']) }}>
    <div class='container typography'>
        <h2>Тренер ведет в отделениях</h2>
        <div class='trainer-offices__inner'>
            <div class='trainer-offices__addresses'>
                @foreach($schools as $school)
                    <x-common.school-card :school="$school"/>
                @endforeach
            </div>
            <div class='trainer-offices__img'><img width='700' height='400' loading="lazy"
                                                   src='{{ asset('static/images/components/content/coach-school/1.jpg') }}' alt=''></div>
        </div>
        <div class='trainer-offices__btn'>
            <x-common.form-modal-trigger class='btn btn_type_accent' :source="$source">
                Хочу к этому тренеру
            </x-common.form-modal-trigger>
        </div>
    </div>
</div>
