<div {{ $attributes->merge(['class' => 'catalog-item']) }}>
    <div class='catalog-item__image catalog-item__image_{{ $student->getSvgIconName() }}'>
        @if($student->getImage())
            {{$student->getImage()}}
        @else
            <img loading="lazy" width='318' height='424' src="{{ $student->getFallbackImage() }}" alt=""/>
        @endif
        @if($student->getSvgIconName())
            <div class='catalog-item__icon catalog-item__icon_{{ $student->getSvgIconName() }} catalog-item__icon_top'>
                <x-base.svg-icon width="22" height="22" :icon="$student->getSvgIconName()"/>
            </div>
        @endif
    </div>
    <div class='catalog-item__text'>
        <div class='catalog-item__name'>{{ $student->getName() }}</div>
    </div>
</div>
