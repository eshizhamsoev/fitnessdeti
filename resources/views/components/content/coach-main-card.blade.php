<?php
/**
 * @var \App\View\Data\Content\CoachMainCard\CoachCardData $coachData
 */


$block = app(\Upline\BemClassname\BemFactory::class)->block('coach-main-card');
?>

{{--<div class="coach-main-card coach-main-card_{{ params.short }}">--}}

<div
    class="{{ $block->cn([$coachData->getDiscipline()->getColor(), $cardKpiSide], [$attributes->get('class')]) }}"
    {{ $attributes->except('class') }}>
    <div class='coach-main-card__image'>
        <div class="coach-main-card__wrapper">
            <x-base.link class='coach-main-card__image-wrapper'
                         :href="$coachData->getUrl()">
                @if($coachData->getImage())
                    {{ $coachData->getImage() }}
                @else
                    <img loading="lazy" width='168' height='200' src="{{ $coachData->getFallbackImage() }}" alt=""/>
                @endif
            </x-base.link>
            <div class='container coach-main-card__container typography'>
                <x-base.link class='h3 coach-main-card__title'
                             :href="$coachData->getUrl()">{{ $coachData->getName() }}</x-base.link>
                <div class='coach-main-card__info'>
                    <div class='coach-main-card__shortcuts'>
                        <div
                            class='coach-main-card__label coach-main-card__label_{{ $coachData->getDiscipline()->getColor() }}'>
                            <x-base.svg-icon width="28" height="28" :icon="$coachData->getDiscipline()->getSvgName()"/>
                        </div>
                        @if($coachData->getYoutubeCode())
                            <x-base.youtube-link class='btn btn_type_primary coach-main-card__shortcuts-btn' :code="$coachData->getYoutubeCode()">
                                <x-base.svg-icon width='18' height='18' :icon="\App\Enums\Website\SvgIconName::PLAY()"/>
                            </x-base.youtube-link>
                        @endif
                    </div>
                    <div class='coach-main-card__info-text'>
                        @if($coachData->getClass()->canBeShown())
                            <div>{{ $coachData->getClass()->getTitle() }}</div>
                        @endif
                        <div><b>{{ $coachData->getDiscipline()->getTitle() }}</b></div>
                    </div>
                </div>
            </div>
            <div class='coach-main-card__certificate'>
                <x-base.svg-icon width="28" height="36" :icon="$labelIcon"/>
            </div>
        </div>
    </div>
    @if($cardKpiSide)
        <div class="coach-main-card__kpi">
            @foreach($kpi as $item)
                <x-common.kpi-item :data="$item"
                                   :size="\App\Enums\Website\Kpi\KpiItemSize::SMALL()"></x-common.kpi-item>
            @endforeach
        </div>
    @endif
</div>

