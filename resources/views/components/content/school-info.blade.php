<div {{ $attributes->merge(['class' => 'school']) }}>
    <div class='container typography'>
        <div class='school__container'>
            <div class='school__map js-school-map'
                 data-name='{{ $school->name }}'
                 data-latitude='{{ $school->latitude }}'
                 data-longitude='{{ $school->longitude }}'
                 data-address='{{ $school->address }}'
            ></div>
            <div class='school__buttons'>
                <x-base.phone-link :phone="$school->phone" class='a-clear btn btn_size_sm btn_type_primary'>Позвонить
                </x-base.phone-link>
                <x-common.form-modal-trigger :source="$source" class='btn btn_size_sm btn_type_accent'>
                    Записаться
                </x-common.form-modal-trigger>
                @if($school->map_route_link)
                    <a href='{{ $school->map_route_link }}' target="_blank" class='a-clear btn btn_size_sm btn_type_secondary-white'>Маршрут</a>
                @endif
            </div>
            <div class='school__title'>
                <h1>{{$school->short_name ? 'Спортивная школа FD - ' . $school->short_name : $school->name }}</h1>
            </div>
            <div class='school__inner'>
                @if($school->metroStations->isNotEmpty())
                    <div class='school__item'>
                        <div><b>Метро:</b></div>
                        <div>{{ $school->metroStations->pluck('name')->join(', ') }}</div>
                    </div>
                @endif
                <div class='school__item'>
                    <div><b>Телефон:</b></div>
                    <div>
                        <x-base.phone-link :phone="$school->phone" class="a-clear"/>
                    </div>
                </div>
                <div class='school__item'>
                    <div><b>Адрес:</b></div>
                    <div>{{ $school->address }}</div>
                </div>
                @if($school->work_hours)
                    <div class='school__item'>
                        <div><b>Часы работы:</b></div>
                        <div>{{ $school->work_hours }}</div>
                    </div>
                @endif
                @if($school->how_to_get)
                    <div class='school__item school__item_how'>
                        <div><b>Как добраться:</b></div>
                        <div>
                            {{$school->how_to_get}}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@once
    @push('body_end_before_script')
        <script src="https://api-maps.yandex.ru/2.1?lang=ru_RU" defer></script>
    @endpush
@endonce
