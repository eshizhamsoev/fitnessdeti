<div {{ $attributes->merge(['class' => 'department-trainers js-department-trainers']) }}>
    <div class='container typography'>
        <h2 class='department-trainers__title'>Тренеры отделения</h2>
        <div class='department-trainers__inner'>
            <x-common.carousel sliderName="department-trainers__slider" :hasShadow="true">
                <x-slot name="slides">
                    @foreach($coaches as $coach)
                        <x-content.coach-main-card :coach="$coach" />
                    @endforeach
                </x-slot>
            </x-common.carousel>
        </div>
    </div>
</div>
