<div {{ $attributes->merge(['class' => 'department-trainers js-department-trainers']) }}>
    <div class='container typography'>
        <h2 class='department-trainers__title'>Тренеры отделения</h2>
        <x-common.coaches-carousel :coaches="$coaches" class='department-trainers__inner' />
    </div>
</div>
