<div class='banner swiper-slide {{$hiddenClasses}}'>
    <picture class='banner-gymnastic-slide__picture'>
        @if($mobileMedia)
            <source media='(max-width: 767px)' srcset={{ $mobileMedia->getUrl('main') }} />
        @endif
        @if($desktopMedia)
            <source media='(min-width: 768px)' srcset={{ $desktopMedia->getUrl('main') }} />
        @endif
        <img loading="lazy"
             src={{ $desktopMedia ? $desktopMedia->getUrl('main') : $mobileMedia->getUrl('main')}} alt=''
             class='banner-gymnastic-slide__image banner-gymnastic-slide__image_position_left'/>
    </picture>
    <div class='banner-gymnastic-slide__container container'>
        <div class='banner-gymnastic-slide__content typography'>
            @if($name)
                <div class='banner-gymnastic-slide__title banner-gymnastic-slide__title_ h1'>
                    {{ $name }}
                </div>
            @endif
            @if($description)
                <div class='banner-gymnastic-slide__text'>
                    @markdown($description)
                </div>
            @endif
            <div class='banner-gymnastic-slide__btn-group'>
                @if($link)
                    <x-base.link :href="$link" class="btn btn_type_accent banner-gymnastic-slide__btn a-clear">
                        {{$linkText ?: 'Оставить заявку'}}
                    </x-base.link>
                @elseif($linkText && $name)
                    <x-common.form-modal-trigger
                        class='btn btn_type_accent banner-gymnastic-slide__btn'
                        :source="$name"
                    >
                        {{$linkText}}
                    </x-common.form-modal-trigger>
                @endif
            </div>

        </div>
    </div>
</div>
