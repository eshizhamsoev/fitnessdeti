<div class="teaching-groups-item">
    @if($label ?? false)
        <div class='teaching-groups-item__label'>
            {{ $label }}
        </div>
    @endif
    <div class="teaching-groups-item__top">
        <div class="teaching-groups-item__column">
            <div class="teaching-groups-item__name">{{ $group->name }}</div>
            <div class="teaching-groups-item__description">
                {{ $group->description }}
            </div>
            <button class="btn teaching-groups-item__details js-teaching-groups-item__details">
                Подробнее
            </button>
        </div>
        <div class="teaching-groups-item__column">
            <div class="teaching-groups-item__price">{{ number_format($group->base_price, 0, '', ' ') }} &#8381;</div>
            <div class="teaching-groups-item__time">{{ $group->time_unit_for_price->description }}</div>
        </div>
    </div>
    <div class="teaching-groups-item__bottom">
        @if($group->age)
            <div class="teaching-groups-item__row">
                <div class="teaching-groups-item__row-key">Возраст:</div>
                <div class="teaching-groups-item__row-value">{{ $group->age  }}</div>
            </div>
        @endif
        @if($group->capacity)
            <div class="teaching-groups-item__row">
                <div class="teaching-groups-item__row-key">Наполняемость:</div>
                <div class="teaching-groups-item__row-value">{{ $group->capacity }}</div>
            </div>
        @endif
        @if($group->price_per_hour)
            <div class="teaching-groups-item__row">
                <div class="teaching-groups-item__row-key">Цена часа:</div>
                <div class="teaching-groups-item__row-value">{{ $group->price_per_hour }}</div>
            </div>
        @endif
        @if($group->coaches)
            <div class="teaching-groups-item__row">
                <div class="teaching-groups-item__row-key">Тренеры:</div>
                <div class="teaching-groups-item__row-value">{{ $group->coaches }}</div>
            </div>
        @endif
        <div class="teaching-groups-item__spacer"></div>
        <div class="teaching-groups-item__buttons">
            <button class="btn teaching-groups-item__hide js-teaching-groups-item__hide">Скрыть</button>
            <x-common.form-modal-trigger
                class="btn btn_type_accent-secondary teaching-groups-item__sign-up"
                heading="Оставьте заявку"
                leading="И мы с вами свяжемся"
                :source="$group->name">
                Записаться
            </x-common.form-modal-trigger>
        </div>
    </div>
</div>
