<?php
/**
 * @var \App\Models\Data\Student[] $students
 */
?>
<div {{ $attributes->merge(['class' => 'students']) }}>
    <div class="container typography">
        <h2 class="students__title">
            {{ $title }}
        </h2>
        <x-common.carousel sliderName="students" :hasShadow="true">
            <x-slot name="slides">

                @foreach($students as $student)
                    <div class="student swiper-slide a-clear">
                        <div class="student__image">
                            {{ $student->getFirstMedia('main')('main') }}
                        </div>
                        <div class="student__content">
                            <div class="student__name">{{ $student->lastname }} {{ $student->firstname }}</div>
                        </div>
                    </div>
                @endforeach
            </x-slot>
        </x-common.carousel>
        <div class='students__btn'>
            <button class='btn btn_type_accent'>
                {{ $button }}
            </button>
        </div>
    </div>
</div>
