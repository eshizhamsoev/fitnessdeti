<?php
/**
 * @var \App\View\Data\Content\Competition\CompetitionModalItem[] $competitions
 */
?>
<div class="modal-events">
    @forelse($competitions as $competition)
        <div class='modal-events__item'>
            @if($competition->hasPhoto())
                <div class='modal-events__image'>
                    {{ $competition->getPhoto() }}
                    @if($competition->getIcon())
                        <div class='modal-events__icon'>
                            <x-base.svg-icon width="34" height="45" :icon="$competition->getIcon()"/>
                        </div>
                    @endif
                    @if($competition->getType())
                        <span class='btn btn_type_primary btn_size_xs modal-events__btn'>
                            {{ $competition->getType() }} соревнование
                        </span>
                    @endif
                </div>
            @endif
            <div class='modal-events__content typography'>
                <h2>{{ $competition->getName() }}</h2>
                <div class='modal-events__text'>
                    {{ $competition->getDescription() }}
                </div>
                <div class='modal-events__info'>
                    <div class='modal-events__when'>
                        <svg class='modal-events__info-icon' width="16" height="16">
                            <use xlink:href='sprite.svg#calendar-second'></use>
                        </svg>
                        <div class='modal-events__info-text'>
                            {{ $competition->getDateStart() }}
                            @if($competition->getDateEnd())
                                - {{ $competition->getDateEnd() }}
                            @endif
                        </div>
                    </div>
                    @if($competition->getTime())
                        <div class='modal-events__when'>
                            <svg class='modal-events__info-icon' width="16" height="16">
                                <use xlink:href='sprite.svg#clock'></use>
                            </svg>
                            <div class='modal-events__info-text'>{{ $competition->getTime() }}</div>
                        </div>
                    @endif
                    @if($competition->getLocation())
                        <div class='modal-events__where'>
                            <svg class='modal-events__info-icon' width="16" height="16">
                                <use xlink:href='sprite.svg#map-pin'></use>
                            </svg>
                            <span class='modal-events__text'>
                                {{ $competition->getLocation() }}
                            </span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @empty
        Нет соревнований на эту дату.
    @endforelse
</div>
