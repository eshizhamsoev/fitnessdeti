<div {{ $attributes->class(['class' => 'gallery']) }}>
    <div class='container typography'>
        <h2 class='gallery__title'>{{ $galleryData->getTitle() }}</h2>
        <div class='gallery__inner'>
            @foreach($galleryData->getPhotos() as $image)
                <div class='gallery__item' data-fancybox='{{ $galleryName }}' data-src={{ $image->getUrl('original') }}>
                    {{ $image('thumb') }}
                </div>
            @endforeach
        </div>
        @if($showMore)
            <div class='gallery__btn'>
                <button class='btn btn_type_secondary'>Смотреть еще</button>
            </div>
        @endif
    </div>
</div>
