<div class='internal-training-item'>
    <div class='internal-training-item__text'>@markdown($text)</div>
    <div class='internal-training-item__icon'>
        <x-base.svg-icon :icon="$icon" width="42" height="42"/>
    </div>
</div>
