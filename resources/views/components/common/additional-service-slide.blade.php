<x-base.link {{ $attributes->merge(['class' => 'a-clear additional-service__item swiper-slide'])->except(['image']) }} :href="$image->link">
    <div class='additional-service__image'>
        {{ $image->getFirstMedia('main')('main') }}
    </div>
    <div class='additional-service__text'>
        {{ $image->name }}
    </div>
</x-base.link>
