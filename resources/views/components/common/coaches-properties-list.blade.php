<div {{$attributes}}>
    @foreach($items as $item)
        <x-common.properties-item
            :title="$item['title']"
            :icon="$item['icon']"
        />
    @endforeach
</div>
