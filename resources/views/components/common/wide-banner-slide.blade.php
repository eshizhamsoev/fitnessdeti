<div class='banner-gymnastic-slide swiper-slide'>
    <picture class='banner-gymnastic-slide__picture'>
        <source media='(max-width: 767px)' srcset={{ $mobileImage->getFirstMedia('main')->getUrl('main') }} />
        <source media='(min-width: 768px)' srcset={{ $desktopImage->getFirstMedia('main')->getUrl('main') }} />
        <img width='375' height='623' loading="lazy" src={{ $desktopImage->getFirstMedia('main')->getUrl('main') }} alt='' class='banner-gymnastic-slide__image banner-gymnastic-slide__image_position_{{ $desktopImagePosition }}' />
    </picture>
    <div class='banner-gymnastic-slide__container container'>
        <div class='banner-gymnastic-slide__content typography'>
            <div
                class='banner-gymnastic-slide__title banner-gymnastic-slide__title_ h1'>{{ $desktopImage->name }}</div>
            <div class='banner-gymnastic-slide__text'>
                @markdown($desktopImage->description)
            </div>
            <div class='banner-gymnastic-slide__btn-group'>
                <x-common.form-modal-trigger
                    class='btn {{$buttonType}} banner-gymnastic-slide__btn'
                    :source="$formSource"
                    :heading="$formHeading"
                    :leading="$formLeading"
                >
                    {{$buttonText}}
                </x-common.form-modal-trigger>
            </div>
        </div>
    </div>
</div>
