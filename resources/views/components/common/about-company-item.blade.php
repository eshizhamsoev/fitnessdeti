<div {{ $attributes->merge(['class' => 'about-company-item swiper-slide']) }}>
    <x-base.link :href="$image->link" class='a-clear about-company-item__image'>
        {{ $image->getFirstMedia('main')('main') }}
    </x-base.link>
    <div class='about-company-item__bottom'>
        <x-base.link :href="$image->link" class='about-company-item__link a-clear'>
            {{$image->name}}
            <x-base.svg-icon width="22" height="27" :icon="$arrow"/>
        </x-base.link>
    </div>
</div>
