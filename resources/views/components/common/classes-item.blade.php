<div {{ $attributes->merge(['class' => 'classes-item swiper-slide']) }}>
    <span class="a-clear classes-item__image">
        {{ $image->getFirstMedia('main')('main') }}
    </span>
    <div class="classes-item__content">
        <div class="classes-item__count">{{ str_pad($count + 1, 2, '0', STR_PAD_LEFT) }}</div>
        <div class="classes-item__text">
            {{$image->name}}
        </div>
    </div>
</div>
