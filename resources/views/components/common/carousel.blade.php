<div {{ $attributes->merge(['class' => 'carousel ' . ($hasShadow ? 'carousel_shadow' : '')]) }} data-swiper-type='{{ $sliderName }}'>
    <div class='carousel__container swiper'>
        <div class='swiper-wrapper'>
            {{ $slides }}
        </div>
        <div class='carousel__pagination swiper-pagination'></div>
    </div>
    <button class='btn carousel-arrow carousel-arrow_prev'>
        <x-base.svg-icon width="10" height="16" :icon="$arrowIcon"/>
    </button>
    <button class='btn carousel-arrow carousel-arrow_next'>
        <x-base.svg-icon width="10" height="16" :icon="$arrowIcon"/>
    </button>
</div>



