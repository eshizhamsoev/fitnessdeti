<button {{ $attributes->merge(['class' => 'js-form-modal-trigger']) }}
        type="button"
        data-form-source="{{ $source }}"
        data-form-heading="{{ $heading }}"
        data-form-leading="{{ $leading }}">
    {{ $slot }}
</button>

@once
    @push('body_end')
        <div id='fancy-modal' class='fancy-modal js-fancy-modal typography' hidden>
            <h2 class='title title_center js-heading'></h2>
            <div class='fancy-modal__subtitle js-leading'></div>
            <form class='fancy-modal__form js-form' action="{{ route('leads.store') }}">
                <span class="js-form-alert"></span>
                @csrf
                <x-base.text-field
                    required
                    name="name"
                    label="Ваше имя"
                    :type="\App\Enums\Website\Forms\TextFieldType::TEXT()"
                />
                <x-base.text-field
                    required
                    minlength="10"
                    pattern="^\+7\(\d\d\d\)\d\d\d-\d\d-\d\d$"
                    name="phone"
                    label="Ваш телефон" placeholder="+7 (___) ___ - __ - __"
                    :type="\App\Enums\Website\Forms\TextFieldType::TEL()"
                />
                <x-base.checking-input-field
                    required
                    name="agreement"
                    :type="\App\Enums\Website\Forms\CheckingFieldType::CHECKBOX()"
                    :is-checked="true"
                >
                    <x-slot name="label">
                        Даю <a href="{{ $privacyUrl }}" target="_blank" class="a-clear fancy-modal__link"> согласие </a>
                        на обработку персональных данных
                    </x-slot>
                </x-base.checking-input-field>

                <button class='btn btn_type_accent fancy-modal__btn js-form-btn' type="submit">Отправить</button>
                <input name='source' class='js-source' type='hidden'>
            </form>
        </div>

        <div id='fancy-modal-success' hidden>
            <h2>Спасибо за ваше обращение!</h2>
        </div>

        <div id='fancy-modal-error' hidden>
            <h2>Произошла ошибка при отправке формы, но вы всегда можете нам позвонить.</h2>
        </div>

        <template data-btn-send>
            <span class='loader loader_small active'></span>Отправляем
        </template>
    @endpush
@endonce
