<div {{ $attributes->merge(['class' => 'properties-item']) }} >
    <div class='properties-item__icon'>
        <x-base.svg-icon width="60" height="60" :icon="$icon" />
    </div>
    <div class='properties-item__line'></div>
    <div class='properties-item__title'>{{ $title }}</div>
</div>
