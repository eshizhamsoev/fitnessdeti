<div {{ $attributes->merge(['class' => 'school-card']) }}>
    @if($school->metroStations->isNotEmpty())
        <x-base.link :href="$school_url" class='school-card__icon school-card__icon_metro'>
            <x-base.svg-icon width="20" height="20" :icon="$metroIcon"></x-base.svg-icon>
        </x-base.link>
    @else
        <x-base.link :href="$school_url" class='school-card__icon school-card__icon'>
            <x-base.svg-icon width="20" height="20" :icon="$buildingIcon"></x-base.svg-icon>
        </x-base.link>
    @endif
    <div class='school-card__address'>
        <x-base.link :href="$school_url" class='school-card__name'>{{ $school->name }}</x-base.link>
        <x-base.link :href="$school_url" class='school-card__street'>{{ $school->address }}</x-base.link>
    </div>
    <div class='school-card__contacts'>
        @if($school->phone)
            <x-base.phone-link :phone="$school->phone" class='school-card__tel a-clear'></x-base.phone-link>
        @endif
        <div class='school-card__socials'>
            @if($school->whatsapp)
                <a href='https://wa.me/{{ $school->whatsapp }}'
                   class='a-clear school-card__social school-card__social_whatsapp'>
                    <x-base.svg-icon width="14" height="14" :icon="$whatsappIcon"/>
                </a>
            @endif
            @if($school->telegram)
                <a href="https://t.me/{{ $school->telegram }}"
                   class='a-clear school-card__social school-card__social_telegram'>
                    <x-base.svg-icon width="14" height="14" :icon="$telegramIcon"/>
                </a>
            @endif
        </div>
    </div>
</div>
