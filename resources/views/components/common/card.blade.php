<div {{ $attributes->merge(['class' => 'card'])->except('image') }}>
    <x-base.link href='{{$image->link}}' class='card__img a-clear'>
        {{ $image->getFirstMedia('main')('main') }}
    </x-base.link>
    <div class='card__content typography'>
        <div class='card__title'>{{ $image->name }}</div>
        <div class='card__text'>
            @markdown($image->description)
        </div>
        <div><a href='{{$image->link}}' class='a-clear btn btn_type_primary card__button'>Подробнее</a></div>
    </div>
</div>
