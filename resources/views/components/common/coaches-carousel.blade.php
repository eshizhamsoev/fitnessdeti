<div {{$attributes}}>
    <x-common.carousel sliderName="general" :hasShadow="true">
        <x-slot name="slides">
            @foreach($coaches as $coach)
                <x-content.coach-small-card class="swiper-slide" :coach="$coach" />
            @endforeach
        </x-slot>
    </x-common.carousel>
</div>
