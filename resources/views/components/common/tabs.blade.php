<ul class='tabs ul-clear tabs_{{ $color }}'>
    @foreach($tabs as $tab)
        <li class='tab js-tab'>
            <x-base.link :href="$tab->getLink()" :class="'btn tab__btn ' . ($tab->getIsActive() ? 'active' : '')">{{ $tab->getButtonText() }}</x-base.link>
        </li>
    @endforeach
</ul>
