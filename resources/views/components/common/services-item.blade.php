<a {{ $attributes->merge(['class' => 'a-clear services__item', 'href' => $image->link]) }}>
    {{ $image->getFirstMedia('main')('main') }}
    <div class='services__text'>{{$image->name}}</div>
</a>
