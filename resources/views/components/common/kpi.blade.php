<div {{ $attributes->merge(['class' => 'kpi']) }}>
    <div class="kpi__container container">
        {{ $slot }}
    </div>
</div>
