<div class='reviews-block__item js-tab-review active'>
    @if($review->youtube_code ?? $image)
        @if($review->youtube_code)
            <div class='reviews-block__img'>
                <img width='500' height='300' loading="lazy"
                     src='https://img.youtube.com/vi/{{$review->youtube_code}}/mqdefault.jpg' alt=''>
                <x-base.youtube-link :code="$review->youtube_code"
                        class='btn reviews-block__item-btn-play'>
                    <x-base.svg-icon :icon="\App\Enums\Website\SvgIconName::PLAY()" width='23' height='23'/>
                </x-base.youtube-link>
            </div>
        @elseif($image)
         <div class='reviews-block__img'>
                {{$image(\App\Models\Data\Review::IMAGE_CONVERSION_REVIEW)}}
            </div>
        @endif
    @endif
    <div class='reviews-block__content'>
        <div class='reviews-block__top'>
            @if($avatar)
                <div class='reviews-block__avatar'>
                    {{$avatar(\App\Models\Data\Review::IMAGE_CONVERSION_AVATAR)}}
                </div>
            @endif
            <div class='reviews-block__info'>
                <div class='reviews-block__name'>{{$review->name}}</div>
                <div class='reviews-block__activity'>{{$review->discipline->name}}</div>
            </div>
        </div>
        <div class='reviews-block__text js-review__text'>
            {{$review->text}}
        </div>
        <button class='btn reviews-block__item-btn js-review__btn'>Читать далее</button>
    </div>
</div>
