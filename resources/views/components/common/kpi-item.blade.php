<?php
$block = app(\Upline\BemClassname\BemFactory::class)->block('kpi-item');
?>

<div {{ $attributes->except(['class']) }}
     class="{{ $block->cn(['size' => $size, 'color' => $data->getColor()]) }}">
    <div class="kpi-item__icon">
        <x-base.svg-icon width="40" height="40" :icon="$data->getIcon()"/>
    </div>
    <div class="kpi-item__content">
        <div class="kpi-item__amount">{{ $data->getAmount() }}</div>
        <div class="kpi-item__title">{{ $data->getTitle() }}</div>
        <div class="kpi-item__title-mobile">{{ $data->getMobileTitle() }}</div>
    </div>
</div>
