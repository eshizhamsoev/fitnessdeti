@extends('layouts.page')
@section('page-content')
    <div class="error">
        <div class="error__code">
            @yield('code')
        </div>

        <div class="error__message">
            @yield('message')
        </div>
    </div>
@endsection
