<?php
/**
 * @var $lead \App\Domain\Leads\Models\Lead
 */
?>
<p>Номер: <b>{{ $lead->id }}</b></p>
<p>Имя: <b>{{ $lead->name }}</b></p>
<p>Телефон: <b>{{ $lead->phone }}</b></p>
<p>Место расположение формы: <b>{{ $lead->source }}</b></p>
<p>Url: <b>{{ $lead->url }}</b></p>
<p>Время отправки: <b>{{ $lead->created_at }}</b></p>

@if($lead->utm_data)
    @foreach($lead->utm_data as $key => $value)
        @if($value)
            <p>{{ $key }}: <b>{{ $value }}</b></p>
        @endif
    @endforeach
@endif
