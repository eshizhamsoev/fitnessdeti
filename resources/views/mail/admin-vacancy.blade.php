<?php
/**
 * @var $application \App\Models\Data\VacancyApplication
 */
?>
<p>Номер: <b>{{ $application->id }}</b></p>
<p>Город: <b>{{ $application->city }}</b></p>
<p>Разряд: <b>{{ $application->rank }}</b></p>
<p>Вид спорта: <b>{{ $application->sport_type }}</b></p>
<p>Телефон: <b>{{ $application->phone }}</b></p>
<p>Электронная почта: <b>{{ $application->email }}</b></p>
<p>Имя: <b>{{ $application->name }}</b></p>
<p>Возраст: <b>{{ $application->age }}</b></p>
<p>Образование: <b>{{ $application->education }}</b></p>
<p>Опыт: <b>{{ $application->experience }}</b></p>
<p>О заявителе: <b>{{ $application->about_applicant }}</b></p>
<p>Время отправки: <b>{{ $application->created_at }}</b></p>

@if($application->utm)
    @foreach($application->utm as $key => $value)
        @if($value)
            <p>{{ $key }}: <b>{{ $value }}</b></p>
        @endif
    @endforeach
@endif
