import './sprite';
import './sliders';
import './fancy';
import './menu';
import './tabs';
import './tippy';
import './mapSearch';
import './mapSchool';
import 'focus-visible';
import './mask';
import './details';
import './form-handler';
import './switch';
import './filterTrainers';
import './observer';
import './loadScript';

const nodeCalendar = document.querySelector('.calendar__inner');

if (nodeCalendar) {
  import('./calendar')
    .then(({ MyCalendar }) => {
      new MyCalendar(nodeCalendar).init();
    })
    .catch((error) => console.error(error));
}

document.querySelectorAll('.js-footer__title').forEach((item) => {
  item.addEventListener('click', () => {
    item.classList.toggle('active');
  });
});

document
  .querySelectorAll('.js-teaching-groups-item__details')
  .forEach((item) => {
    item.addEventListener('click', () => {
      item.parentNode.parentNode.parentElement.classList.toggle('active');
    });
  });

document.querySelectorAll('.js-teaching-groups-item__hide').forEach((item) => {
  item.addEventListener('click', () => {
    item.parentNode.parentNode.parentElement.classList.toggle('active');
  });
});

document.querySelectorAll('.js-questions__icon').forEach((item) => {
  item.addEventListener('click', () => {
    item.parentElement.classList.toggle('active');
  });
});

document.querySelectorAll('.reviews__more').forEach((item) => {
  item.addEventListener('click', () => {
    item.parentElement
      .querySelector('.reviews__text')
      .classList.toggle('active');
    item.parentElement.querySelectorAll('.reviews__more').forEach((more) => {
      more.classList.toggle('active');
    });
  });
});

const elements = document.querySelector('.js-department-trainers');
if (elements) {
  let open = false;
  const items = elements.querySelectorAll('.js-department-trainers__item');
  const moreBtn = elements.querySelector('.js-department-trainers__btn_more');
  if (moreBtn) {
    moreBtn.addEventListener('click', () => {
      items.forEach((e) => {
        e.classList.toggle('department-trainers__item');
      });
      open = !open;
      moreBtn.innerHTML = open ? 'Свернуть' : 'Показать еще';
    });
  }
}

const reviews = document.querySelectorAll('.js-tab-review');
for (const review of reviews) {
  const btn = review.querySelector('.js-review__btn');
  const content = review.querySelector('.js-review__text');
  let open = false;
  if (btn) {
    btn.addEventListener('click', () => {
      content.classList.toggle('active');
      open = !open;
      btn.innerHTML = open ? 'Свернуть' : 'Читать далее';
    });
  }
}

const toUp = document.querySelector('.up');

if (toUp) {
  window.addEventListener('scroll', () => {
    if (window.pageYOffset > 541) {
      toUp.classList.add('active');
    } else {
      toUp.classList.remove('active');
    }
  });
  toUp.addEventListener('click', () => {
    window.scrollTo(0, 0);
  });
}
