const trainersElements = document.querySelector('.js-trainers');
function hideElement(el) {
  el.classList.add('hide');
}
function showElement(el) {
  el.classList.remove('hide');
}
function isTrainerMatches(criterion, trainerNode) {
  const { name, discipline } = trainerNode.dataset;
  if (criterion.search && !name.toLowerCase().includes(criterion.search)) {
    return false;
  }
  return !(
    criterion.disciplines.length > 0 &&
    !criterion.disciplines.includes(discipline)
  );
}
function generateCriterion(input, boxes) {
  return {
    search: input.value.toLowerCase(),
    disciplines: Array.from(boxes)
      .filter((e) => e.matches(':checked'))
      .map((e) => e.value),
  };
}
function displayTrainers(criterion, trainersArr) {
  for (const trainer of trainersArr) {
    if (isTrainerMatches(criterion, trainer)) {
      showElement(trainer);
    } else {
      hideElement(trainer);
    }
  }
}
if (trainersElements) {
  const input = trainersElements.querySelector('.js-trainers-input');
  const checkboxes = trainersElements.querySelectorAll('[name="filter"]');
  const btn = trainersElements.querySelector('.js-trainers-btn');
  const arrTrainers = Array.from(
    trainersElements.querySelectorAll('[data-name]')
  );
  checkboxes.forEach((el) => {
    el.addEventListener('change', () => {
      const criterion = generateCriterion(input, checkboxes);
      displayTrainers(criterion, arrTrainers);
    });
  });
  input.addEventListener('input', () => {
    if (btn) {
      hideElement(btn);
    }
    const criterion = generateCriterion(input, checkboxes);
    displayTrainers(criterion, arrTrainers);
  });
  if (btn) {
    btn.addEventListener('click', () => {
      for (const item of arrTrainers) {
        showElement(item);
      }
    });
  }
}
