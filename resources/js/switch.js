const switchElement = document.querySelector('.js-switch-slider');
if (switchElement && switchElement.dataset.for) {
  const radiosInput = document.querySelectorAll(
    `[name=${switchElement.dataset.for}]`
  );
  switchElement.addEventListener('click', () => {
    for (const item of radiosInput) {
      if (!item.checked) {
        item.click();
        break;
      }
    }
  });
}
