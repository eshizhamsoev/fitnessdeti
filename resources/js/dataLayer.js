window.dataLayer = window.dataLayer || [];

export function sendLead(formData) {
  window.dataLayer.push({
    event: 'lead',
    data: Object.fromEntries(formData.entries()),
  });
}

export function sendLeadFailed(formData) {
  window.dataLayer.push({
    event: 'lead',
    data: Object.fromEntries(formData.entries()),
  });
}
