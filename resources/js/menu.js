import 'mmenu-js';
import 'mmenu-js/dist/mmenu.css';

document.addEventListener('DOMContentLoaded', () => {
  const menu = document.querySelector('#my-menu');
  if (!menu) {
    return;
  }
  const mmenu = document.querySelector('[data-mmenu]').content.cloneNode(true);
  window.mmenu = new window.Mmenu(
    '#my-menu',
    {
      extensions: ['pagedim-black', 'shadow-panels', 'effect-slide-menu'],
      navbars: [
        {
          content: [mmenu],
        },
      ],
    },
    {
      // configuration
      classNames: {
        fixedElements: {
          fixed: 'fixed',
        },
      },
    }
  );
});
