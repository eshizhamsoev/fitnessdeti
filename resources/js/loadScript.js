export function loadScript(url) {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.src = url;
    document.head.append(script);
    script.onload = resolve;
    script.onerror = reject;
  });
}
