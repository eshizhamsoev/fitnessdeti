import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { Fancybox } from '@fancyapps/ui';

const screenWidth = document.documentElement.clientWidth;
const axios = require('axios');

function nextMonth(months) {
  for (const month of months) {
    month.setMonth(month.getMonth() + 1);
  }
}

function prevMonth(months) {
  for (const month of months) {
    month.setMonth(month.getMonth() - 1);
  }
}

async function showDate(date) {
  const response = await axios.get(`/api/competitions-by-date/${date}`);

  // eslint-disable-next-line no-new
  new Fancybox([
    {
      src: response.data.html,
      type: 'html',
    },
  ]);
}

export class MyCalendar {
  constructor(node) {
    this.node = node;
    this.currentDate = new Date();
    this.prevDate = new Date();
    this.nextDate = new Date();
    this.months = [this.prevDate, this.currentDate, this.nextDate];
  }

  initMonths() {
    this.prevDate.setMonth(this.prevDate.getMonth() - 1);
    this.nextDate.setMonth(this.nextDate.getMonth() + 1);
  }

  initCalendar() {
    const calendars = this.node.querySelectorAll('.calendar__full');
    const events = window[this.node.dataset.sourceName];
    for (const [index, calendarFull] of calendars.entries()) {
      const calendar = new Calendar(calendarFull, {
        plugins: [dayGridPlugin, interactionPlugin],
        initialView: 'dayGridMonth',
        headerToolbar: {
          left: false,
          center: 'title',
          right: false,
        },
        locale: 'ru',
        firstDay: 1,
        initialDate: this.months[index],
        height: 'auto',
        dateClick: (e) => {
          if (events.some((item) => item.start === e.dateStr)) {
            showDate(e.dateStr);
          }
        },
        eventClick: (e) => {
          showDate(e.event.id);
        },
        events,
      });
      if (screenWidth <= 1024) {
        calendar.setOption('headerToolbar', { left: 'prev', right: 'next' });
      }
      calendar.render();
    }
    this.node.querySelectorAll('.fc-daygrid-event-harness').forEach((e) => {
      const elem = e.parentElement.parentElement.parentElement;
      elem.style.backgroundColor = 'var(--color-green)';
      elem.style.color = 'var(--color-white)';
    });
  }

  initCalendarButtons() {
    const buttonsNext = document.querySelectorAll('.calendar__next');
    const buttonsPrev = document.querySelectorAll('.calendar__prev');
    for (const button of buttonsNext) {
      button.addEventListener('click', () => {
        nextMonth(this.months);
        this.initCalendar(this.node);
      });
    }
    for (const button of buttonsPrev) {
      button.addEventListener('click', () => {
        prevMonth(this.months);
        this.initCalendar(this.node);
      });
    }
  }

  init() {
    this.initMonths();
    this.initCalendar();
    this.initCalendarButtons();
  }
}
