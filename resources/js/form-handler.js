import axios from 'axios';
import { Fancybox } from '@fancyapps/ui';
import { sendLead, sendLeadFailed } from './dataLayer';

function disableInputs(inputs) {
  for (const input of inputs) {
    input.setAttribute("disabled", "disabled")
  }
}

function enableInputs(inputs) {
  for (const input of inputs) {
    input.removeAttribute("disabled")
  }
}

class Loader {
  constructor(element) {
    this.element = element
    this.innerElement = element.innerHTML
  }

  add() {
    this.element.disabled = true
    this.element.innerHTML = document.querySelector('[data-btn-send]').innerHTML
  }

  remove() {
    this.element.innerHTML = this.innerElement
  }
}

document.addEventListener('submit', (e) => {
  const form = e.target;
  if (!form.classList.contains('js-form')) {
    return;
  }
  e.preventDefault();
  form.dataset.processing = 'true';
  const alert = form.querySelector('.js-form-alert');
  const buttons = form.querySelectorAll('[type="submit"]');
  const currentBtn = form.querySelector('.js-form-btn')
  const inputs = form.querySelectorAll('input')
  const fancyModal = form.closest('.js-fancy-modal')
  const formData = new FormData(form);
  const loader = new Loader(currentBtn)

  disableInputs(inputs)
  disableInputs(buttons)
  loader.add()
  axios
    .post(form.action, formData)
    .then(() => {
      Fancybox.close([{src: fancyModal, type: 'inline'}])
      new Fancybox([
        {
          src: '#fancy-modal-success',
          type: 'inline',
        },
      ]);
      sendLead(formData);
    })
    .catch((error) => {
      if (error.response.status !== 422) {
        throw new Error('Wrong response');
      }
      alert.classList.add('fancy-modal__alert')
      alert.textContent = Object.values(error.response.data.errors)
        .flat()
        .join(' ');
    })
    .catch(() => {
      new Fancybox([
        {
          src: '#fancy-modal-error',
          type: 'inline',
        },
      ]);
      sendLeadFailed(formData);
    })
    .finally(() => {
      form.dataset.processing = null;
      enableInputs(inputs)
      enableInputs(buttons)
      loader.remove()
    });
});
