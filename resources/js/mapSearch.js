function createFindItem(item) {
  const element = document
    .querySelector('[data-find-item-template]')
    .content.cloneNode(true);
  if (element) {
    element
      .querySelector('[data-find-id]')
      .setAttribute('data-find-id', item.id);

    const links = element.querySelectorAll(
      '[data-template-action="main-link"]'
    );
    for (const link of links) {
      link.setAttribute('href', item.link);
    }

    const metroBuildingIcon = element.querySelector("[data-find-item='icon']");
    const metroBuildingIconUse = metroBuildingIcon.querySelector('use');
    const spritePath = metroBuildingIconUse
      .getAttribute('xlink:href')
      .split('#')[0];

    if (item.metro) {
      metroBuildingIcon.setAttribute(
        'class',
        'school-card__icon school-card__icon_metro'
      );
      metroBuildingIconUse.setAttribute('xlink:href', `${spritePath}#metro`);
    } else {
      metroBuildingIcon.setAttribute('class', 'school-card__icon');
      metroBuildingIconUse.setAttribute('xlink:href', `${spritePath}#building`);
    }

    element.querySelector("[data-find-item='name']").textContent = item.name;
    element.querySelector("[data-find-item='address']").textContent =
      item.address;
    element
      .querySelector("[data-find-item='tel']")
      .setAttribute('href', `tel:${item.phone.number}`);
    element.querySelector("[data-find-item='tel']").textContent =
      item.phone.formatted;
    element
      .querySelector("[data-find-item='whatsapp']")
      .setAttribute('href', item.whatsapp);
    element
      .querySelector("[data-find-item='telegram']")
      .setAttribute('href', item.telegram);
  }
  return element;
}

function appendFindItems(elements, arr) {
  const items = elements.querySelectorAll('.js-school-card');
  const itemsWrapper = elements.querySelector('.js-find-us__items');

  for (const item of items) {
    item.remove();
  }
  for (const itemArr of arr) {
    itemsWrapper.append(createFindItem(itemArr));
  }
}

function getMapMarkers(addresses) {
  const mark = document.querySelector('[data-my-placemark]').innerHTML;
  const animatedLayout = window.ymaps.templateLayoutFactory.createClass(mark, {
    build() {
      animatedLayout.superclass.build.call(this);
      this.getData().options.set('shape', {
        type: 'Circle',
        coordinates: [0, -30],
        radius: 30,
      });
    },
  });

  const array = addresses.map((item) => [
    item.id,
    new window.ymaps.Placemark(
      [item.latitude, item.longitude],
      {
        balloonContentHeader: `<div class='balloon__title'>Спортивная школа FD - м.${item.name}</div>`,
        balloonContentBody: `<div >${item.address}</div><div class='balloon__text'>Позвонить <a class='a-clear balloon__link' href='tel:${item.phone.number}'>${item.phone.formatted}</a> перейти к <a href='${item.link}' class='a-clear balloon__link'>школе</a></div>`,
      },
      {
        iconLayout: animatedLayout,
      }
    ),
  ]);
  return new Map(array);
}

function createCollectionPlaceMarkers(mapPlaceMarkers) {
  const myCollection = new window.ymaps.GeoObjectCollection({}, {});

  for (const value of mapPlaceMarkers.values()) {
    myCollection.add(value);
  }
  return myCollection;
}

async function getPlaceMarkNode(placeMark){
  const overlay = await placeMark.getOverlay()
  return overlay?.getElement()?.querySelector('.my-placemark')
}

function initHoveredAddress(elements, mapPlaceMarkers) {
  const items = elements.querySelector('.js-find-us__items');
  items.addEventListener(
    'mouseenter',
    async (event) => {
      const item = event.target;
      if (item.matches('.school-card')) {
        const id = Number(item.dataset.findId);
        const placeMark = mapPlaceMarkers.get(id);
        const placeMarkNode = await getPlaceMarkNode(placeMark)
        placeMark.options.set('zIndex', 10000)
        placeMarkNode.classList.add('active')
      }
    },
    true
  );
  items.addEventListener(
    'mouseleave',
    async (event) => {
      const item = event.target;
      if (item.matches('.school-card')) {
        const id = Number(item.dataset.findId);
        const placeMark = mapPlaceMarkers.get(id);
        const placeMarkNode = await getPlaceMarkNode(placeMark)
        placeMark.options.set('zIndex', '')
        placeMarkNode.classList.remove('active')
      }
    },
    true
  );
}

function initClickPlaceMarker(mapMarkers) {
  for (const [key, value] of mapMarkers) {
    value.events.add('click', () => {
      // eslint-disable-next-line no-console
      console.log(key);
    });
  }
}

function initEventsToMarkers(elements, mapPlaceMarkers) {
  initHoveredAddress(elements, mapPlaceMarkers);
  initClickPlaceMarker(mapPlaceMarkers);
}

function updateCollection(collection, mapPlaceMarkers, arrAddress) {
  collection.removeAll();
  arrAddress
    .map((item) => mapPlaceMarkers.get(item.id))
    .forEach((item) => collection.add(item));
}

function addressFilter(array, value) {
  return array.filter(
    (item) =>
      item.name.toUpperCase().includes(value) ||
      item.address.toUpperCase().includes(value)
  );
}

function showFilteredAddress(
  input,
  arr,
  elements,
  collection,
  mapPlaceMarkers
) {
  let filteredArr = [];
  const inputValue = input.value.trim().toUpperCase();
  if (!inputValue) {
    input.classList.add('error');
    return;
  }
  filteredArr = addressFilter(arr, inputValue);
  if (filteredArr.length === 0) {
    input.classList.add('error');
  } else {
    appendFindItems(elements, filteredArr);
    updateCollection(collection, mapPlaceMarkers, filteredArr);
  }
}

function initSearch(elements, arr, collection, mapPlaceMarkers) {
  const btn = elements.querySelector('.js-find-us__btn');
  const input = elements.querySelector('.js-find-us__input');
  const form = elements.querySelector('.js-find-us-form');

  input.onfocus = () => input.classList.remove('error');

  btn.addEventListener('click', () => {
    showFilteredAddress(input, arr, elements, collection, mapPlaceMarkers);
  });
  form.addEventListener('submit', (event) => {
    event.preventDefault();
    showFilteredAddress(input, arr, elements, collection, mapPlaceMarkers);
  });
}

function createYmap(elements, addresses) {
  const findUsElement = elements.querySelector('.js-find-us__map');

  return findUsElement
    ? new window.ymaps.Map(findUsElement, {
        center: [addresses[0].latitude, addresses[0].longitude],
        zoom: 11,
        controls: [],
      })
    : false;
}

function initMap() {
  const node = document.querySelector('.js-find-us');

  if (!node) {
    return;
  }
  const arrAddresses = window[node.dataset.sourceName];
  const myMap = createYmap(node, arrAddresses);
  const mapPlaceMarkers = getMapMarkers(arrAddresses);
  const myCollection = createCollectionPlaceMarkers(mapPlaceMarkers);

  myMap.geoObjects.add(myCollection);
  if (mapPlaceMarkers.size > 1) {
    myMap.setBounds(myMap.geoObjects.getBounds(), {
      checkZoomRange: true,
      zoomMargin: 50,
    });
  }
  initEventsToMarkers(node, mapPlaceMarkers);
  initSearch(node, arrAddresses, myCollection, mapPlaceMarkers);
}

export function showMap() {
  return new Promise((resolve) => {
    if (window.ymaps) {
      resolve(window.ymaps.ready(initMap));
    }
  });
}
