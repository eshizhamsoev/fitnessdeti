import '@fancyapps/ui/dist/fancybox.css';
import { Fancybox } from '@fancyapps/ui';

Fancybox.bind('[data-src="#header__cities"]', {});

const fancyRequests = document.querySelectorAll('.fancy-modal');
for (const item of fancyRequests) {
  const checkbox = item.querySelector('input[type="checkbox"]');
  const btn = item.querySelector('.fancy-modal__btn');
  if (!checkbox.checked) {
    btn.disabled = true;
  }
  checkbox.addEventListener('click', () => {
    btn.disabled = !checkbox.checked;
  });
}


function openRequestModal(source, heading, leading){
  const modalElement = document.querySelector('.js-fancy-modal');
  modalElement.querySelector('.js-source').value = source;
  modalElement.querySelector('.js-heading').innerHTML =
    heading || 'Позвонить мне';
  modalElement.querySelector('.js-leading').innerHTML =
    leading || 'Оставьте заявку и мы с вами свяжемся';
  Fancybox.show([
    {
      src: '#fancy-modal',
      type: 'inline',
    },
  ]);
}

document.body.addEventListener('click', (element) => {
  const { target } = element;
  if (target.closest('.js-form-modal-trigger')) {
    const { formHeading, formLeading, formSource } = target.dataset;
    openRequestModal(formSource, formHeading, formLeading)
  }
});

if(location.hash === '#open-form'){
  openRequestModal('Открыта по ссылке')
} else if(location.hash === '#open-first-try-form') {
  openRequestModal('Записаться на пробное занятие. Открыта по ссылке', 'Запись на пробное занятие', 'Оставьте заявку и запишитесь на пробное занятие')
}
