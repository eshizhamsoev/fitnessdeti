import Swiper, { Navigation, Pagination, Scrollbar, Autoplay } from 'swiper';

const screenWidth = window.screen.width;

const options = {
  main: {
    modules: [Pagination],
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
    spaceBetween: 10,
  },
  general: {
    modules: [Pagination, Autoplay],
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 30,
      },
    },
  },
  students: {
    modules: [Pagination],
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
    },
  },
  classes: {
    modules: [Scrollbar, Autoplay],
    scrollbar: {
      el: '.swiper-scrollbar',
      hide: true,
    },
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
    },
  },
};

if (screenWidth > 768) {
  options.reviews = {
    modules: [],
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
    },
  };
  options.department = {
    modules: [],
    slidesPerView: 4,
    spaceBetween: 40,
  };
  options.talisman = {
    modules: [],
    slidesPerView: 4,
    spaceBetween: 11,
  };
} else if (screenWidth <= 768) {
  options.bannerGymnastic = {
    modules: [Pagination],
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
    spaceBetween: 10,
  };
}

function generateOptions(name) {
  return options[name];
}

function initSlider(node) {
  const name = node.dataset.swiperType;
  const newOptions = generateOptions(name);
  if (!newOptions) {
    return;
  }
  const swiper = node.querySelector('.swiper');
  const prevEl = node.querySelector('.carousel-arrow_prev');
  const nextEl = node.querySelector('.carousel-arrow_next');

  if (prevEl && nextEl) {
    newOptions.modules.push(Navigation);
    newOptions.navigation = {
      prevEl,
      nextEl,
    };
  }
  new Swiper(swiper, newOptions);
}

const sliders = document.querySelectorAll('[data-swiper-type]');
for (const slider of sliders) {
  initSlider(slider);
}
