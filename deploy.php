<?php
/** @noinspection PhpIncludeInspection */
/** @noinspection PhpMethodParametersCountMismatchInspection */
/** @noinspection PhpUndefinedFunctionInspection */

namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/rsync.php';

set('http_user', getenv('USERNAME'));
set('writable_use_sudo', false);

// Shared files/dirs between deploys
add('shared_files', ['.env', 'public/.htaccess']);
set('shared_dirs', ['storage/app', 'storage/logs', 'storage/media-library']);


set('rsync_src', function () {
    return __DIR__; // If your project isn't in the root, you'll need to change this.
});

// Configuring the rsync exclusions.
// You'll want to exclude anything that you don't want on the production server.
add('rsync', [
    'exclude' => [
        '.git',
        '/.env',
        '/public/robots.txt',
        '/storage/app', '/storage/logs', '/storage/media-library',
        '/vendor/',
        '/node_modules/',
        '.gitlab-ci.yml',
        'deploy.php',
    ],
]);


// Writable dirs by web server
// hack for a lot of folders in storage/app/public
set('writable_dirs', [
    'bootstrap/cache',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);

set('allow_anonymous_stats', false);

set('bin/php', function () {
    return '/usr/bin/php7.4';
});

set('bin/composer', function () {
    if (test('[ -f {{deploy_path}}/.dep/composer.phar ]')) {
        return '{{bin/php}} {{deploy_path}}/.dep/composer.phar';
    }

    if (commandExist('composer')) {
        return '{{bin/php}} ' . locateBinaryPath('composer');
    }

    run("cd {{deploy_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}");
    run('mv {{deploy_path}}/composer.phar {{deploy_path}}/.dep/composer.phar');
    return '{{bin/php}} {{deploy_path}}/.dep/composer.phar';
});

host('production')
    ->hostname(getenv('HOSTNAME'))
    ->user(getenv('USERNAME'))
    ->port('22')
    ->forwardAgent(true)
    ->set('deploy_path', getenv('DEPLOY_PATH'))
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no');

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync', // Deploy code & built assets
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link', // |
    'artisan:view:cache',   // |
    'artisan:config:cache', // | Laravel specific steps
    'artisan:optimize',     // |
    'artisan:migrate',      // |
    'deploy:symlink',
    'deploy:unlock',
    'cleanup'
]);

// Recipe modifiers:

// Unlock after failed deploy process
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');
