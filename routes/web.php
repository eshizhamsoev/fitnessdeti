<?php

use App\Http\Controllers\CompetitionsController;
use App\Http\Controllers\LeadsController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\VacancyApplicationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/api/competitions-by-date/{date}', [CompetitionsController::class ,'index']);

Route::post('/leads', [LeadsController::class, 'store'])->name('leads.store')->middleware('throttle');

Route::post('/vacancy-application', [VacancyApplicationsController::class, 'send'])->name('vacancy.send')->middleware('throttle');

Route::get('{fullUrl}', [PageController::class, 'index'])->where('fullUrl', '^(?!nova).*')->middleware('utm');




