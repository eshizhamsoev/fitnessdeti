<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCitiesColumnToPageBlocks extends Migration
{
    public function up()
    {
        Schema::table('page_blocks', function (Blueprint $table) {
            $table->text('cities')->nullable();
        });
    }

    public function down()
    {
        Schema::table('page_blocks', function (Blueprint $table) {
            $table->dropColumn('cities');
        });
    }
}
