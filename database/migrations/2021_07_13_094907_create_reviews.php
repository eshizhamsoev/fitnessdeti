<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('announce');
            $table->text('text');
            $table->foreignId('discipline_id')->nullable()->constrained('disciplines');
            $table->foreignId('coach_id')->nullable()->constrained('coaches');
            $table->foreignId('school_id')->nullable()->constrained('schools');
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
