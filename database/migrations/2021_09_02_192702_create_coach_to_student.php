<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachToStudent extends Migration
{
    public function up()
    {
        Schema::create('coach_to_student', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignId('student_id')->constrained('students');
            $table->foreignId('coach_id')->constrained('coaches');
            $table->unique(['coach_id', 'student_id']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('coach_to_student');
    }
}
