<?php

use App\Enums\Website\DisciplineColor;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorToDisciplinesTable extends Migration
{
    public function up()
    {
        Schema::table('disciplines', function (Blueprint $table) {
            $table->enum('color', [DisciplineColor::PINK, DisciplineColor::BLUE])->default(DisciplineColor::PINK);
        });
    }

    public function down()
    {
        Schema::table('disciplines', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
