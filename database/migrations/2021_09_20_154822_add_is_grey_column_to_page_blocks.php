<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsGreyColumnToPageBlocks extends Migration
{
    public function up()
    {
        Schema::table('page_blocks', function (Blueprint $table) {
            $table->boolean('is_grey')->default(false);
        });
    }

    public function down()
    {
        Schema::table('page_blocks', function (Blueprint $table) {
            $table->dropColumn('is_grey');
        });
    }
}
