<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDbConstrainsToUrlToCityTable extends Migration
{
    public function up()
    {
        Schema::table('url_to_city', function (Blueprint $table) {
            $table->dropForeign(['url_id']);
            $table->foreign('url_id')
                ->references('id')
                ->on('urls')
                ->onDelete('cascade');

            $table->dropForeign(['city_id']);
            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('url_to_city', function (Blueprint $table) {
            //
        });
    }
}
