<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateEndColumnToCompetitions extends Migration
{
    public function up()
    {
        Schema::table('competitions', function (Blueprint $table) {
            $table->renameColumn('date', 'date_start');
            $table->date('date_end')->nullable();
            $table->enum('competition_type', ['ranked', 'commercial', 'unknown'])->change();
        });
    }

    public function down()
    {
        Schema::table('competitions', function (Blueprint $table) {
            //
        });
    }
}
