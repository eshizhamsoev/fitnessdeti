<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoaches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coaches', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->foreignId('discipline_id')->constrained('disciplines');
            $table->string('youtube_code')->nullable();
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->integer('medal_count')->nullable();
            $table->integer('child_count')->nullable();
            $table->integer('sport_category_count')->nullable();
            $table->enum('coach_type', ['senior', 'ordinary']);
            $table->enum('coach_class', ['unknown', 'kms', 'ms', 'zms', 'msmk'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coaches');
    }
}
