<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResumeToCoachesTable extends Migration
{
    public function up()
    {
        Schema::table('coaches', function (Blueprint $table) {
            $table->json('resume')->nullable();
        });
    }

    public function down()
    {
        Schema::table('coaches', function (Blueprint $table) {
            $table->dropColumn('resume');
        });
    }
}
