<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacancyApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancy_applications', function (Blueprint $table) {
            $table->id();
            $table->string('city');
            $table->string('rank')->nullable();
            $table->string('sport_type')->nullable();
            $table->string('phone');
            $table->string('email');
            $table->string('name');
            $table->unsignedInteger('age')->nullable();
            $table->string('education')->nullable();
            $table->text('experience')->nullable();
            $table->text('about_applicant')->nullable();
            $table->json('utm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancy_applications');
    }
}
