<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServices extends Migration
{
    public function up()
    {
        Schema::create('training_groups', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignId('city_id')->constrained('cities');

            $table->string('name');
            $table->text('description');

            $table->integer('base_price');
            $table->string('type');
            $table->enum('time_unit_for_price', ['month', 'lesson']);

            $table->string('age')->nullable();
            $table->string('capacity')->nullable();
            $table->string('price_per_hour')->nullable();
            $table->text('coaches')->nullable();


            $table->integer('priority')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('training_groups');
    }
}
