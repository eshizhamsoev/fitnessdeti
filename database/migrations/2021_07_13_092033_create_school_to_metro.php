<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolToMetro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_to_metro', function (Blueprint $table) {
            $table->id();
            $table->foreignId('station_id')->constrained('metro_stations');
            $table->foreignId('school_id')->constrained('schools');
            $table->unique(['station_id', 'school_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_to_metro');
    }
}
