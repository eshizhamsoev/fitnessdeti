<?php


namespace App\Services;


class SettingsFields
{
    public const SCHOOL = 'school_page';
    public const COACH = 'coach_page';
    public const BLOG = 'blog_page';
    public const REVIEWS = 'reviews_page';

    public const PRIVACY_PAGE = 'privacy_page';
    public const SAFETY_RULES_PAGE = 'safety_rules';
    public const OFFER_AND_ACCEPTANCE = 'offer_and_acceptance';
    public const LLC = 'llc';
    public const USER_AGREEMENT = 'user_agreement';

    public const SCHOOL_TEMPLATE_PAGE = 'school_template_page';

    public const EMAIL = 'email';

    public const SOCIAL_INSTAGRAM = 'social_instagram';
    public const SOCIAL_YOUTUBE = 'social_youtube';
    public const SOCIAL_FACEBOOK = 'social_facebook';
    public const SOCIAL_VK = 'social_vk';
    public const SOCIAL_TIKTOK = 'social_tiktok';

    public const CODE_IN_HEAD = 'code_in_head';
    public const CODE_IN_BODY_START = 'code_in_body_start';
    public const CODE_IN_BODY_END = 'code_in_body_end';

    public const EMAIL_NOTIFICATION_LEAD = 'email_notification_lead';
    public const NOTIFICATION_EMAIL = 'email';
    public const VACANCY_APPLICATION_EMAIL = 'vacancy_application_email';

    public const DEFAULT_CITY = 'default_city';
}
