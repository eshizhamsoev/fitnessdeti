<?php


namespace App\Services\UrlGenerator;


use App\Services\UrlGenerator\Urlable;
use App\Models\Data\Coach;
use App\Models\Data\School;
use App\Services\SettingsFields;
use App\Services\Transliterator\Transliterator;

class UrlGenerator
{

    private Transliterator $transliterator;

    public function __construct(Transliterator $transliterator)
    {
        $this->transliterator = $transliterator;
    }

    public function makeUrl(Urlable $object)
    {
        $url = '';
        $parentUrl = $object->getParentUrl();
        if ($parentUrl !== null) {
            $url = trim($parentUrl->url, '/');
        }
//        dd([$url, $this->transliterator->transliterate($object->getStringForUrl())]);
        return '/' . trim($url . '/' . $this->transliterator->transliterate($object->getStringForUrl()), '/');
    }

}
