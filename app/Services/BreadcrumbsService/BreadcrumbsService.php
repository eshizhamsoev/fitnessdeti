<?php

namespace App\Services\BreadcrumbsService;

use App\Models\Website\Url;

class BreadcrumbsService
{
    /**
     * @param  Url  $url
     * @return BreadcrumbsItemData[]
     */
    public function getBreadcrumbs(Url $url): array
    {
        $container = new BreadcrumbsContainer();
        if($url->entity instanceof BreadcrumbsVisible) {
            $container->append(
                new BreadcrumbsItemData(
                    $url->entity->getNameForBreadcrumbs(),
                    $url
                )
            );
        }
        while ($url = $url->entity->getParentUrl()) {
            if($url->entity instanceof BreadcrumbsVisible) {
                $container->prepend(
                    new BreadcrumbsItemData(
                        $url->entity->getNameForBreadcrumbs(),
                        $url
                    )
                );
            }
        }
        return $container->getBreadcrumbs();
    }
}
