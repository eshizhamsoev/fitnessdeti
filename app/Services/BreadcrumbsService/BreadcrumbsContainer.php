<?php

namespace App\Services\BreadcrumbsService;

class BreadcrumbsContainer
{
    private array $breadcrumbs = [];

    public function getBreadcrumbs(): array
    {
        return $this->breadcrumbs;
    }

    public function prepend(BreadcrumbsItemData $breadcrumbsItemData)
    {
        array_unshift($this->breadcrumbs, $breadcrumbsItemData);
    }

    public function append(BreadcrumbsItemData $breadcrumbsItemData)
    {
        $this->breadcrumbs[] = $breadcrumbsItemData;
    }
}
