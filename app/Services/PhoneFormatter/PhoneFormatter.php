<?php

namespace App\Services\PhoneFormatter;

class PhoneFormatter
{
    private string $pattern;

    public function __construct(string $pattern = '+7 (???) ??? ?? ??')
    {
        $this->pattern = $pattern;
    }

    public function format(PhoneNumber $phoneNumber): string
    {
        return \Str::replaceArray(
            '?',
            str_split($phoneNumber->getNumberWithoutSeven()),
            $this->pattern
        );
    }
}
