<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\Website\ButtonType;
use App\Models\Website\Gallery;
use App\View\Data\Block\PhotoshootData;
use App\View\Data\Block\WideBannerData;
use R64\NovaFields\Select;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class PhotoshootBlockSettings implements ConfigurableBlockSettings
{
    const HEADING = 'heading';
    const LEADING = 'leading';
    const GALLERY = 'gallery';
    const BUTTON_TEXT = 'buttonText';
    const FORM_HEADING = 'formHeading';
    const FORM_LEADING = 'formLeading';
    const FORM_SOURCE = 'formSource';

    public function getSettings(): array
    {
        return [
            Text::make('Heading', self::HEADING),
            Text::make('Leading', self::LEADING),
            Select::make('Gallery', self::GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))->required(),
            Text::make('Button Text', self::BUTTON_TEXT),
            Text::make('Form Heading', self::FORM_HEADING),
            Text::make('Form Leading', self::FORM_LEADING),
            Text::make('Form Source', self::FORM_SOURCE)
        ];
    }

    public function extractSettings(?array $data): PhotoshootData
    {
        return new PhotoshootData(
            $data[self::HEADING],
            $data[self::LEADING],
            Gallery::find($data[self::GALLERY]),
            $data[self::BUTTON_TEXT],
            $data[self::FORM_HEADING],
            $data[self::FORM_LEADING],
            $data[self::FORM_SOURCE]
        );
    }
}

