<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\TrainingGroupType;
use App\View\Data\Block\TrainingGroupData;
use App\View\Data\Block\TrainingGroupWithSwitchData;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class TrainingGroupWithSwitchBlockSettings implements ConfigurableBlockSettings
{
    private const NAME_LEFT = 'name_left';
    private const TYPE_LEFT = 'type_left';

    private const NAME_RIGHT = 'name_right';
    private const TYPE_RIGHT = 'type_right';
    private const HEADING = 'heading';
    private const LEADING = 'leading';

    public function getSettings(): array
    {
        return [
            Text::make('Heading', self::HEADING),
            Text::make('Leading', self::LEADING),

            Enum::make('Left type', self::TYPE_LEFT)->attach(TrainingGroupType::class),
            Text::make('Left name', self::NAME_LEFT),
            Enum::make('Right type', self::TYPE_RIGHT)->attach(TrainingGroupType::class),
            Text::make('Right name', self::NAME_RIGHT),
        ];
    }

    public function extractSettings(array $data): TrainingGroupWithSwitchData
    {
        return new TrainingGroupWithSwitchData(
            TrainingGroupType::fromValue($data[self::TYPE_LEFT]),
            $data[self::NAME_LEFT],
            TrainingGroupType::fromValue($data[self::TYPE_RIGHT]),
            $data[self::NAME_RIGHT],
            $data[self::HEADING],
            $data[self::LEADING]
        );
    }
}
