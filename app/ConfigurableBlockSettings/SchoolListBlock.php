<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\SchoolListData;
use R64\NovaFields\Boolean;
use R64\NovaFields\Text;

class SchoolListBlock implements ConfigurableBlockSettings
{
    private const SHOW_ALL = 'show_all';
    private const HEADING = 'heading';

    public function getSettings(): array
    {
        return [
            Text::make(__(self::HEADING), self::HEADING),
            Boolean::make(__(self::SHOW_ALL), self::SHOW_ALL),
        ];
    }

    public function extractSettings(array $data): SchoolListData
    {
        return (new SchoolListData(
            $data[self::HEADING]
        ))->setShouldShowAll($data[self::SHOW_ALL]);
    }
}
