<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\InfoBannerSides;
use App\Enums\Website\ButtonType;
use App\Enums\Website\GalleryType;
use App\Models\Website\Gallery;
use App\View\Data\Block\GalleryData;
use App\View\Data\Block\InfoBannerData;
use App\View\Data\Block\WideBannerData;
use R64\NovaFields\Select;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class InfoBannerBlockSettings implements ConfigurableBlockSettings
{

    private const GALLERY = 'gallery';
    private const BUTTON_TYPE = 'buttonType';
    private const BLOCK_NAME = 'blockName';
    private const SIDE = 'side';
    private const BUTTON_TEXT = 'buttonText';
    private const FORM_HEADING = 'formHeading';
    private const FORM_LEADING = 'formLeading';
    private const FORM_SOURCE = 'formSource';




    public function getSettings(): array
    {
        return [
            Text::make('Block Name', self::BLOCK_NAME),
            Select::make('Gallery', self::GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))
                ->nullable(),
            Enum::make('Button Type', self::BUTTON_TYPE)->attach(ButtonType::class),
            Enum::make('Side', self::SIDE)->attach(InfoBannerSides::class),
            Text::make('Button Text', self::BUTTON_TEXT),
            Text::make('Form Heading', self::FORM_HEADING)->nullable(),
            Text::make('Form Leading', self::FORM_LEADING)->nullable(),
            Text::make('Form Source', self::FORM_SOURCE)->nullable()
        ];
    }

    public function extractSettings(?array $data): InfoBannerData
    {
        return new InfoBannerData(
            $data[self::BLOCK_NAME],
            InfoBannerSides::fromValue($data[self::SIDE]),
            ButtonType::fromValue($data[self::BUTTON_TYPE]),
            Gallery::find($data[self::GALLERY]),
            $data[self::BUTTON_TEXT],
            $data[self::FORM_HEADING],
            $data[self::FORM_LEADING],
            $data[self::FORM_SOURCE]
        );
    }
}
