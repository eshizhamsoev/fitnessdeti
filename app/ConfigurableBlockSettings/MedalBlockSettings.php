<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\MedalData;
use R64\NovaFields\Text;

class MedalBlockSettings implements ConfigurableBlockSettings
{
    private const GOLD = 'gold';
    private const SILVER = 'silver';
    private const BRONZE = 'bronze';
    private const BUTTON = 'button';

    public function getSettings(): array
    {
        return [
            Text::make(__(self::GOLD), self::GOLD),
            Text::make(__(self::SILVER), self::SILVER),
            Text::make(__(self::BRONZE), self::BRONZE),
            Text::make(__(self::BUTTON), self::BUTTON),
        ];
    }

    public function extractSettings(?array $data): MedalData
    {
        return new MedalData(
            $data[self::GOLD] ?? '',
            $data[self::SILVER] ?? '',
            $data[self::BRONZE] ?? '',
            $data[self::BUTTON] ?? null,
        );
    }
}
