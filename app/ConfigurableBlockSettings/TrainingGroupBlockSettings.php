<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\TrainingGroupType;
use App\View\Data\Block\TrainingGroupData;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class TrainingGroupBlockSettings implements ConfigurableBlockSettings
{
    private const TYPE = 'type';
    private const HEADING = 'heading';
    private const LEADING = 'leading';

    public function getSettings(): array
    {
        return [
            Enum::make('Type', self::TYPE)->attach(TrainingGroupType::class),
            Text::make('Heading', self::HEADING),
            Text::make('Leading', self::LEADING),
        ];
    }

    public function extractSettings(array $data): TrainingGroupData
    {
        return new TrainingGroupData(
            TrainingGroupType::fromValue($data[self::TYPE]),
            $data[self::HEADING],
            $data[self::LEADING]
        );
    }
}
