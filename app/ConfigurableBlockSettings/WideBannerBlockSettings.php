<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\Website\ButtonType;
use App\Enums\Website\GalleryType;
use App\Enums\Website\WideBannerDesktopImagePosition;
use App\Models\Website\Gallery;
use App\View\Data\Block\GalleryData;
use App\View\Data\Block\WideBannerData;
use R64\NovaFields\Boolean;
use R64\NovaFields\Select;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class WideBannerBlockSettings implements ConfigurableBlockSettings
{

    private const DESKTOP_GALLERY = 'desktopGallery';
    private const MOBILE_GALLERY = 'mobileGallery';
    private const BUTTON_TYPE = 'buttonType';
    private const BLOCK_NAME = 'blockName';
    private const BUTTON_TEXT = 'buttonText';
    private const FORM_HEADING = 'formHeading';
    private const FORM_LEADING = 'formLeading';
    private const FORM_SOURCE = 'formSource';
    private const DESKTOP_IMAGE_POSITION = 'image_position';




    public function getSettings(): array
    {
        return [
            Text::make('Block Name', self::BLOCK_NAME),
            Select::make('Desktop Gallery', self::DESKTOP_GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('Mobile Gallery', self::MOBILE_GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))
                ->nullable(),
            Enum::make('Desktop Image Position', self::DESKTOP_IMAGE_POSITION)->attach(WideBannerDesktopImagePosition::class),
            Enum::make('Button Type', self::BUTTON_TYPE)->attach(ButtonType::class),
            Text::make('Button Text', self::BUTTON_TEXT),
            Text::make('Form Heading', self::FORM_HEADING),
            Text::make('Form Leading', self::FORM_LEADING),
            Text::make('Form Source', self::FORM_SOURCE)
        ];
    }

    public function extractSettings(?array $data): WideBannerData
    {
        return new WideBannerData(
            $data[self::BLOCK_NAME],
            Gallery::find($data[self::DESKTOP_GALLERY]),
            Gallery::find($data[self::MOBILE_GALLERY]),
            ButtonType::fromValue($data[self::BUTTON_TYPE]),
            $data[self::BUTTON_TEXT],
            $data[self::FORM_HEADING],
            $data[self::FORM_LEADING],
            $data[self::FORM_SOURCE],
            !empty($data[self::DESKTOP_IMAGE_POSITION]) ? WideBannerDesktopImagePosition::fromValue($data[self::DESKTOP_IMAGE_POSITION]) : null
        );
    }
}
