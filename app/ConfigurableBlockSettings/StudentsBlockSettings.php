<?php

namespace App\ConfigurableBlockSettings;

use App\Models\Website\Gallery;
use App\View\Data\Block\StudentsBlockData;
use R64\NovaFields\Select;

class StudentsBlockSettings implements ConfigurableBlockSettings
{
    const GALLERY = 'gallery';

    public function getSettings(): array
    {
        return [
            Select::make('Gallery', self::GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))
                ->nullable()
        ];
    }

    public function extractSettings(?array $data) :StudentsBlockData
    {
        return new StudentsBlockData(Gallery::find($data[self::GALLERY]));
    }
}
