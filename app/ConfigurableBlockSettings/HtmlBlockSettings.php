<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\HtmlData;
use R64\NovaFields\Text;
use R64\NovaFields\Textarea;

class HtmlBlockSettings implements ConfigurableBlockSettings
{
    private const BLOCK_NAME = 'blockName';
    private const HTML_CODE = 'htmlCode';


    public function getSettings(): array
    {
        return [
            Text::make('Block Name', self::BLOCK_NAME),
            Textarea::make('HTML code', self::HTML_CODE)
        ];
    }

    public function extractSettings(?array $data): HtmlData
    {
        return new HtmlData($data[self::HTML_CODE]);
    }
}
