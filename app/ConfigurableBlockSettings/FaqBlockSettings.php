<?php

namespace App\ConfigurableBlockSettings;

use App\Models\Website\Faq;
use App\View\Data\Block\FaqData;
use R64\NovaFields\Select;

class FaqBlockSettings implements ConfigurableBlockSettings
{
    private const FAQ = 'faq';

    public function getSettings(): array
    {
        return [
            Select::make('Faq', self::FAQ)
                ->options(Faq::all()->pluck('name', 'id'))
                ->nullable(),
        ];
    }

    public function extractSettings(?array $data): FaqData
    {
        $faq = Faq::find($data[self::FAQ]);

        return new FaqData(
            $faq,
        );
    }
}
