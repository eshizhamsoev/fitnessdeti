<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\Website\GalleryType;
use App\Models\Website\Gallery;
use App\View\Data\Block\GalleryData;
use R64\NovaFields\Select;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class GalleryBlockSettings implements ConfigurableBlockSettings
{

    private const GALLERY = 'gallery';
    private const GALLERY_TYPE = 'galleryType';
    private const BLOCK_NAME = 'blockName';

    public function getSettings(): array
    {
        return [
            Select::make('Gallery', self::GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))
                ->nullable(),
            Enum::make('Gallery Type', self::GALLERY_TYPE)->attach(GalleryType::class),
            Text::make('Block Name', self::BLOCK_NAME)
        ];
    }

    public function extractSettings(?array $data): GalleryData
    {
        return new GalleryData(
            Gallery::find($data[self::GALLERY]), GalleryType::fromValue($data[self::GALLERY_TYPE]), $data[self::BLOCK_NAME]
        );
    }
}
