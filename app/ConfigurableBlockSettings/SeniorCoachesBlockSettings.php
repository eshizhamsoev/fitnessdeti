<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\SeniorCoachesData;
use R64\NovaFields\Text;

class SeniorCoachesBlockSettings implements ConfigurableBlockSettings
{
    private const HEADING = 'heading';

    public function getSettings(): array
    {
        return [
            Text::make('Heading', self::HEADING)
        ];
    }

    public function extractSettings(?array $data): SeniorCoachesData
    {
        return new SeniorCoachesData($data[self::HEADING]);
    }
}

