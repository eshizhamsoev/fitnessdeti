<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\MarkdownData;
use R64\NovaFields\Text;
use R64\NovaFields\Textarea;

class MarkdownBlockSettings implements ConfigurableBlockSettings
{
    private const MARKDOWN_TEXT = 'markdownText';


    public function getSettings(): array
    {
        return [
            Textarea::make('Markdown text', self::MARKDOWN_TEXT)
        ];
    }

    public function extractSettings(?array $data): MarkdownData
    {
        return new MarkdownData($data[self::MARKDOWN_TEXT]);
    }
}
