<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\Website\BlockType;

class SettingResolver
{
    const CLASSES = [
        BlockType::KPI => KpiBlockSettings::class,
        BlockType::FEDERATION => FederationBlockSettings::class,
        BlockType::SCHOOL_LIST => SchoolListBlock::class,
        BlockType::FAQ => FaqBlockSettings::class,
        BlockType::GALLERY => GalleryBlockSettings::class,
        BlockType::WIDE_BANNER => WideBannerBlockSettings::class,
        BlockType::MEDAL => MedalBlockSettings::class,
        BlockType::TRAINING_GROUP => TrainingGroupBlockSettings::class,
        BlockType::TRAINING_GROUP_WITH_SWITCH => TrainingGroupWithSwitchBlockSettings::class,
        BlockType::HTML_CODE => HtmlBlockSettings::class,
        BlockType::COACHES => CoachesBlockSettings::class,
        BlockType::TIMETABLE => TimetableBlockSettings::class,
        BlockType::SENIOR_COACHES => SeniorCoachesBlockSettings::class,
        BlockType::MARKDOWN => MarkdownBlockSettings::class,
        BlockType::BREADCRUMBS => BreadcrumbsBlockSettings::class,
        BlockType::INFO_BANNER => InfoBannerBlockSettings::class,
        BlockType::BLOG => BlogBlockSettings::class,
        BlockType::SENIOR_TRAINERS_PROPERTIES => SeniorTrainersPropertiesBlockSettings::class,
        BlockType::INTERNAL_TRAINING => InternalTrainingBlockSettings::class,
        BlockType::COACHING_STAFF => CoachingStaffBlockSettings::class,
        BlockType::CERTIFIED_COACHES => CertifiedCoachesBlockSettings::class,
        BlockType::REVIEWS_SLIDER => ReviewsSliderBlockSettings::class,
        BlockType::REVIEWS_BLOCK => ReviewsBlockBlockSettings::class,
        BlockType::CAMPS => CampsBlockSettings::class,
        BlockType::PHOTSHOOT => PhotoshootBlockSettings::class,
        BlockType::MASTER_CLASSES => MasterClassesBlockSettings::class,
        BlockType::CALENDAR => CalendarBlockSettings::class,
        BlockType::STUDENTS_BLOCK => StudentsBlockSettings::class,
        BlockType::TEAM => TeamBlockSettings::class,
        BlockType::VACANCY_FORM => VacancyFormBlockSettings::class,
        BlockType::MAIN_BANNER => MainBannerBlockSettings::class,
        BlockType::PURPOSE_BANNER => PurposeBannerBlockSettings::class
    ];

    public function resolve(BlockType $blockType): ConfigurableBlockSettings
    {
        $class = self::CLASSES[$blockType->value] ?? null;
        if (!$class) {
            throw new \InvalidArgumentException('No block setting class for block type: ' . $blockType->value);
        }
        return new $class;
    }
}
