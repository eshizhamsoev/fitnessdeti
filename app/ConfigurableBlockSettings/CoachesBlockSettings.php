<?php

namespace App\ConfigurableBlockSettings;

use App\Models\Data\Discipline as DisciplineModel;
use App\Nova\Data\Discipline;
use App\View\Data\Block\CoachesBlockData;
use OptimistDigital\MultiselectField\Multiselect;

class CoachesBlockSettings implements ConfigurableBlockSettings
{
    private const FILTER_DISCIPLINES = 'filter_disciplines';

    public function getSettings(): array
    {
        return [
            Multiselect::make('Disciplines for filtering', self::FILTER_DISCIPLINES)
                ->asyncResource(Discipline::class)
                ->reorderable(true)
        ];
    }

    public function extractSettings(?array $data)
    {
        return new CoachesBlockData(
            !empty($data[self::FILTER_DISCIPLINES]) ?
                DisciplineModel::whereIn('id', json_decode($data[self::FILTER_DISCIPLINES]))->get()->all()
                : []
        );
    }
}

