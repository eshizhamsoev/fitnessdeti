<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\ReviewsData;
use R64\NovaFields\Text;

class ReviewsSliderBlockSettings implements ConfigurableBlockSettings
{
    private const HEADER = 'header';
    private const BUTTON_TEXT = 'buttonText';

    public function getSettings(): array
    {
        return [
            Text::make('Header', self::HEADER)->required(),
            Text::make('Button Text', self::BUTTON_TEXT)
        ];
    }

    public function extractSettings(?array $data): ReviewsData
    {
        return new ReviewsData($data[self::HEADER], $data[self::BUTTON_TEXT]);
    }
}
