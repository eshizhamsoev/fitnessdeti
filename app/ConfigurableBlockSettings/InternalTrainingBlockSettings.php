<?php

namespace App\ConfigurableBlockSettings;

use App\Enums\InternalTrainingTitlePositions;
use App\Enums\Website\ButtonType;
use App\Enums\Website\GalleryType;
use App\Models\Website\Gallery;
use App\View\Data\Block\GalleryData;
use App\View\Data\Block\InternalTrainingData;
use App\View\Data\Block\WideBannerData;
use Laravel\Nova\Fields\Markdown;
use R64\NovaFields\Select;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;

class InternalTrainingBlockSettings implements ConfigurableBlockSettings
{

    private const BLOCK_NAME = 'blockName';
    private const TITLE_POSITION = 'titlePosition';
    private const BUTTON_TEXT = 'buttonText';
    private const HEADING_TEXT = 'headingText';
    private const LEFT_BLOCK_TEXT = 'leftBlockText';
    private const CENTER_BLOCK_TEXT = 'centerBlockText';
    private const RIGHT_BLOCK_TEXT = 'rightBlockText';




    public function getSettings(): array
    {
        return [
            Text::make('Block Name', self::BLOCK_NAME),
            Enum::make('Title Position', self::TITLE_POSITION)->attach(InternalTrainingTitlePositions::class),
            Text::make('Button Text', self::BUTTON_TEXT),
            Text::make('Heading Text', self::HEADING_TEXT),
            Markdown::make('Left Block Text', self::LEFT_BLOCK_TEXT),
            Markdown::make('Center Block Text', self::CENTER_BLOCK_TEXT),
            Markdown::make('Right Block Text', self::RIGHT_BLOCK_TEXT),
        ];
    }

    public function extractSettings(?array $data): InternalTrainingData
    {
        return new InternalTrainingData(
            $data[self::BLOCK_NAME],
            InternalTrainingTitlePositions::fromValue($data[self::TITLE_POSITION]),
            $data[self::BUTTON_TEXT],
            $data[self::HEADING_TEXT],
            $data[self::LEFT_BLOCK_TEXT],
            $data[self::CENTER_BLOCK_TEXT],
            $data[self::RIGHT_BLOCK_TEXT],
        );
    }
}
