<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\FederationData;
use R64\NovaFields\Text;
use R64\NovaFields\Textarea;

class FederationBlockSettings implements ConfigurableBlockSettings
{
    private const GYMNASTIC_HEADING = 'gymnastic_heading';
    private const GYMNASTIC_TEXT = 'gymnastic_text';

    private const ACROBATIC_HEADING = 'acrobatic_heading';
    private const ACROBATIC_TEXT = 'acrobatic_text';

    public function getSettings(): array
    {
        return [
            Text::make(__(self::GYMNASTIC_HEADING), self::GYMNASTIC_HEADING),
            Textarea::make(__(self::GYMNASTIC_TEXT), self::GYMNASTIC_TEXT),
            Text::make(__(self::ACROBATIC_HEADING), self::ACROBATIC_HEADING),
            Textarea::make(__(self::ACROBATIC_TEXT), self::ACROBATIC_TEXT),
        ];
    }

    public function extractSettings(?array $data): FederationData
    {
        return new FederationData(
            $data[self::GYMNASTIC_HEADING] ?? '',
            $data[self::GYMNASTIC_TEXT] ?? '',
            $data[self::ACROBATIC_HEADING] ?? '',
            $data[self::ACROBATIC_TEXT] ?? ''
        );
    }
}
