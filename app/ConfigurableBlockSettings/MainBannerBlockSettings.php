<?php

namespace App\ConfigurableBlockSettings;

use App\Models\Website\Gallery;
use App\View\Data\Block\MainBannerData;
use App\View\Data\Block\WideBannerData;
use R64\NovaFields\Select;

class MainBannerBlockSettings implements ConfigurableBlockSettings
{

    private const GALLERY = 'gallery';

    public function getSettings(): array
    {
        return [
            Select::make('Gallery', self::GALLERY)
                ->options(Gallery::all()->pluck('name', 'id'))
                ->nullable(),
        ];
    }

    public function extractSettings(?array $data): MainBannerData
    {
        return new MainBannerData(
            Gallery::find($data[self::GALLERY]),
        );
    }
}
