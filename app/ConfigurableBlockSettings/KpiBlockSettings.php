<?php

namespace App\ConfigurableBlockSettings;

use App\View\Data\Block\KpiData;
use R64\NovaFields\Text;

class KpiBlockSettings implements ConfigurableBlockSettings
{
    private const MEDALS = 'medals';
    private const RANKS = 'ranks';
    private const CHILDREN = 'children';

    public function getSettings(): array
    {
        return [
            Text::make(__(self::MEDALS), self::MEDALS),
            Text::make(__(self::RANKS), self::RANKS),
            Text::make(__(self::CHILDREN), self::CHILDREN),
        ];
    }

    public function extractSettings(?array $data): KpiData
    {
        return new KpiData(
            $data[self::MEDALS] ?? '',
            $data[self::RANKS] ?? '',
            $data[self::CHILDREN] ?? '',
        );
    }
}
