<?php

namespace App\View\Data\Gallery;

use Spatie\MediaLibrary\MediaCollections\Models\Media;

class GalleryData
{
    private string $title;
    private array $photos;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function addPhoto(Media $media)
    {
        $this->photos[] = $media;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Media[]
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }
}
