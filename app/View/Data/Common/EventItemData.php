<?php

namespace App\View\Data\Common;

use App\Models\Data\Camp;
use App\Models\Data\MasterClass;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class EventItemData
{

    private ?Media $media;
    private string $mediaConversion;
    private string $date;
    private string $name;
    private ?string $location;

    private function __construct(
        ?Media $media,
        string $mediaConversion,
        string $date,
        string $name,
        ?string $location
    ) {

        $this->media = $media;
        $this->mediaConversion = $mediaConversion;
        $this->date = $date;
        $this->name = $name;
        $this->location = $location;
    }

    public static function constructFromCamp(Camp $camp): EventItemData
    {
        $date = self::makeCampDateString($camp->date_from, $camp->date_to);
        $media = $camp->getFirstMedia(Camp::IMAGE_COLLECTION_MAIN);
        $name = $camp->name;
        $location = $camp->location;
        $mediaConversion = Camp::IMAGE_CONVERSION_MAIN;

        return new static($media, $mediaConversion, $date, $name, $location);
    }

    public static function constructFromMasterClass(MasterClass $masterClass): EventItemData
    {
        $date = self::makeMasterClassDateString($masterClass->date, $masterClass->time_from, $masterClass->time_to);
        $media = $masterClass->getFirstMedia(MasterClass::IMAGE_COLLECTION_MAIN);
        $name = $masterClass->name;
        $location = $masterClass->location;
        $mediaConversion = MasterClass::IMAGE_CONVERSION_MAIN;

        return new static($media, $mediaConversion, $date, $name, $location);
    }

    /**
     * @return ?Media
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @return string
     */
    public function getMediaConversion(): string
    {
        return $this->mediaConversion;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    private static function makeCampDateString(Carbon $carbonDateFrom, Carbon $carbonDateTo): string
    {
        Date::setLocale('ru');

        $dateFrom = new Date($carbonDateFrom);
        $dateTo = new Date($carbonDateTo);

        $startDay = $dateFrom->month === $dateTo->month ? $dateFrom->format('d') : $dateFrom->translatedFormat('d F');
        $endDay = $dateTo->translatedFormat('d F');

        $startYear = $dateFrom->format('Y');
        $endYear = $dateTo->format('Y');

        $yearString = implode(' ', $startYear === $endYear ? [$startYear, 'г.'] : [$startYear, '-', $endYear, 'г.']);

        return implode(' ', [$startDay, '-', $endDay, $yearString]);
    }

    private static function makeMasterClassDateString(Carbon $date, ?string $from, ?string $to)
    {
        $parts = [$date->format('Y-m-d')];
        if ($from) {
            $parts[] = 'с';
            $parts[] = substr($from, 0, 5);
        }
        if ($to) {
            $parts[] = 'до';
            $parts[] = substr($to, 0, 5);
        }
        return implode(' ', $parts);
    }
}
