<?php

namespace App\View\Data\Common;

class TabData
{
    private string $buttonText;
    private ?string $link;
    private bool $isActive;

    public function __construct(string $buttonText, bool $isActive, string $link)
    {
        $this->buttonText = $buttonText;
        $this->link = $link;
        $this->isActive = $isActive;
    }


    /**
     * @return string
     */
    public function getButtonText(): string
    {
        return $this->buttonText;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }
}
