<?php

namespace App\View\Data\Common;

use App\Enums\Website\Kpi\KpiColor;
use App\Enums\Website\SvgIconName;

class KpiItemData
{
    private KpiColor $color;
    private SvgIconName $icon;
    private string $amount;
    private string $title;
    private ?string $mobileTitle = null;

    public function __construct(
        KpiColor $color,
        SvgIconName $icon,
        string $amount,
        string $title
    ) {

        $this->color = $color;
        $this->icon = $icon;
        $this->amount = $amount;
        $this->title = $title;
    }

    /**
     * @return KpiColor
     */
    public function getColor(): KpiColor
    {
        return $this->color;
    }

    /**
     * @return SvgIconName
     */
    public function getIcon(): SvgIconName
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getMobileTitle(): ?string
    {
        return $this->mobileTitle ?? $this->title;
    }

    /**
     * @param  string|null  $mobileTitle
     * @return KpiItemData
     */
    public function setMobileTitle(?string $mobileTitle): KpiItemData
    {
        $this->mobileTitle = $mobileTitle;
        return $this;
    }


}
