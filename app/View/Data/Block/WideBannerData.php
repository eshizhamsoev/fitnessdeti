<?php

namespace App\View\Data\Block;

use App\Enums\Website\ButtonType;
use App\Enums\Website\GalleryType;
use App\Enums\Website\WideBannerDesktopImagePosition;
use App\Models\Website\Gallery;

class WideBannerData
{

    private ?string $blockName;
    private ButtonType $buttonType;
    private ?Gallery $desktopGallery;
    private ?Gallery $mobileGallery;
    private ?string $buttonText;
    private ?string $formHeading;
    private ?string $formLeading;
    private ?string $formSource;
    private ?WideBannerDesktopImagePosition $wideBannerDesktopImagePosition;


    public function __construct(
        ?string    $blockName,
        ?Gallery    $desktopGallery,
        ?Gallery    $mobileGallery,
        ButtonType $buttonType,
        ?string    $buttonText,
        ?string    $formHeading,
        ?string    $formLeading,
        ?string    $formSource,
        ?WideBannerDesktopImagePosition    $wideBannerDesktopImagePosition
    )
    {
        $this->blockName = $blockName;
        $this->buttonType = $buttonType;
        $this->desktopGallery = $desktopGallery;
        $this->mobileGallery = $mobileGallery;
        $this->buttonText = $buttonText;
        $this->formHeading = $formHeading;
        $this->formLeading = $formLeading;
        $this->formSource = $formSource;
        $this->wideBannerDesktopImagePosition = $wideBannerDesktopImagePosition;
    }

    /**
     * @return string|null
     */
    public function getBlockName(): ?string
    {
        return $this->blockName;
    }

    /**
     * @return ButtonType
     */
    public function getButtonType(): ButtonType
    {
        return $this->buttonType;
    }

    /**
     * @return Gallery
     */
    public function getDesktopGallery(): ?Gallery
    {
        return $this->desktopGallery;
    }

    /**
     * @return Gallery
     */
    public function getMobileGallery(): ?Gallery
    {
        return $this->mobileGallery;
    }

    /**
     * @return string|null
     */
    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

    /**
     * @return string|null
     */
    public function getFormHeading(): ?string
    {
        return $this->formHeading;
    }

    /**
     * @return string|null
     */
    public function getFormLeading(): ?string
    {
        return $this->formLeading;
    }

    /**
     * @return string|null
     */
    public function getFormSource(): ?string
    {
        return $this->formSource;
    }

    /**
     * @return WideBannerDesktopImagePosition|null
     */
    public function getWideBannerDesktopImagePosition(): ?WideBannerDesktopImagePosition
    {
        return $this->wideBannerDesktopImagePosition;
    }
}
