<?php

namespace App\View\Data\Block;

class MarkdownData
{
    private string $markdownText;

    public function __construct(string $markdownText)
    {
        $this->markdownText = $markdownText;
    }

    /**
     * @return string
     */
    public function getMarkdownText(): string
    {
        return $this->markdownText;
    }

}
