<?php

namespace App\View\Data\Block;

use App\Enums\InternalTrainingTitlePositions;

class InternalTrainingData
{

    private string $blockName;
    private InternalTrainingTitlePositions $titlePosition;
    private ?string $buttonText;
    private string $headingText;
    private string $leftBlockText;
    private string $centerBlockText;
    private string $rightBlockText;

    public function __construct(
        string $blockName,
        InternalTrainingTitlePositions $titlePosition,
        ?string $buttonText,
        string $headingText,
        string $leftBlockText,
        string $centerBlockText,
        string $rightBlockText
    )
    {

        $this->blockName = $blockName;
        $this->titlePosition = $titlePosition;
        $this->buttonText = $buttonText;
        $this->headingText = $headingText;
        $this->leftBlockText = $leftBlockText;
        $this->centerBlockText = $centerBlockText;
        $this->rightBlockText = $rightBlockText;
    }

    /**
     * @return string
     */
    public function getBlockName(): string
    {
        return $this->blockName;
    }

    /**
     * @return InternalTrainingTitlePositions
     */
    public function getTitlePosition(): InternalTrainingTitlePositions
    {
        return $this->titlePosition;
    }

    /**
     * @return string
     */
    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

    /**
     * @return string
     */
    public function getHeadingText(): string
    {
        return $this->headingText;
    }

    /**
     * @return string
     */
    public function getLeftBlockText(): string
    {
        return $this->leftBlockText;
    }

    /**
     * @return string
     */
    public function getCenterBlockText(): string
    {
        return $this->centerBlockText;
    }
    /**
     * @return string
     */
    public function getRightBlockText(): string
    {
        return $this->rightBlockText;
    }
}
