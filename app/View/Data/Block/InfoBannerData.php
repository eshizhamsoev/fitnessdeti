<?php

namespace App\View\Data\Block;

use App\Enums\InfoBannerSides;
use App\Enums\Website\ButtonType;
use App\Enums\Website\GalleryType;
use App\Models\Website\Gallery;

class InfoBannerData
{

    private ?string $blockName;
    private ButtonType $buttonType;
    private ?Gallery $gallery;
    private ?string $buttonText;
    private ?string $formHeading;
    private ?string $formLeading;
    private ?string $formSource;
    private ?string $side;

    public function __construct(
        ?string    $blockName,
        ?InfoBannerSides $side,
        ButtonType $buttonType,
        ?Gallery   $gallery,
        ?string    $buttonText,
        ?string    $formHeading,
        ?string    $formLeading,
        ?string    $formSource
    )
    {
        $this->blockName = $blockName;
        $this->buttonType = $buttonType;
        $this->side = $side;
        $this->gallery = $gallery;
        $this->buttonText = $buttonText;
        $this->formHeading = $formHeading;
        $this->formLeading = $formLeading;
        $this->formSource = $formSource;
    }

    /**
     * @return string|null
     */
    public function getBlockName(): ?string
    {
        return $this->blockName;
    }

    /**
     * @return ButtonType
     */
    public function getButtonType(): ButtonType
    {
        return $this->buttonType;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @return string|null
     */
    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

    /**
     * @return string|null
     */
    public function getFormHeading(): ?string
    {
        return $this->formHeading;
    }

    /**
     * @return string|null
     */
    public function getFormLeading(): ?string
    {
        return $this->formLeading;
    }

    /**
     * @return string|null
     */
    public function getFormSource(): ?string
    {
        return $this->formSource;
    }

    /**
     * @return string|null
     */
    public function getSide(): ?string
    {
        return $this->side;
    }
}
