<?php

namespace App\View\Data\Block;

class KpiData
{
    private string $medals;
    private string $ranks;
    private string $children;

    public function __construct(string $medals, string $ranks, string $children)
    {
        $this->medals = $medals;
        $this->ranks = $ranks;
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getMedals(): string
    {
        return $this->medals;
    }

    /**
     * @return string
     */
    public function getRanks(): string
    {
        return $this->ranks;
    }

    /**
     * @return string
     */
    public function getChildren(): string
    {
        return $this->children;
    }


}
