<?php

namespace App\View\Data\Block;

class ReviewsData
{

    private string $header;
    private ?string $buttonText;

    public function __construct(string $header, ?string $buttonText)
    {

        $this->header = $header;
        $this->buttonText = $buttonText;
    }

    /**
     * @return string
     */
    public function getHeader(): string
    {
        return $this->header;
    }

    /**
     * @return string
     */
    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }
}
