<?php

namespace App\View\Data\Block;

class SeniorCoachesData
{
    private ?string $heading;

    public function __construct(?string $heading)
    {
        $this->heading = $heading;
    }

    /**
     * @return ?string
     */
    public function getHeading(): ?string
    {
        return $this->heading;
    }
}
