<?php

namespace App\View\Data\Block;

use App\Models\Website\Gallery;

class PhotoshootData
{
    private ?string $heading;
    private ?string $leading;
    private Gallery $gallery;
    private ?string $buttonText;
    private ?string $formHeading;
    private ?string $formLeading;
    private ?string $formSource;

    public function __construct(
        ?string  $heading,
        ?string  $leading,
        Gallery $gallery,
        ?string  $buttonText,
        ?string $formHeading,
        ?string $formLeading,
        ?string $formSource
    )
    {

        $this->heading = $heading;
        $this->leading = $leading;
        $this->gallery = $gallery;
        $this->buttonText = $buttonText;
        $this->formHeading = $formHeading;
        $this->formLeading = $formLeading;
        $this->formSource = $formSource;
    }

    /**
     * @return string|null
     */
    public function getHeading(): ?string
    {
        return $this->heading;
    }

    /**
     * @return string|null
     */
    public function getLeading(): ?string
    {
        return $this->leading;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery(): Gallery
    {
        return $this->gallery;
    }

    /**
     * @return string|null
     */
    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

    /**
     * @return string|null
     */
    public function getFormHeading(): ?string
    {
        return $this->formHeading;
    }

    /**
     * @return string|null
     */
    public function getFormLeading(): ?string
    {
        return $this->formLeading;
    }

    /**
     * @return string|null
     */
    public function getFormSource(): ?string
    {
        return $this->formSource;
    }
}
