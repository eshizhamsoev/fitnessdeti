<?php

namespace App\View\Data\Block;

use App\Models\Website\BlogPost;
use Spatie\MediaLibrary\MediaCollections\HtmlableMedia;

class BlogItemData
{
    private BlogPost $blogPost;

    public function __construct(BlogPost $blogPost)
    {
        $this->blogPost = $blogPost;
    }

    public function hasMedia(): bool
    {
        return $this->blogPost->hasMedia(\App\Models\Website\BlogPost::IMAGE_COLLECTION_MAIN);
    }

    public function getMedia(): ?HtmlableMedia
    {
        return $this->blogPost->getFirstMedia(\App\Models\Website\BlogPost::IMAGE_COLLECTION_MAIN)(\App\Models\Website\BlogPost::IMAGE_CONVERSION_MAIN);
    }

    public function getLink(): ?string
    {
        if (!$this->blogPost->url) {
            return null;
        }
        return $this->blogPost->url->url;
    }

    public function hasText(): bool
    {
        return $this->blogPost->text !== null;
    }

    public function getText(): ?string
    {
        return $this->blogPost->text;
    }

    public function getAnnounce(): ?string
    {
        return $this->blogPost->announce;
    }

    public function getTitle(): string
    {
        return $this->blogPost->name;
    }
}
