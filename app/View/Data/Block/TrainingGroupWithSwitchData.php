<?php

namespace App\View\Data\Block;

use App\Enums\TrainingGroupType;

class TrainingGroupWithSwitchData
{
    private TrainingGroupType $typeLeft;
    private TrainingGroupType $typeRight;
    private string $nameLeft;
    private string $nameRight;

    private ?string $heading;
    private ?string $leading;

    public function __construct(
        TrainingGroupType $typeLeft,
        string $nameLeft,
        TrainingGroupType $typeRight,
        string $nameRight,
        string $heading,
        string $leading
    ) {
        $this->typeLeft = $typeLeft;
        $this->nameLeft = $nameLeft;
        $this->typeRight = $typeRight;
        $this->nameRight = $nameRight;
        $this->heading = $heading;
        $this->leading = $leading;
    }

    /**
     * @return TrainingGroupType
     */
    public function getTypeLeft(): TrainingGroupType
    {
        return $this->typeLeft;
    }

    /**
     * @return TrainingGroupType
     */
    public function getTypeRight(): TrainingGroupType
    {
        return $this->typeRight;
    }

    /**
     * @return string
     */
    public function getNameLeft(): string
    {
        return $this->nameLeft;
    }

    /**
     * @return string
     */
    public function getNameRight(): string
    {
        return $this->nameRight;
    }

    /**
     * @return string|null
     */
    public function getHeading(): ?string
    {
        return $this->heading;
    }

    /**
     * @return string|null
     */
    public function getLeading(): ?string
    {
        return $this->leading;
    }
}
