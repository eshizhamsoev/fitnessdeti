<?php

namespace App\View\Data\Block;

class MedalData
{
    private string $gold;
    private string $silver;
    private string $bronze;
    private ?string $buttonText;

    public function __construct(string $gold, string $silver, string $bronze, ?string $buttonText)
    {

        $this->gold = $gold;
        $this->silver = $silver;
        $this->bronze = $bronze;
        $this->buttonText = $buttonText;
    }

    /**
     * @return string
     */
    public function getGold(): string
    {
        return $this->gold;
    }

    /**
     * @return string
     */
    public function getSilver(): string
    {
        return $this->silver;
    }

    /**
     * @return string
     */
    public function getBronze(): string
    {
        return $this->bronze;
    }

    /**
     * @return ?string
     */
    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

}
