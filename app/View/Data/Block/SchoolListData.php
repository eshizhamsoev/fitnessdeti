<?php

namespace App\View\Data\Block;

class SchoolListData
{
    private string $heading;
    private bool $shouldShowAll = false;

    public function __construct(string $heading)
    {
        $this->heading = $heading;
    }


    /**
     * @return bool
     */
    public function shouldShowAll(): bool
    {
        return $this->shouldShowAll;
    }

    /**
     * @param  bool  $shouldShowAll
     * @return SchoolListData
     */
    public function setShouldShowAll(bool $shouldShowAll): self
    {
        $this->shouldShowAll = $shouldShowAll;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeading(): string
    {
        return $this->heading;
    }
}
