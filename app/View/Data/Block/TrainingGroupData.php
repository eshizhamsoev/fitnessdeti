<?php

namespace App\View\Data\Block;

use App\Enums\TrainingGroupType;

class TrainingGroupData
{
    private TrainingGroupType $type;
    private ?string $heading;
    private ?string $leading;

    public function __construct(TrainingGroupType $type, ?string $heading, ?string $leading)
    {
        $this->type = $type;
        $this->heading = $heading;
        $this->leading = $leading;
    }

    /**
     * @return TrainingGroupType
     */
    public function getType(): TrainingGroupType
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getHeading(): ?string
    {
        return $this->heading;
    }

    /**
     * @return string|null
     */
    public function getLeading(): ?string
    {
        return $this->leading;
    }
}
