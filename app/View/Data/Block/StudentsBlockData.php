<?php

namespace App\View\Data\Block;

use App\Models\Website\Gallery;

class StudentsBlockData
{


    private ?Gallery $gallery;

    public function __construct(Gallery $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery(): Gallery
    {
        return $this->gallery;
    }
}
