<?php

namespace App\View\Data\Block;

use App\Models\Data\Discipline;

class CoachesBlockData
{

    /**
     * @var Discipline[]
     */
    private array $disciplines;

    public function __construct(array $disciplines)
    {
        $this->disciplines = $disciplines;
    }

    /**
     * @return Discipline[]
     */
    public function getDisciplines(): array
    {
        return $this->disciplines;
    }


}
