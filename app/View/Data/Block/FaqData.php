<?php

namespace App\View\Data\Block;

use App\Models\Website\Faq;

class FaqData
{
    private Faq $faq;

    public function __construct(Faq $faq)
    {

        $this->faq = $faq;
    }

    /**
     * @return Faq
     */
    public function getFaq(): Faq
    {
        return $this->faq;
    }

}
