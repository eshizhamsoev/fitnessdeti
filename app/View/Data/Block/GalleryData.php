<?php

namespace App\View\Data\Block;

use App\Enums\Website\GalleryType;
use App\Models\Website\Gallery;

class GalleryData
{

    private ?string $blockName;
    private Gallery $gallery;
    private GalleryType $galleryType;


    public function __construct(Gallery $gallery, GalleryType $galleryType, ?string $blockName)
    {
        $this->blockName = $blockName;
        $this->gallery = $gallery;
        $this->galleryType = $galleryType;
    }


    /**
     * @return Gallery
     */
    public function getGallery(): Gallery
    {
        return $this->gallery;
    }

    /**
     * @return string
     */
    public function getGalleryType(): string
    {
        return $this->galleryType;
    }

    /**
     * @return ?string
     */
    public function getBlockName(): ?string
    {
        return $this->blockName;
    }
}
