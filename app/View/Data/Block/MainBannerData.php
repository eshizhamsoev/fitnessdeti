<?php

namespace App\View\Data\Block;

use App\Enums\Website\ButtonType;
use App\Enums\Website\GalleryType;
use App\Enums\Website\WideBannerDesktopImagePosition;
use App\Models\Website\Gallery;

class MainBannerData
{
    private ?Gallery $gallery;

    public function __construct(
        ?Gallery    $gallery
    )
    {
        $this->gallery = $gallery;
    }

    /**
     * @return Gallery|null
     */
    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }
}
