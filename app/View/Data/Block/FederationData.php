<?php

namespace App\View\Data\Block;

class FederationData
{
    private string $gymnasticName;
    private string $gymnasticText;
    private string $acrobaticName;
    private string $acrobaticText;

    public function __construct(
        string $gymnasticName,
        string $gymnasticText,
        string $acrobaticName,
        string $acrobaticText
    ) {
        $this->gymnasticName = $gymnasticName;
        $this->gymnasticText = $gymnasticText;
        $this->acrobaticName = $acrobaticName;
        $this->acrobaticText = $acrobaticText;
    }

    /**
     * @return string
     */
    public function getGymnasticName(): string
    {
        return $this->gymnasticName;
    }

    /**
     * @return string
     */
    public function getGymnasticText(): string
    {
        return $this->gymnasticText;
    }

    /**
     * @return string
     */
    public function getAcrobaticName(): string
    {
        return $this->acrobaticName;
    }

    /**
     * @return string
     */
    public function getAcrobaticText(): string
    {
        return $this->acrobaticText;
    }


}
