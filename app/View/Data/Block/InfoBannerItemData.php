<?php

namespace App\View\Data\Block;

use App\Models\Website\GalleryImage;

class InfoBannerItemData
{

    private ?string $desktopImageUrl;
    private ?string $mobileImageUrl;
    private string $hiddenClass;
    private ?string $name;
    private ?string $description;
    private ?string $link;

    private const DESKTOP_HIDDEN_CLASS = 'banner-info__hidden_desktop';
    private const MOBILE_HIDDEN_CLASS = 'banner-info__hidden_mobile';

    public function __construct(GalleryImage $galleryItem)
    {
        $this->mobileImageUrl = $galleryItem->getFirstMedia('mobile') ? $galleryItem->getFirstMedia('mobile')->getUrl('main') : null;
        $this->desktopImageUrl = $galleryItem->getFirstMedia('main') ? $galleryItem->getFirstMedia('main')->getUrl('main') : null;
        $this->name = $galleryItem->name;
        $this->description = $galleryItem->description;
        $this->link = $galleryItem->link;
        $this->hiddenClass = $this->makeHiddenClass();
    }

    /**
     * @return string|null
     */
    public function getDesktopImageUrl(): ?string
    {
        return $this->desktopImageUrl;
    }

    /**
     * @return string|null
     */
    public function getMobileImageUrl(): ?string
    {
        return $this->mobileImageUrl;
    }

    /**
     * @return string|null
     */
    public function getHiddenClass(): string
    {
        return $this->hiddenClass;
    }

    /**
     * @return mixed|string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed|string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed|string|null
     */
    public function getLink()
    {
        return $this->link;
    }

    private function makeHiddenClass() :string
    {
        $hiddenClassArray = [];

        if (!$this->desktopImageUrl)
        {
            $hiddenClassArray[] = $this::DESKTOP_HIDDEN_CLASS;
        }

        if (!$this->mobileImageUrl)
        {
            $hiddenClassArray[] = $this::MOBILE_HIDDEN_CLASS;
        }

        return implode(' ', $hiddenClassArray);
    }
}
