<?php

namespace App\View\Data\Content\Competition;

use App\Enums\CompetitionType;
use App\Enums\Website\SvgIconName;
use App\Models\Data\Competition;
use Spatie\MediaLibrary\MediaCollections\HtmlableMedia;

class CompetitionModalItem
{

    public const COMPETITION_ICONS = [
        'ranked' => SvgIconName::LABEL_RANK,
        'commercial' => SvgIconName::LABEL_CALENDAR
    ];

    private Competition $competition;

    public function __construct(Competition $competition)
    {
        $this->competition = $competition;
    }

    public function getName()
    {
        return $this->competition->name;
    }

    public function getDescription()
    {
        return $this->competition->description;
    }

    public function hasPhoto(): bool
    {
        return $this->competition->hasMedia(Competition::IMAGE_COLLECTION_MAIN);
    }

    public function getPhoto(): HtmlableMedia
    {
        return $this->competition->getFirstMedia(Competition::IMAGE_COLLECTION_MAIN)(Competition::IMAGE_CONVERSION_MAIN);
    }

    public function getIcon(): ?SvgIconName
    {
        $icon = self::COMPETITION_ICONS[$this->competition->competition_type->value] ?? null;
        return $icon ? SvgIconName::fromValue($icon) : null;
    }

    public function getType(): ?string
    {
        return $this->competition->competition_type && $this->competition->competition_type->value !== CompetitionType::UNKNOWN ? $this->competition->competition_type->description : null;
    }

    public function getLocation(): ?string
    {
        return $this->competition->location;
    }

    public function getTime(): ?string
    {
        return ($this->competition->time_from && $this->competition->time_to) ? substr($this->competition->time_from, 0,
                5) . '-' . substr($this->competition->time_to, 0, 5) : '';
    }

    public function getDateStart(): string
    {
        return $this->competition->date_start->format('d.m.Y');
    }

    public function getDateEnd(): ?string
    {
        if(!$this->competition->date_end || $this->competition->date_end->isSameDay($this->competition->date_start)){
            return null;
        }
        return $this->competition->date_end->format('d.m.Y');
    }
}
