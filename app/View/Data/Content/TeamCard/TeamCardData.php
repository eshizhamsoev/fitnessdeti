<?php

namespace App\View\Data\Content\TeamCard;

use App\Enums\Website\DisciplineColor;
use App\Enums\Website\SvgIconName;
use App\Models\Data\Coach;
use App\Models\Data\Discipline;
use App\Models\Data\Student;
use Spatie\MediaLibrary\MediaCollections\HtmlableMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class TeamCardData
{
    private Student $student;
    private ?SvgIconName $svgIconName = null;


    public function __construct(Student $student)
    {
        $this->student = $student;
        if (isset($this->student->discipline)) {
            $this->svgIconName = $this->student->discipline->color->is(DisciplineColor::PINK) ? SvgIconName::DANCER() : SvgIconName::GYMNAST();
        }

    }

    public function getName()
    {
        return $this->student->lastname . ' ' . $this->student->firstname ;
    }

    public function getFallbackImage()
    {
        return asset('/static/images/common/person_placeholder_woman.jpg');
    }

    public function getImage(): ?HtmlableMedia
    {
        $media = $this->student->getFirstMedia(Student::IMAGE_COLLECTION_MAIN);
        if (!$media) {
            return null;
        }
        return $media('main');
    }

    /**
     * @return SvgIconName|null
     */
    public function getSvgIconName(): ?SvgIconName
    {
        return $this->svgIconName;
    }
}
