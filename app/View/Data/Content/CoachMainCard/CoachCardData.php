<?php

namespace App\View\Data\Content\CoachMainCard;

use App\Models\Data\Coach;
use Spatie\MediaLibrary\MediaCollections\HtmlableMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class CoachCardData
{
    private Coach $coach;

    public function __construct(Coach $coach)
    {
        $this->coach = $coach;
    }

    public function getName()
    {
        return $this->coach->name;
    }

    public function getClass(): CoachClassData
    {
        return new CoachClassData($this->coach->coach_class);
    }

    public function getDiscipline(): CoachDisciplineData
    {
        return new CoachDisciplineData($this->coach->discipline);
    }

    public function getFallbackImage()
    {
        return asset('/static/images/common/person_placeholder_woman.jpg');
    }

    public function getUrl(): ?string
    {
        if (!$this->coach->url) {
            return null;
        }
        return $this->coach->url->url;
    }

    public function getImage(): ?HtmlableMedia
    {
        $media = $this->coach->getFirstMedia(Coach::IMAGE_COLLECTION_MAIN);
        if (!$media) {
            return null;
        }
        return $media('main');
    }

    public function getYoutubeCode(): ?string
    {
        return $this->coach->youtube_code;
    }
}
