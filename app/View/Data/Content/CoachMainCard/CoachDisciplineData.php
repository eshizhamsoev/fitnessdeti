<?php

namespace App\View\Data\Content\CoachMainCard;


use App\Enums\Website\DisciplineColor;
use App\Enums\Website\SvgIconName;
use App\Models\Data\Discipline;

class CoachDisciplineData
{
    private Discipline $discipline;

    public function __construct(Discipline $discipline)
    {
        $this->discipline = $discipline;
    }

    public function getTitle()
    {
        return $this->discipline->name;
    }


    public function getColor()
    {
        return $this->discipline->color->value;
    }

    public function getSvgName(): SvgIconName
    {
        return $this->discipline->color->is(DisciplineColor::PINK) ? SvgIconName::DANCER() : SvgIconName::GYMNAST();
    }
}
