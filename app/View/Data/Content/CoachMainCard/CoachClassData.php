<?php

namespace App\View\Data\Content\CoachMainCard;

use App\Enums\CoachClass;

class CoachClassData
{
    private CoachClass $coachClass;

    public function __construct(CoachClass $coachClass)
    {
        $this->coachClass = $coachClass;
    }

    public function getTitle(): string
    {
        return $this->coachClass->description;
    }

    public function canBeShown()
    {
        return $this->coachClass->value !== CoachClass::Unknown;
    }
}
