<?php

namespace App\View\Components\Content;

use App\Models\Data\Coach;
use App\View\Data\Content\CoachMainCard\CoachCardData;
use Illuminate\View\Component;

class CoachSmallCard extends Component
{
    private Coach $coach;

    public function __construct(Coach $coach)
    {
        $this->coach = $coach;
    }

    public function render()
    {
        return view('components.content.coach-small-card', [
            'data' => new CoachCardData($this->coach)
        ]);
    }
}
