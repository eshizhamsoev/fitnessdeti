<?php

namespace App\View\Components\Content;

use App\Models\Data\TrainingGroup;
use Illuminate\View\Component;

class TeachingGroupItem extends Component
{
    private TrainingGroup $group;

    public function __construct(TrainingGroup $group)
    {
        $this->group = $group;
    }

    public function render()
    {
        return view('components.content.teaching-group-item', [
            'group' => $this->group
        ]);
    }
}
