<?php

namespace App\View\Components\Content;

use App\Models\Website\GalleryImage;
use Illuminate\View\Component;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MainBannerSlide extends Component
{
    private ?Media $desktopMedia;
    private ?Media $mobileMedia;
    private ?string $description;
    private ?string $name;
    private ?string $link;
    private ?string $linkText;

    private const DESKTOP_HIDDEN_CLASS = 'banner-gymnastic-slide__desktop-hidden';
    private const MOBILE_HIDDEN_CLASS = 'banner-gymnastic-slide__mobile-hidden';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(GalleryImage $image)
    {
        $this->desktopMedia = $image->getFirstMedia('main');
        $this->mobileMedia = $image->getFirstMedia('mobile');
        $this->description = $image->description;
        $this->name = $image->name;
        $this->link = $image->link;
        $this->linkText = $image->link_text;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $hiddenClasses = [];

        if (!$this->desktopMedia) {
            $hiddenClasses[] = self::DESKTOP_HIDDEN_CLASS;
        }

        if (!$this->mobileMedia) {
            $hiddenClasses[] = self::MOBILE_HIDDEN_CLASS;
        }

        return view('components.content.main-banner-slide', [
            'desktopMedia' => $this->desktopMedia,
            'mobileMedia' => $this->mobileMedia,
            'description' => $this->description,
            'name' => $this->name,
            'link' => $this->link,
            'linkText' => $this->linkText,
            'hiddenClasses' => implode(' ', $hiddenClasses)
        ]);
    }
}
