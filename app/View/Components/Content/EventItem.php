<?php

namespace App\View\Components\Content;

use App\Models\Data\Camp;
use App\View\Data\Common\EventItemData;
use DateTime;
use Illuminate\View\Component;
use Jenssegers\Date\Date;

class EventItem extends Component
{
    private EventItemData $data;
    private string $formSource;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(EventItemData $data, string $formSource = 'Источник не указан')
    {
        $this->data = $data;
        $this->formSource = $formSource;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.content.event-item', [
            'eventItem' => $this->data,
            'formSource' => $this->formSource
        ]);
    }
}
