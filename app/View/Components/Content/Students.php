<?php

namespace App\View\Components\Content;

use App\Models\Data\Coach;
use App\Models\Data\Student;
use Illuminate\Support\Collection;
use Illuminate\View\Component;
use function view;

class Students extends Component
{

    private array $students;

    /**
     * @param Student[] $students
     */
    public function __construct(array $students)
    {
        $this->students = $students;
    }

    public function render()
    {
        return view('components.content.students', [
            'students' => $this->students
        ]);
    }
}
