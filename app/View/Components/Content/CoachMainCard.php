<?php

namespace App\View\Components\Content;

use App\Enums\Website\CoachCardKpiSide;
use App\Enums\Website\SvgIconName;
use App\Models\Data\Coach;
use App\View\Data\Content\CoachMainCard\CoachCardData;
use Illuminate\View\Component;

class CoachMainCard extends Component
{
    private Coach $coach;
    private ?CoachCardKpiSide $cardKpiSide;

    public function __construct(
        Coach $coach,
        ?CoachCardKpiSide $cardKpiSide = null
    ) {
        $this->coach = $coach;
        $this->cardKpiSide = $cardKpiSide;
    }

    public function render()
    {
        return view('components.content.coach-main-card', [
            'coachData' => new CoachCardData($this->coach),
            'cardKpiSide' => $this->cardKpiSide,
            'labelIcon' => new SvgIconName(SvgIconName::LABEL_CERTIFICATE),
            'kpi' => $this->coach->getKpi()
        ]);
    }
}
