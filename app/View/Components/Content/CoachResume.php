<?php

namespace App\View\Components\Content;

use App\Models\Data\Coach;
use Illuminate\View\Component;

class CoachResume extends Component
{
    private Coach $coach;

    public function __construct(Coach $coach)
    {
        $this->coach = $coach;
    }

    public function render()
    {
        $resume = $this->coach->resume;

        return view('components.content.coach-resume', [
            'resume' => $resume,
            'coachName' => $this->coach->name,
            'source' => 'Тренер ' . $this->coach->name
        ]);
    }
}
