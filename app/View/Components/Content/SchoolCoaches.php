<?php

namespace App\View\Components\Content;

use App\Models\Data\School;
use Illuminate\View\Component;

class SchoolCoaches extends Component
{
    private School $school;

    public function __construct(School $school)
    {
        $this->school = $school;
    }

    public function render()
    {
        return view('components.content.school-coaches', [
            'coaches' => $this->school->coaches
        ]);
    }
}
