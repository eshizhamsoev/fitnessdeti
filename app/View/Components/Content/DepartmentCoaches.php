<?php

namespace App\View\Components\Content;

use Illuminate\View\Component;

class DepartmentCoaches extends Component
{
    private array $coaches;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(array $coaches)
    {
        $this->coaches = $coaches;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.content.department-coaches', [
            'coaches' => $this->coaches
        ]);
    }
}
