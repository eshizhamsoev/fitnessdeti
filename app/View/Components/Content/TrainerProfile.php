<?php

namespace App\View\Components\Content;

use App\Models\Data\Coach;
use Illuminate\View\Component;

class TrainerProfile extends Component
{
    private Coach $coach;

    public function __construct(Coach $coach)
    {
        $this->coach = $coach;
    }

    public function render()
    {
        return view('components.content.trainer-profile', [
            'coach' => $this->coach
        ]);
    }
}
