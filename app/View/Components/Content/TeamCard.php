<?php

namespace App\View\Components\Content;

use App\Models\Data\Student;
use App\View\Data\Content\TeamCard\TeamCardData;
use Illuminate\View\Component;

class TeamCard extends Component
{
    private TeamCardData $student;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Student $student)
    {
        $this->student = new TeamCardData($student);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.content.team-card',[
            'student' => $this->student
        ]);
    }
}
