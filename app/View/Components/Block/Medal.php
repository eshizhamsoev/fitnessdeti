<?php

namespace App\View\Components\Block;

use App\View\Data\Block\MedalData;
use Illuminate\View\Component;

class Medal extends Component
{
    private MedalData $data;

    public function __construct(MedalData $data)
    {
        $this->data = $data;
    }

    public function render()
    {
        return view('components.block.medal', [
            'data' => $this->data
        ]);
    }
}
