<?php

namespace App\View\Components\Block;

use App\Enums\Website\Kpi\KpiColor;
use App\Enums\Website\SvgIconName;
use App\View\Data\Block\KpiData;
use App\View\Data\Common\KpiItemData;
use Illuminate\View\Component;

class Kpi extends Component
{
    private KpiData $data;

    public function __construct(KpiData $data)
    {
        $this->data = $data;
    }

    public function render()
    {
        return view('components.block.kpi', [
            'items' => [
                (new KpiItemData(
                    KpiColor::fromValue(KpiColor::RED),
                    SvgIconName::MEDAL(),
                    $this->data->getMedals(),
                    'Медалей получено'
                ))->setMobileTitle('Медалей'),
                (new KpiItemData(
                    KpiColor::fromValue(KpiColor::GREEN),
                    SvgIconName::RIBBON(),
                    $this->data->getRanks(),
                    'Разрядов по всей школе'
                ))->setMobileTitle('Разрядов'),
                (new KpiItemData(
                    KpiColor::fromValue(KpiColor::PURPLE),
                    SvgIconName::CHILDREN(),
                    $this->data->getChildren(),
                    'Детей обучено за 10 лет'
                ))->setMobileTitle('Детей'),

            ]
        ]);
    }
}
