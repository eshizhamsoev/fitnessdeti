<?php

namespace App\View\Components\Block;

use App\View\Data\Block\HtmlData;
use Illuminate\View\Component;

class HtmlCode extends Component
{
    private string $htmlCode;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(HtmlData $data)
    {
        $this->htmlCode = $data->getHtmlCode();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.html-code', ['htmlCode' => $this->htmlCode]);
    }
}
