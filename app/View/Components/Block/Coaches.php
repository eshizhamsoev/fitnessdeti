<?php

namespace App\View\Components\Block;

use App\Enums\CoachType;
use App\Models\Data\Coach;
use App\View\Data\Block\CoachesBlockData;
use Illuminate\View\Component;

class Coaches extends Component
{
    private CoachesBlockData $data;

    public function __construct(CoachesBlockData $data)
    {
        $this->data = $data;
    }

    public function render()
    {
        return view('components.block.coaches', [
            'coaches' => Coach::where('coach_type', CoachType::Ordinary())
                ->with(['discipline', 'media', 'url'])
                ->orderBy('name')
                ->get()
                ->sortByDesc(function (Coach $item) {
                    return $item->media->isNotEmpty();
                })
            ,
            'disciplines' => $this->data->getDisciplines()
        ]);
    }
}
