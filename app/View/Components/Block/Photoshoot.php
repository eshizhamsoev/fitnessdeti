<?php

namespace App\View\Components\Block;

use App\View\Data\Block\PhotoshootData;
use Illuminate\View\Component;

class Photoshoot extends Component
{
    private PhotoshootData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(PhotoshootData $data)
    {
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.photoshoot',[
            'heading' => $this->data->getHeading(),
            'leading' => $this->data->getLeading(),
            'gallery'=> $this->data->getGallery()->images,
            'buttonText' => $this->data->getButtonText(),
            'formSource' => $this->data->getFormSource(),
            'formHeading' => $this->data->getFormHeading(),
            'formLeading' => $this->data->getFormLeading()
        ] );
    }
}
