<?php

namespace App\View\Components\Block;

use App\Models\Data\School;
use App\Models\Website\Page;
use App\Services\CurrentCityRepository;
use App\Services\PhoneFormatter\PhoneFormatter;
use App\Services\PhoneFormatter\PhoneNumber;
use App\Services\SettingsFields;
use App\View\Data\Block\SchoolListData;
use Illuminate\View\Component;

class SchoolList extends Component
{
    static int $renderedId = 0;
    private SchoolListData $data;
    private PhoneFormatter $phoneFormatter;
    private CurrentCityRepository $currentCityRepository;

    public function __construct(
        PhoneFormatter $phoneFormatter,
        SchoolListData $data,
        CurrentCityRepository $currentCityRepository
    ) {
        $this->data = $data;
        $this->phoneFormatter = $phoneFormatter;
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        $schools = School::with(['metroStations', 'url'])->byCity($this->currentCityRepository->getCurrentCity())->get();
        self::$renderedId += 1;

        $visibleSchools = $this->data->shouldShowAll() ? $schools : $schools->slice(0, 5);

        return view('components.block.school-list', [
            'schools' => $visibleSchools,
            'heading' => $this->data->getHeading(),
            'jsVariableName' => sprintf('data-map-%s', self::$renderedId),
            'schoolsPageUrl' => $this->getSchoolPageUrl(),
            'shouldShowAll' => $this->data->shouldShowAll(),
            'dataForMap' => $schools->map(fn(School $school) => [
                'id' => $school->id,
                'name' => $school->name,
                'metro' => $school->metroStations->count() > 0,
                'address' => $school->address,
                'latitude' => $school->latitude,
                'longitude' => $school->longitude,
                'link' => $school->url ? $school->url->url : null,
                'phone' => $this->getPhone($school->phone),
                'whatsapp' => 'https://wa.me/' . $school->whatsapp,
                'telegram' => 'https://t.me/' . $school->telegram,
            ])

        ]);
    }

    private function getSchoolPageUrl()
    {
        $page = Page::find(nova_get_setting(SettingsFields::SCHOOL));
        if (!$page || !$page->url) {
            return null;
        }

        return $page->url->url;
    }

    private function getPhone(?string $number): ?array
    {
        if (!$number) {
            return null;
        }
        $phoneNumber = new PhoneNumber($number);
        return [
            'number' => $phoneNumber->getInternationalNumber(),
            'formatted' => $this->phoneFormatter->format($phoneNumber),
        ];
    }
}
