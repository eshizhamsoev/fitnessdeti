<?php

namespace App\View\Components\Block;

use App\View\Data\Block\InfoBannerData;
use App\View\Data\Block\InfoBannerItemData;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class InfoBanner extends Component
{
    private InfoBannerData $data;
    private Collection $galleryItems;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(InfoBannerData $data)
    {
        $this->data = $data;
        $this->makeGalleryItems();
    }

    private function makeGalleryItems ()
    {
       $this->galleryItems = $this->data->getGallery()->images->map(function ($item) {
            return new InfoBannerItemData($item);
        });
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.info-banner', [
            'blockName' => $this->data->getBlockName(),
            'buttonType' => $this->data->getButtonType(),
            'side' => $this->data->getSide(),
            'galleryItems' => $this->galleryItems,
            'buttonText' => $this->data->getButtonText(),
            'formHeading' => $this->data->getFormHeading(),
            'formLeading' => $this->data->getFormLeading(),
            'formSource' => $this->data->getFormSource(),
        ]);
    }
}
