<?php

namespace App\View\Components\Block;

use Illuminate\View\Component;

class AboutCompany extends Component
{
    private object $images;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(object $images)
    {
        $this->images = $images;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.about-company', ['images' => $this->images]);
    }
}
