<?php

namespace App\View\Components\Block;

use App\Services\CurrentUrlRepository;
use Illuminate\View\Component;

class Breadcrumbs extends Component
{
    private CurrentUrlRepository $currentUrlRepository;

    public function __construct(CurrentUrlRepository $currentUrlRepository)
    {
        $this->currentUrlRepository = $currentUrlRepository;
    }

    public function render()
    {
        return view('components.block.breadcrumbs', [
            'url' => $this->currentUrlRepository->getCurrentUrl()
        ]);
    }
}
