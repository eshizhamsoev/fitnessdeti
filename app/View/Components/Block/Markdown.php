<?php

namespace App\View\Components\Block;

use App\View\Data\Block\MarkdownData;
use Illuminate\View\Component;

class Markdown extends Component
{
    private MarkdownData $data;

    public function __construct(MarkdownData $data)
    {
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.markdown', ['markdownText' => $this->data->getMarkdownText()]);
    }
}
