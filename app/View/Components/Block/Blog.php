<?php

namespace App\View\Components\Block;

use App\Models\Website\BlogPost;
use App\View\Data\Block\BlogItemData;
use Illuminate\View\Component;

class Blog extends Component
{
    public function render()
    {
        $blogs = BlogPost::orderByDesc('date')->paginate(10);
        return view('components.block.blog', [
            'blogs' => $blogs->map(fn(BlogPost $blogPost) => new BlogItemData($blogPost)),
            'links' => $blogs->links('components.base.pagination')
        ]);
    }
}
