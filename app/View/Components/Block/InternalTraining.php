<?php

namespace App\View\Components\Block;

use App\Enums\Website\SvgIconName;
use App\View\Data\Block\InternalTrainingData;
use Illuminate\View\Component;

class InternalTraining extends Component
{
    private InternalTrainingData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(InternalTrainingData $data)
    {

        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.internal-training', [
            'titlePosition' => $this->data->getTitlePosition(),
            'buttonText' => $this->data->getButtonText(),
            'headingText' => $this->data->getHeadingText(),
            'blocks' => [
                [
                    'text' => $this->data->getLeftBlockText(),
                    'icon' => SvgIconName::FRIEND(),
                ],
                [
                    'text' => $this->data->getCenterBlockText(),
                    'icon' => SvgIconName::MEDICAL_INSURANCE(),
                ],
                [
                    'text' => $this->data->getRightBlockText(),
                    'icon' => SvgIconName::TROPHY_2(),
                ]
            ],
        ]);
    }
}
