<?php

namespace App\View\Components\Block;

use App\Models\Data\MasterClass;
use App\View\Data\Common\EventItemData;
use Carbon\Carbon;
use Illuminate\View\Component;

class MasterClasses extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $masterClasses = MasterClass::where('is_enabled', 1)->where('date', '>', Carbon::today())->get()->sortBy('date');

        return view('components.block.master-classes', [
            'masterClasses' => $masterClasses->map(fn(MasterClass $masterCLass) => EventItemData::constructFromMasterClass($masterCLass))
        ]);

    }
}
