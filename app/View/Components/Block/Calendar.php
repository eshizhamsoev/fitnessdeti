<?php

namespace App\View\Components\Block;

use App\Models\Data\Competition;
use Carbon\Carbon;
use Illuminate\View\Component;

class Calendar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {

        $dates = Competition::where('is_enabled', true)->groupBy('date_start')->select([
            'date_start',
            \DB::raw('COUNT(id) as count')
        ])->get();

        $eventsMap = $dates->map(fn($date) => [
            'id' => $date->date_start->format('Y-m-d'),
            'title' => $date->count . ' шт.',
            'start' => $date->date_start->format('Y-m-d')
        ]);

        return view('components.block.calendar', [
            'jsVariableName' => 'calendarData',
            'events' => $eventsMap
        ]);
    }
}
