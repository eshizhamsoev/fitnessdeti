<?php

namespace App\View\Components\Block;

use App\Models\Data\Student;
use Illuminate\View\Component;

class Team extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.team', [
            'students' => Student::where('is_in_team', true)->where('is_enabled', true)->get()
        ]);
    }
}
