<?php

namespace App\View\Components\Block;

use App\Models\Data\Camp;
use App\View\Data\Common\EventItemData;
use Carbon\Carbon;
use Illuminate\View\Component;

class Camps extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {

        $camps = Camp::where('is_enabled', 1)->where('date_from', '>', Carbon::today())->get()->sortBy('date_from');

        return view('components.block.camps', [
            'camps' => $camps->map(fn(Camp $camp) => EventItemData::constructFromCamp($camp))
        ]);
    }
}
