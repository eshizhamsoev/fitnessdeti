<?php

namespace App\View\Components\Block;

use App\Enums\CoachType;
use App\Models\Data\Coach;
use Illuminate\View\Component;

class CoachingStaff extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.coaching-staff', [
            'coaches' => Coach::whereHas('media')
                ->with(['discipline', 'media', 'url'])
                ->orderBy('name')
                ->limit(8)
                ->get()
                ->all()
        ]);
    }
}
