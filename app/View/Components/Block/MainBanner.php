<?php

namespace App\View\Components\Block;

use App\Enums\Website\ButtonType;
use App\Enums\Website\SvgIconName;
use App\Enums\Website\WideBannerDesktopImagePosition;
use App\Models\Website\Gallery as Gallery;
use App\View\Data\Block\MainBannerData;
use Illuminate\View\Component;

class MainBanner extends Component
{

    private ?Gallery $gallery;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(MainBannerData $data)
    {
        $this->gallery = $data->getGallery();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.main-banner', [
            'gallery' => $this->gallery,
            'arrowIcon' => SvgIconName::ARROW(),
            'playIcon' => SvgIconName::PLAY(),
            'watchUpIcon' => SvgIconName::WATCH_UP()
        ]);
    }
}
