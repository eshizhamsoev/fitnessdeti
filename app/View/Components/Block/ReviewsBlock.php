<?php

namespace App\View\Components\Block;

use App\Models\Data\Review;
use App\Models\Website\Page;
use App\Services\SettingsFields;
use App\View\Data\Common\TabData;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\View\Component;

class ReviewsBlock extends Component
{

    private const TYPE_PARAMETER_NAME = 'type';

    private const TEXT_PARAMETER = 'text';

    public const VIDEO_PARAMETER = 'video';

    private ?string $typeParameter;

    public function __construct(Request $request)
    {
        $this->typeParameter = $request->input(self::TYPE_PARAMETER_NAME);
    }

    private function makeReviewsUrl(?string $type = null): string
    {
            return '/' . ($type ? request()->path() . '?type=' . $type : request()->path());
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {

        $query = Review::query();

        if ($this->typeParameter === self::TEXT_PARAMETER) {
            $query = $query->where('youtube_code', null);
        } else if ($this->typeParameter === self::VIDEO_PARAMETER) {
            $query = $query->where('youtube_code', '!=', null);
        }
        $reviews = $query->paginate(7);

        $reviews->appends([self::TYPE_PARAMETER_NAME => $this->typeParameter]);

        return view('components.block.reviews-block', [
            'tabs' => [
                new TabData(
                    'Все отзывы',
                    $this->typeParameter === null,
                    $this->makeReviewsUrl()
                ),
                new TabData(
                    'Видео отзывы',
                    $this->typeParameter === self::VIDEO_PARAMETER,
                    $this->makeReviewsUrl(self::VIDEO_PARAMETER)
                ),
                new TabData(
                    'Текстовые отзывы',
                    $this->typeParameter === self::TEXT_PARAMETER,
                    $this->makeReviewsUrl(self::TEXT_PARAMETER)
                )
            ],
            'reviews' => $reviews,
            'links' => $reviews->links('components.base.pagination')
        ]);
    }
}
