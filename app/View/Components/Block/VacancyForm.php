<?php

namespace App\View\Components\Block;

use App\Enums\Website\Forms\CheckingFieldType;
use App\Enums\Website\Forms\TextFieldType;
use App\Models\Data\City;
use Illuminate\View\Component;

class VacancyForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    private array $cities;

    public function __construct()
    {
        $this->cities = City::enabled()->get()->toArray();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.vacancy-form', [
            'cities' => $this->cities,
            'inputTypes' => [
                'radio' => CheckingFieldType::RADIO(),
                'checkbox' => CheckingFieldType::CHECKBOX(),
                'text' => TextFieldType::TEXT(),
                'number' => TextFieldType::NUMBER(),
                'tel' => TextFieldType::TEL()
            ]
        ]);
    }
}
