<?php

namespace App\View\Components\Block;

use App\Models\Data\Student;
use App\View\Components\Content\Students;
use App\View\Data\Block\StudentsBlockData;
use Illuminate\View\Component;

class StudentsBlock extends Component
{
    private StudentsBlockData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(StudentsBlockData $data)
    {
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.students-block', [
            'students' => $this->data->getGallery()->images()->get()
        ]);
    }
}
