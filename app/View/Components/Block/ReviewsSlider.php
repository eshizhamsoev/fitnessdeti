<?php

namespace App\View\Components\Block;

use App\Enums\Website\SvgIconName;
use App\Models\Data\Review;
use App\Models\Website\Page;
use App\Services\SettingsFields;
use App\View\Data\Block\ReviewsData;
use Illuminate\View\Component;

class ReviewsSlider extends Component
{
    private ReviewsData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(ReviewsData $data)
    {
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $pageId = nova_get_setting(SettingsFields::REVIEWS);

        if ($pageId) {
            $page = Page::find($pageId);
            $pageUrl = $page && $page->url ? $page->url->url : request()->path();

        } else {
            $pageUrl = request()->path();
        }

        return view('components.block.reviews-slider', [
            'header' => $this->data->getHeader(),
            'buttonText' => $this->data->getButtonText(),
            'playIcon' => SvgIconName::PLAY(),
            'reviews' => Review::with(['media', 'discipline'])->whereHas('media')->limit(10)->get(),
            'allReviewsLink' => $pageUrl
        ]);
    }
}
