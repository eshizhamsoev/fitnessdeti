<?php

namespace App\View\Components\Block;

use App\Services\CurrentCityRepository;
use App\View\Data\Block\TrainingGroupData;
use Illuminate\View\Component;

class TrainingGroup extends Component
{


    private TrainingGroupData $data;
    private CurrentCityRepository $currentCityRepository;

    public function __construct(TrainingGroupData $data, CurrentCityRepository $currentCityRepository)
    {
        $this->data = $data;
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        $trainingGroups = \App\Models\Data\TrainingGroup::query()
            ->byType($this->data->getType())
            ->byCity($this->currentCityRepository->getCurrentCity())
            ->orderBy('priority')
            ->get();

        return view('components.block.training-group', [
            'training_groups' => $trainingGroups,
            'heading' => $this->data->getHeading(),
            'leading' => $this->data->getLeading(),
        ]);
    }
}
