<?php

namespace App\View\Components\Block;

use App\View\Data\Block\FaqData;
use Illuminate\View\Component;

class Faq extends Component
{
    private FaqData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(FaqData $data)
    {
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.faq', [
            'questions' => $this->data->getFaq()->questions()->get()
        ]);
    }
}
