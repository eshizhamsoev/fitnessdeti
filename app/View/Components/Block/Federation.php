<?php

namespace App\View\Components\Block;

use App\View\Data\Block\FederationData;
use Illuminate\View\Component;

class Federation extends Component
{
    private FederationData $data;

    public function __construct(FederationData $data)
    {

        $this->data = $data;
    }

    public function render()
    {
        return view('components.block.federation', [
            'federationData' => $this->data
        ]);
    }
}
