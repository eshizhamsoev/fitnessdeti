<?php

namespace App\View\Components\Block;

use App\Services\CurrentCityRepository;
use App\View\Data\Block\TrainingGroupWithSwitchData;
use Illuminate\View\Component;

class TrainingGroupWithSwitch extends Component
{

    private TrainingGroupWithSwitchData $data;
    private CurrentCityRepository $currentCityRepository;

    public function __construct(
        TrainingGroupWithSwitchData $data,
        CurrentCityRepository $currentCityRepository
    ) {
        $this->data = $data;
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {

        $left = \App\Models\Data\TrainingGroup::byType($this->data->getTypeLeft())
            ->byCity($this->currentCityRepository->getCurrentCity())
            ->orderBy('priority')
            ->get();

        $right = \App\Models\Data\TrainingGroup::byType($this->data->getTypeRight())
            ->byCity($this->currentCityRepository->getCurrentCity())
            ->orderBy('priority')
            ->get();

        return view('components.block.training-group-with-switch', [
            'left_groups' => $left,
            'right_groups' => $right,

            'left_name' => $this->data->getNameLeft(),
            'right_name' => $this->data->getNameRight(),

            'heading' => $this->data->getHeading(),
            'leading' => $this->data->getLeading(),
        ]);
    }
}
