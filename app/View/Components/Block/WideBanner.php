<?php

namespace App\View\Components\Block;

use App\Enums\Website\WideBannerDesktopImagePosition;
use App\View\Data\Block\WideBannerData;
use Illuminate\View\Component;

class WideBanner extends Component
{
    private WideBannerData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(WideBannerData $data)
    {
        $this->data = $data;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.wide-banner', [
            'desktopImages' => $this->data->getDesktopGallery()->images ?? null,
            'mobileImages' => $this->data->getMobileGallery()->images ?? null,
            'buttonType' => $this->data->getButtonType(),
            'buttonText' => $this->data->getButtonText(),
            'formHeading' => $this->data->getFormHeading(),
            'formLeading' => $this->data->getFormLeading(),
            'formSource' => $this->data->getFormSource(),
            'desktopImagePosition' => $this->data->getWideBannerDesktopImagePosition()
        ]);
    }
}
