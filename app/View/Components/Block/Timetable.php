<?php

namespace App\View\Components\Block;

use Illuminate\View\Component;

class Timetable extends Component
{
    private ?string $source;

    public function __construct(string $source = null)
    {
        $this->source = $source;
    }

    public function render()
    {
        return view('components.block.timetable', [
            'source' => 'Расписание занятий' . $this->source ? (' ' . $this->source) : ''
        ]);
    }
}
