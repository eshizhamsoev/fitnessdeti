<?php

namespace App\View\Components\Block;

use App\Enums\Website\GalleryType;
use App\View\Data\Block\GalleryData;
use Illuminate\View\Component;

class Gallery extends Component
{
    private GalleryData $data;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(GalleryData $data)
    {
        $this->data = $data;
    }


    private function selectView(string $name)
    {
        switch ($name) {
            case GalleryType::OUR_SERVICES;
                return 'block.our-services';
            case GalleryType::THREE_CARD;
                return 'block.three-card';
            case GalleryType::ADDITIONAL_SERVICE_CAROUSEL;
                return 'block.additional-service-slider';
            case GalleryType::CLASSES;
                return 'block.classes';
            case GalleryType::SERVICES;
                return 'block.services';
            case GalleryType::ABOUT_COMPANY;
                return 'block.about-company';
        }
    return '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $gallery = $this->data->getGallery();
        $gallery->load(['images', 'images.media']);
        return view('components.block.gallery', [
            'type' => $this->selectView($this->data->getGalleryType()),
            'header' => $this->data->getBlockName(),
            'images' => $gallery->images
        ]);
    }
}
