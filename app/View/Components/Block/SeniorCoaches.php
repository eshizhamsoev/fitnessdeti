<?php

namespace App\View\Components\Block;

use App\Enums\CoachType;
use App\Models\Data\Coach;
use App\View\Data\Block\SeniorCoachesData;
use Illuminate\View\Component;

class SeniorCoaches extends Component
{
    private SeniorCoachesData $data;

    public function __construct(SeniorCoachesData $data)
    {
        $this->data = $data;
    }

    public function render()
    {
        $coaches = Coach::byType(CoachType::Senior())->get();
        return view('components.block.senior-coaches', [
            'heading' => $this->data->getHeading(),
            'firstCoach' => $coaches[0] ?? null,
            'secondCoach' => $coaches[1] ?? null,
        ]);
    }
}
