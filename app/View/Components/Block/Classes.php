<?php

namespace App\View\Components\Block;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class Classes extends Component
{
    private object $images;
    private string $header;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(object $images, string $header)
    {

        $this->images = $images;
        $this->header = $header;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.block.classes', [
            'arrowIcon' => SvgIconName::ARROW(),
            'images' => $this->images,
            'header' => $this->header
            ]);
    }
}
