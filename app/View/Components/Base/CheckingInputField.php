<?php

namespace App\View\Components\Base;

use App\Enums\Website\Forms\CheckingFieldType;
use Illuminate\View\Component;

class CheckingInputField extends Component
{
    private string $name;
    private CheckingFieldType $type;
    private string $value;
    private bool $isChecked;
    private bool $isDisabled;
    private ?string $label;

    public function __construct(
        string $name,
        CheckingFieldType $type,
        ?string $label = null,
        string $value = '1',
        bool $isChecked = false,
        bool $isDisabled = false
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
        $this->isChecked = $isChecked;
        $this->isDisabled = $isDisabled;
        $this->label = $label;
    }

    public function render()
    {
        return view('components.base.checking-input-field', [
            'name' => $this->name,
            'type' => $this->type,
            'label' => $this->label,
            'value' => $this->value,
            'isChecked' => $this->isChecked,
            'isDisabled' => $this->isDisabled
        ]);
    }
}
