<?php

namespace App\View\Components\Base;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class SvgIcon extends Component
{
    private int $width;
    private int $height;
    private SvgIconName $icon;

    public function __construct(int $width, int $height, SvgIconName $icon)
    {
        $this->width = $width;
        $this->height = $height;
        $this->icon = $icon;
    }

    public function render()
    {
        return view('components.base.svg-icon', [
            'width' => $this->width,
            'height' => $this->height,
            'href' => sprintf('%s#%s', asset('/sprite.svg'), $this->icon->value)
        ]);
    }
}
