<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Fieldset extends Component
{
    private ?string $title;
    private bool $center;
    private bool $column;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(?string $title = null, bool $center = false, bool $column = false)
    {
        $this->title = $title;
        $this->center = $center;
        $this->column = $column;
        if ($this->center) {
            $this->attributes->only(['class'])->merge([
               'class' => 'fieldset_center'
            ]);
        }

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.base.fieldset', [
            'title' => $this->title,
            'column' => $this->column
        ]);
    }
}
