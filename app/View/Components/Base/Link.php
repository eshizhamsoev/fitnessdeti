<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Link extends Component
{
    private ?string $href;
    private ?string $currentLinkClass;

    public function __construct(?string $href, ?string $currentLinkClass = null)
    {
        $this->href = $href;
        $this->currentLinkClass = $currentLinkClass;
    }

    private function makePathWithQuery()
    {

        $queryString = request()->getQueryString() ?  : '';
        $path = '/' . ltrim(request()->path(), '/');

        return $queryString ? $path . '?' . $queryString : $path;
    }

    public function render()
    {
        $inNewTab = $this->href ? str_contains($this->href, 'http') : null;

        $currentPathWithQuery = $this->makePathWithQuery();

        $isCurrent = is_null($this->href) || $this->href === $currentPathWithQuery;

        return view('components.base.link', [
            'href' => $this->href,
            'isCurrent' => $isCurrent,
            'inNewTab' => $inNewTab,
            'currentLinkClass' => $this->currentLinkClass
        ]);
    }
}
