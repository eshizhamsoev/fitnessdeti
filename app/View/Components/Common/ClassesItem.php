<?php

namespace App\View\Components\Common;

use App\Models\Website\GalleryImage;
use Illuminate\View\Component;

class ClassesItem extends Component
{
    private GalleryImage $image;
    private int $count;

    public function __construct(GalleryImage $image, int $count)
    {
        $this->image = $image;
        $this->count = $count;
    }

    public function render()
    {
        return view('components.common.classes-item', [
            'image' => $this->image,
            'count' => $this->count
        ]);
    }
}
