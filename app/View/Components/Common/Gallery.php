<?php

namespace App\View\Components\Common;

use App\View\Data\Gallery\GalleryData;
use Illuminate\View\Component;

class Gallery extends Component
{
    static int $renderedId = 0;
    private GalleryData $galleryData;

    public function __construct(GalleryData $galleryData)
    {
        $this->galleryData = $galleryData;
    }

    public function render()
    {
        self::$renderedId += 1;
        return view('components.common.gallery', [
            'galleryData' => $this->galleryData,
            'galleryName' => sprintf('gallery-%s', self::$renderedId),
            'showMore' => false
        ]);
    }
}
