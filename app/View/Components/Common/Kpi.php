<?php

namespace App\View\Components\Common;

use Illuminate\View\Component;

class Kpi extends Component
{
    public function render()
    {
        return view('components.common.kpi');
    }
}
