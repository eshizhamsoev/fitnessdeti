<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use App\Models\Data\School;
use Illuminate\View\Component;
use function view;

class SchoolCard extends Component
{
    private School $school;

    public function __construct(School $school)
    {
        $this->school = $school;
    }

    public function render()
    {
        return view('components.common.school-card', [
            'school' => $this->school,
            'school_url' => $this->school->url ? $this->school->url->url : null,
            'telegramIcon' => SvgIconName::TELEGRAM(),
            'whatsappIcon' => SvgIconName::WHATSAPP(),
            'buildingIcon' => SvgIconName::BUILDING(),
            'metroIcon' => SvgIconName::METRO()
        ]);
    }
}
