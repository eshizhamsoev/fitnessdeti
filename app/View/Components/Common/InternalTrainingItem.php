<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class InternalTrainingItem extends Component
{
    private string $text;
    private SvgIconName $icon;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $text, SvgIconName $icon)
    {
        //
        $this->text = $text;
        $this->icon = $icon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.internal-training-item', [
            'text' => $this->text,
            'icon' => $this->icon
        ]);
    }
}
