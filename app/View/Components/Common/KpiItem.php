<?php

namespace App\View\Components\Common;

use App\Enums\Website\Kpi\KpiItemSize;
use App\View\Data\Common\KpiItemData;
use Illuminate\View\Component;

class KpiItem extends Component
{
    private KpiItemData $data;
    private ?KpiItemSize $size;

    public function __construct(
        KpiItemData $data,
        ?KpiItemSize $size = null
    ) {
        $this->data = $data;
        $this->size = $size;
    }

    public function render()
    {
        return view('components.common.kpi-item', [
            'data' => $this->data,
            'size' => $this->size
        ]);
    }
}
