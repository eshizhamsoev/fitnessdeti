<?php

namespace App\View\Components\Common;

use App\View\Data\Common\TabData;
use Illuminate\View\Component;

class Tabs extends Component
{
    private string $color;
    /**
     * @var TabData[]
     */
    private array $tabs;

    /**
     * Create a new component instance.
     *
     * @param string $color
     * @param TabData[] $tabs
     */
    public function __construct(string $color, array $tabs)
    {
        $this->color = $color;
        $this->tabs = $tabs;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.tabs', [
            'color' => $this->color,
            'tabs' => $this->tabs
        ]);
    }
}
