<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;
use function view;

class Carousel extends Component
{
    private string $sliderName;
    private bool $hasShadow;

    public function __construct(string $sliderName, bool $hasShadow = false)
    {
        $this->sliderName = $sliderName;
        $this->hasShadow = $hasShadow;
    }

    public function render()
    {
        return view('components.common.carousel', [
            'sliderName' => $this->sliderName,
            'arrowIcon' => SvgIconName::ARROW(),
            'hasShadow' => $this->hasShadow
        ]);
    }
}
