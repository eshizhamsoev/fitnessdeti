<?php

namespace App\View\Components\Common;

use App\Models\Data\Coach;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class CoachesCarousel extends Component
{
    private array $coaches;

    /**
     * Create a new component instance.
     *
     * @param Coach[] $coaches
     */
    public function __construct(array $coaches)
    {
        $this->coaches = $coaches;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.coaches-carousel',[
            'coaches' => $this->coaches
        ]);
    }
}
