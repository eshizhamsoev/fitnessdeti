<?php

namespace App\View\Components\Common;

use App\Enums\Website\WideBannerDesktopImagePosition;
use App\Models\Website\GalleryImage;
use Illuminate\View\Component;

class WideBannerSlide extends Component
{
    private ?GalleryImage $desktopImage;
    private ?GalleryImage $mobileImage;
    private string $buttonType;
    private string $buttonText;
    private string $formHeading;
    private string $formLeading;
    private string $formSource;
    private ?WideBannerDesktopImagePosition $desktopImagePosition;

    /**
     * Create a new component instance.
     *
     * @param GalleryImage|null $desktopImage
     * @param GalleryImage|null $mobileImage
     * @param $buttonType
     * @param $buttonText
     * @param $formHeading
     * @param $formLeading
     * @param $formSource
     * @param WideBannerDesktopImagePosition|null $desktopImagePosition
     */
    public function __construct(
        ?GalleryImage $desktopImage,
        ?GalleryImage $mobileImage,
        $buttonType,
        $buttonText,
        $formHeading,
        $formLeading,
        $formSource,
        ?WideBannerDesktopImagePosition $desktopImagePosition = null
    )
    {
        $this->desktopImage = $desktopImage;
        $this->mobileImage = $mobileImage;
        $this->buttonType = $buttonType;
        $this->buttonText = $buttonText;
        $this->formHeading = $formHeading;
        $this->formLeading = $formLeading;
        $this->formSource = $formSource;
        $this->desktopImagePosition = $desktopImagePosition;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.wide-banner-slide', [
            'desktopImage' => $this->desktopImage,
            'mobileImage' => $this->mobileImage,
            'buttonType' => $this->buttonType,
            'buttonText' => $this->buttonText,
            'formHeading' => $this->formHeading,
            'formLeading' => $this->formLeading,
            'formSource' => $this->formSource,
            'desktopImagePosition' => $this->desktopImagePosition? $this->desktopImagePosition->value : WideBannerDesktopImagePosition::POSITION_CENTER
        ]);
    }
}
