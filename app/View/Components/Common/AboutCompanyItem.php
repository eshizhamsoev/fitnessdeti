<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class AboutCompanyItem extends Component
{
    private object $image;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(object $image)
    {
        //
        $this->image = $image;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.about-company-item', ['image' => $this->image, 'arrow' => SvgIconName::ARROW_RIGHT()]);
    }
}
