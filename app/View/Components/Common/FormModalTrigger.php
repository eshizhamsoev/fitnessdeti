<?php

namespace App\View\Components\Common;

use App\Models\Website\Page;
use App\Services\SettingsFields;
use Illuminate\View\Component;

class FormModalTrigger extends Component
{
    private string $source;
    private string $heading;
    private string $leading;

    public function __construct(
        string $source,
        string $heading = 'Позвонить мне',
        string $leading = 'Оставьте заявку и мы с вами свяжемся'
    ) {
        $this->source = $source;
        $this->heading = $heading;
        $this->leading = $leading;
    }

    public function render()
    {
        return view('components.common.form-modal-trigger', [
            'source' => $this->source,
            'heading' => $this->heading,
            'leading' => $this->leading,
            'privacyUrl' => $this->getPrivacy()
        ]);
    }

    private function getPrivacy(): ?string
    {
        $page = Page::find(nova_get_setting(SettingsFields::PRIVACY_PAGE));
        return $page && $page->url ? $page->url->url : null;
    }
}
