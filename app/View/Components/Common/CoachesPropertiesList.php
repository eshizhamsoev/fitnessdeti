<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class CoachesPropertiesList extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.coaches-properties-list', [
            'items' => [
                [
                    'icon' => SvgIconName::TROPHY_COLOR(),
                    'title' => 'КМС/МС/МСМК'
                ],
                [
                    'icon' => SvgIconName::THUMBS_UP(),
                    'title' => 'Опыт больше 2-х лет'
                ],
                [
                    'icon' => SvgIconName::CERTIFICATE_COLOR(),
                    'title' => 'Спец. образование'
                ]
            ]
        ]);
    }
}
