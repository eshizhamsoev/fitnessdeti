<?php

namespace App\View\Components\Common;

use App\Enums\Website\SvgIconName;
use Illuminate\View\Component;

class PropertiesItem extends Component
{
    private string $title;
    private SvgIconName $icon;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $title, SvgIconName $icon)
    {

        $this->title = $title;
        $this->icon = $icon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.properties-item', [
            'title' => $this->title,
            'icon' => $this->icon
        ]);
    }
}
