<?php

namespace App\View\Components\Common;

use App\Models\Data\Review;
use Illuminate\View\Component;

class ReviewsBlockItem extends Component
{
    private Review $review;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.reviews-block-item',[
            'review' => $this->review,
            'image' => $this->review->getFirstMedia(Review::IMAGE_COLLECTION_REVIEW),
            'avatar' => $this->review->getFirstMedia(Review::IMAGE_COLLECTION_AVATAR),
        ]);
    }
}
