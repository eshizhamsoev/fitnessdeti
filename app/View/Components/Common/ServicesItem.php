<?php

namespace App\View\Components\Common;

use Illuminate\View\Component;

class ServicesItem extends Component
{
    private object $image;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(object $image)
    {
        $this->image = $image;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.common.services-item', ['image' => $this->image]);
    }
}
