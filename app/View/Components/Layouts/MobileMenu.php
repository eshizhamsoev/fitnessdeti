<?php

namespace App\View\Components\Layouts;

use App\Enums\Website\SvgIconName;
use App\Services\CurrentCityRepository;
use App\Services\SettingsFields;
use Illuminate\View\Component;

class MobileMenu extends Component
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        $current = $this->currentCityRepository->getCurrentCity();
        return view('components.layouts.mobile-menu', [
            'phone' => $current->phone,
            'socials' => [
                [
                    'link' => $current->whatsapp,
                    'icon' => SvgIconName::WHATSAPP()
                ],
                [
                    'link' => $current->telegram,
                    'icon' => SvgIconName::TELEGRAM()
                ],
                [
                    'link' => nova_get_setting(SettingsFields::SOCIAL_INSTAGRAM),
                    'icon' => SvgIconName::INSTAGRAM()
                ]
            ],
            'links' => [
                [
                    "name" => "Художественная гимнастика",
                    "link" => "/gymnastic",
                ],
                [
                    "name" => "Спортивная акробатика",
                    "link" => "/acrobatics",
                ],
                [
                    "name" => "О школе",
                    "link" => "/about",
                    "children" => [
                        [
                            "name" => "О школе",
                            "link" => "/about",
                        ],
                        [
                            "name" => "Будущее спорта",
                            "link" => "/about/future",
                        ],
                        [
                            "name" => "История школы",
                            "link" => "/about/history",
                        ],
                    ],
                ],
                [
                    "name" => "Услуги",
                    "link" => "/uslugi",
                    "children" => [
                        [
                            "name" => "Подготовительные группы",
                            "link" => "/initial-groups",
                        ],
                        [
                            "name" => "Персональные тренировки",
                            "link" => "/personal-training",
                        ],
                        [
                            "name" => "Онлайн-тренировки",
                            "link" => "/online",
                        ],
                        [
                            "name" => "Фотосессии",
                            "link" => "/photo-session",
                        ],
                        [
                            "name" => "Спортивное страхование",
                            "link" => "/sportivnoe-strahovanie",
                        ],
                        [
                            "name" => "Пошив и аренда купальников",
                            "link" => "/poshiv-i-arenda-kupalnikov",
                        ],
                        [
                            "name" => "Брендированная форма",
                            "link" => "/brendirovannaya-forma",
                        ],
                    ],
                ],
                [
                    "name" => "Адреса школ",
                    "link" => "/school-addresses",
                ],
                [
                    "name" => "Тренерский состав",
                    "link" => "/treners",
                ],
                [
                    "name" => "Родителям",
                    "link" => "/parents",
                ],
                [
                    "name" => "Соревнования",
                    "link" => "/turnaments",
                ],
                [
                    "name" => "Сборы и мероприятия",
                    "link" => "/cups",
                ],
                [
                    "name" => "Экзамены и программы",
                    "link" => "/exams-and-programs",
                ],
                [
                    "name" => "Акции",
                    "link" => "/sales",
                ],
                [
                    "name" => "Интернет магазин",
                    "link" => "https://fd-store.ru/",
                ],
            ]
        ]);
    }

    private function getEmailLink(): ?string
    {
        if (!nova_get_setting(SettingsFields::EMAIL)) {
            return null;
        }
        return 'mailto:' . nova_get_setting(SettingsFields::EMAIL);
    }
}
