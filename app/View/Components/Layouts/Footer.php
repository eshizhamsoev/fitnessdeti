<?php

namespace App\View\Components\Layouts;

use App\Enums\Website\SvgIconName;
use App\Models\Website\Page;
use App\Services\CurrentCityRepository;
use App\Services\SettingsFields;
use Illuminate\View\Component;

class Footer extends Component
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        return view('components.layouts.footer', [
            'phone' => $this->currentCityRepository->getCurrentCity()->phone,
            'email' => nova_get_setting(SettingsFields::EMAIL),

            'socials' => [
                [
                    'link' => nova_get_setting(SettingsFields::SOCIAL_YOUTUBE),
                    'name' => 'Наш youtube канал',
                    'icon' => SvgIconName::YOUTUBE(),
                ],

                [
                    'link' => nova_get_setting(SettingsFields::SOCIAL_INSTAGRAM),
                    'name' => 'Наш Instagram',
                    'icon' => SvgIconName::INSTAGRAM(),
                ],

                [
                    'link' => nova_get_setting(SettingsFields::SOCIAL_FACEBOOK),
                    'name' => 'Наш facebook',
                    'icon' => SvgIconName::FACEBOOK(),
                ],

                [
                    'link' => nova_get_setting(SettingsFields::SOCIAL_VK),
                    'name' => 'Мы в vk',
                    'icon' => SvgIconName::VK(),
                ],

                [
                    'link' => nova_get_setting(SettingsFields::SOCIAL_TIKTOK),
                    'name' => 'Мы в TikTok',
                    'icon' => SvgIconName::TIKTOK(),
                ]
            ],
            'schools_page_url' => $this->getPageUrl(SettingsFields::SCHOOL),
            'privacy_page_url' => $this->getPageUrl(SettingsFields::PRIVACY_PAGE),
            'safety_rules_page_url' => $this->getPageUrl(SettingsFields::SAFETY_RULES_PAGE),
            'offer_and_acceptance_url' => $this->getPageUrl(SettingsFields::OFFER_AND_ACCEPTANCE),
            'llc' => $this->getPageUrl(SettingsFields::LLC),
            'user_agreement' => $this->getPageUrl(SettingsFields::USER_AGREEMENT),

//            'phone' => $this
            'menu' => [
                'services' => [
                    "name" => "Услуги",
                    "link" => "/uslugi",
                    "children" => [
                        [
                            "name" => "Художественная гимнастика",
                            "link" => "/gymnastic",
                        ],
                        [
                            "name" => "Спортивная акробатика",
                            "link" => "/acrobatics",
                        ],
                        [
                            "name" => "Подготовительные группы",
                            "link" => "/initial-groups",
                        ],
                        [
                            "name" => "Персональные тренировки",
                            "link" => "/personal-training",
                        ],
                        [
                            "name" => "Онлайн-тренировки",
                            "link" => "/online",
                        ],
                        [
                            "name" => "Постановка программ",
                            "link" => "/setting-programm",
                        ],
                        [
                            "name" => "Спортивное страхование",
                            "link" => "/sportivnoe-strahovanie",
                        ],
                        [
                            "name" => "Пошив и аренда купальников",
                            "link" => "/poshiv-i-arenda-kupalnikov",
                        ],
                        [
                            "name" => "Фотосессии",
                            "link" => "/photo-session",
                        ],
                        [
                            "name" => "Брендированная форма",
                            "link" => "/brendirovannaya-forma",
                        ],
                        [
                            "name" => "Интернет-магазин",
                            "link" => "http://fd-store.ru/",
                        ],
                        [
                            "name" => "Спецпредложения",
                            "link" => "/specpredlozheniya",
                        ],
                    ],
                ],
                'about' => [
                    "name" => "О школе FD",
                    "link" => "/about",
                    "children" => [
                        [
                            "name" => "О школе",
                            "link" => "/about",
                        ],
                        [
                            "name" => "История школы",
                            "link" => "/about/history",
                        ],
                        [
                            "name" => "Будущее спорта",
                            "link" => "/about/future",
                        ],
                        [
                            "name" => "Сборная FD",
                            "link" => "/about/future#f1",
                        ],
                        [
                            "name" => "Звёзды FD",
                            "link" => "/about/future#f2",
                        ],
                        [
                            "name" => "Тренеры",
                            "link" => "/treners",
                        ],
                    ],
                ],
                'events' => [
                    "name" => "Мероприятия",
                    "link" => "/turnaments",
                    "children" => [
                        [
                            "name" => "Соревнования",
                            "link" => "/turnaments",
                        ],
                        [
                            "name" => "Лагеря и сборы",
                            "link" => "/cups",
                        ],
                        [
                            "name" => "Мастер-классы",
                            "link" => "/master-classes",
                        ],
                        [
                            "name" => "Экзамены и программы",
                            "link" => "/exams-and-programs",
                        ],
                        [
                            "name" => "Сертификация и обучение",
                            "link" => "/sertifikaciya-i-obuchenie",
                        ],
                    ],
                ],
            ]
        ]);
    }

    private function getSchoolPageUrl()
    {
        $page = Page::find(nova_get_setting(SettingsFields::SCHOOL));
        if (!$page || !$page->url) {
            return null;
        }
        return $page->url->url;
    }



    private function getPrivacyUrl(): ?string
    {
        $page = Page::find(nova_get_setting(SettingsFields::PRIVACY_PAGE));
        return $page && $page->url ? $page->url->url : null;
    }


    private function getSafetyRulesUrl(): ?string
    {
        $page = Page::find(nova_get_setting(SettingsFields::SAFETY_RULES_PAGE));
        return $page && $page->url ? $page->url->url : null;
    }

    private function getPageUrl(string $field): ?string
    {
        $page = Page::find(nova_get_setting($field));
        return $page && $page->url ? $page->url->url : null;
    }
}
