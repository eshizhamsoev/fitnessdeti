<?php

namespace App\View\Components\Layouts;

use App\Services\UrlGenerator\Urlable;
use App\Models\Website\Url;
use App\Services\BreadcrumbsService\BreadcrumbsService;
use Illuminate\View\Component;

class Breadcrumbs extends Component
{
    private Url $url;
    private BreadcrumbsService $service;

    public function __construct(BreadcrumbsService $service, Url $url)
    {
        $this->service = $service;
        $this->url = $url;
    }

    public function render()
    {

        return view('components.layouts.breadcrumbs', ['links' => $this->service->getBreadcrumbs($this->url)]);
    }
}
