<?php

namespace App\View\Components\Layouts;

use App\ConfigurableBlockSettings\SettingResolver;
use App\Models\Website\ConfigurableBlock;
use App\Nova\Website\ConfigurableNovaBlock;
use Illuminate\View\Component;

class Blocks extends Component
{
    private array $blocks;
    private SettingResolver $resolver;

    public function __construct(SettingResolver $resolver, array $blocks)
    {
        $this->blocks = $blocks;
        $this->resolver = $resolver;
    }

    public function render()
    {
        return view('components.layouts.blocks', [
            'blocks' => array_map(fn(ConfigurableBlock $block
            ) => [
                'is_grey' => $block->pivot->is_grey,
                'type' => 'block.' . $block->type,
                'data' => $this->resolver->resolve($block->type)->extractSettings($block->settings)
            ], $this->blocks)
        ]);
    }
}
