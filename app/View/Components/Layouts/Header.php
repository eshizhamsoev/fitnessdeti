<?php

namespace App\View\Components\Layouts;

use App\Enums\Website\SvgIconName;
use App\Models\Data\City;
use App\Services\CurrentCityRepository;
use App\Services\SettingsFields;
use Illuminate\View\Component;

class Header extends Component
{
    private CurrentCityRepository $currentCityRepository;

    public function __construct(CurrentCityRepository $currentCityRepository)
    {
        $this->currentCityRepository = $currentCityRepository;
    }

    public function render()
    {
        $city = $this->currentCityRepository->getCurrentCity();
        return view('components.layouts.header', [
            'phone' => $city->phone,
            'cityName' => $city->name,
            'cities' => City::enabled()->whereNotNull('domain')->orderByDesc('priority')->get(),
            'contact_links' => [
                [
                    'icon' => SvgIconName::INSTAGRAM(),
                    'link' => nova_get_setting(SettingsFields::SOCIAL_INSTAGRAM)
                ],
                [
                    'icon' => SvgIconName::WHATSAPP(),
                    'link' => $city->whatsapp
                ],
                [
                    'icon' => SvgIconName::TELEGRAM(),
                    'link' => $city->telegram
                ]
            ],
            'links' => [
                [
                    "name" => "О школе",
                    "link" => "/about",
                    "children" => [
                        [
                            "name" => "О школе",
                            "link" => "/about",
                        ],
                        [
                            "name" => "Будущее спорта",
                            "link" => "/about/future",
                        ],
                        [
                            "name" => "История школы",
                            "link" => "/about/history",
                        ],
                        [
                            "name" => "Стать тренером",
                            "link" => "/vakansii",
                        ]
                    ],
                ],
                [
                    "name" => "Акции",
                    "link" => "/sales",
                ],
                [
                    "name" => "Услуги",
                    "link" => "/uslugi",
                    "children" => [
                        [
                            "name" => "Художественная гимнастика",
                            "link" => "/gymnastic",
                        ],
                        [
                            "name" => "Спортивная акробатика",
                            "link" => "/acrobatics",
                        ],
                        [
                            "name" => "Подготовительные группы",
                            "link" => "/initial-groups",
                        ],
                        [
                            "name" => "Персональные тренировки",
                            "link" => "/personal-training",
                        ],
                        [
                            "name" => "Онлайн-тренировки",
                            "link" => "/online",
                        ],
                    ],
                ],
                [
                    "name" => "Адреса школ",
                    "link" => "/school-addresses",
                ],
                [
                    "name" => "Родителям",
                    "link" => "/parents",
                ],
                [
                    "name" => "Тренерский состав",
                    "link" => "/treners",
                ],
                [
                    "name" => "Соревнования",
                    "link" => "/turnaments",
                ],
                [
                    "name" => "Сборы и мероприятия",
                    "link" => "/cups",
                ],
            ]
        ]);
    }
}
