<?php

namespace App\Listeners;

use App\Domain\Leads\Events\VacancyApplicationCreated;
use App\Notifications\NewVacancyApplicationNotification;
use App\Services\SettingsFields;
use Notification;

class NewVacancyApplicationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VacancyApplicationCreated  $event
     * @return void
     */
    public function handle(VacancyApplicationCreated $event)
    {
        $receivers = nova_get_setting(SettingsFields::VACANCY_APPLICATION_EMAIL);
        if ($receivers) {
            $notification = new NewVacancyApplicationNotification($event->getVacancyApplication());
            Notification::route('mail', $receivers)->notify($notification);
        }
    }
}
