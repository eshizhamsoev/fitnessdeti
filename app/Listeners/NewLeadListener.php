<?php

namespace App\Listeners;

use App\Domain\Leads\Events\LeadCreated;
use App\Notifications\NewLeadNotification;
use App\Services\SettingsFields;
use Notification;

class NewLeadListener
{
    public function __construct()
    {
        //
    }

    public function handle(LeadCreated $event)
    {
        $receivers = nova_get_setting(SettingsFields::EMAIL_NOTIFICATION_LEAD);
        if ($receivers) {
            $notification = new NewLeadNotification($event->getLead());
            Notification::route('mail', $receivers)->notify($notification);
        }
    }
}
