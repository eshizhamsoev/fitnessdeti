<?php

namespace App\Support;

use Illuminate\Support\Facades\Log;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\ImageFactory;

trait CroppedResponsiveImage
{
    public function responsiveCrop(
        Conversion $conversion,
        Media $media,
        string $collectionName,
        int $targetWidth,
        int $targetHeight
    ): ?Conversion {

        if($collectionName !== $media->collection_name) {
            return null;
        }

        if ($media->hasGeneratedConversion($conversion->getName())) {
            return $conversion;
        }
        if (!is_file($media->getPath())) {
            return $conversion;
        }

        if (pathinfo($media->getPath(), PATHINFO_EXTENSION) === 'svg') {
            return $conversion;
        }

        $image = ImageFactory::load($media->getPath());

        $width = $image->getWidth();
        $height = $image->getHeight();

        $doubleSizedWidth = $targetWidth * 2;
        $doubleSizedHeight = $targetHeight * 2;

        if ($width > $doubleSizedWidth && $height > $doubleSizedHeight) {
            $cropWidth = $doubleSizedWidth;
            $cropHeight = $doubleSizedHeight;
        } else {
            $aspectRatio = $targetWidth / $targetHeight;
            $cropWidth = min($width, $height * $aspectRatio);
            $cropHeight = min($height, $width / $aspectRatio);
        }

        return $conversion
            ->fit(
                Manipulations::FIT_CROP,
                $cropWidth,
                $cropHeight
            )
            ->optimize()
            ->withResponsiveImages();

    }
}
