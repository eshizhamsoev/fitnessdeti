<?php


namespace App\Domain\Leads\Models;


use App\Domain\Leads\Events\LeadCreated;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $dispatchesEvents = [
        'created' => LeadCreated::class,
    ];

    protected $casts = [
        'utm_data' => 'json'
    ];
}
