<?php

namespace App\Domain\Leads\Events;

use App\Models\Data\VacancyApplication;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VacancyApplicationCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private VacancyApplication $vacancyApplication;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VacancyApplication $vacancyApplication)
    {
        $this->vacancyApplication = $vacancyApplication;
    }

    /**
     * @return VacancyApplication
     */
    public function getVacancyApplication(): VacancyApplication
    {
        return $this->vacancyApplication;
    }


}
