<?php


namespace App\Domain\Leads\Services;


use App\Domain\Leads\Data\LeadData;
use App\Domain\Leads\Models\Lead;

class LeadService
{
    public function createLead(LeadData $data): Lead
    {
        $lead = new Lead();
        $lead->name = $data->getName();
        $lead->phone = $data->getPhone();
        $lead->source = $data->getSource();
        $lead->url = $data->getUrl();
        $lead->utm_data = $data->getUtmData()->toArray();
        $lead->save();
        return $lead;
    }
}
