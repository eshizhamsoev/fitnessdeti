<?php

namespace App\Domain\Leads\Services;

use App\Http\Middleware\SetUtmData;
use App\Models\Data\VacancyApplication;
use Illuminate\Http\Request;

class VacancyApplicationService
{
    public static function createApplication(Request $request)
    {
        $utmData = [];

        foreach (SetUtmData::UTM_FIELDS as $field)
        {
            $utmData = array_merge($utmData, [$field => session()->get($field)]);
        }

        $validatedData = $request->validate([
            'city' => ['required', 'max:255'],
            'grade' => ['nullable'],
            'sport' => ['nullable'],
            'telephone' => ['required', 'max:20'],
            'email' => ['required', 'max:255'],
            'fullname' => ['required', 'max:255'],
            'age' => ['numeric', 'nullable'],
            'education' => ['max:255', 'nullable'],
            'experience' => ['max:255', 'nullable'],
            'aboutself' => ['nullable'],
        ]);

        $model = new VacancyApplication();

        $model->city = $validatedData['city'];
        $model->rank = $validatedData['grade'] ?? null;
        $model->sport_type = $validatedData['sport'] ?? null;
        $model->phone = $validatedData['telephone'];
        $model->email = $validatedData['email'];
        $model->name = $validatedData['fullname'];
        $model->age = $validatedData['age'] ?? null;
        $model->education = $validatedData['education'] ?? null;
        $model->experience = $validatedData['experience'] ?? null;
        $model->about_applicant = $validatedData['aboutself'] ?? null;
        $model->utm = $utmData;

        $model->save();
    }
}
