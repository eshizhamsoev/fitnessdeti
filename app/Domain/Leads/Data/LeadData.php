<?php


namespace App\Domain\Leads\Data;


class LeadData
{
    private string $phone;
    private ?string $source;
    private ?string $url;
    /**
     * @var UTMData|null
     */
    private ?UTMData $utm_data;
    private string $name;

    public function __construct(
        string $name,
        string $phone,
        ?string $source = null,
        ?string $url = null,
        ?UTMData $data = null
    ) {
        $this->phone = $phone;
        $this->source = $source;
        $this->url = $url;
        $this->utm_data = $data;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @return UTMData|null
     */
    public function getUtmData(): ?UTMData
    {
        return $this->utm_data;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}
