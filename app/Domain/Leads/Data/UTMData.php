<?php


namespace App\Domain\Leads\Data;


class UTMData
{
    const UTM_MEDIUM = 'utm_medium';
    const UTM_CAMPAIGN = 'utm_campaign';
    const UTM_SOURCE = 'utm_source';
    const UTM_TERM = 'utm_term';
    const UTM_CONTENT = 'utm_content';

    private ?string $utm_medium;
    private ?string $utm_campaign;
    private ?string $utm_source;
    private ?string $utm_term;
    private ?string $utm_content;

    public function __construct(
        ?string $utm_medium,
        ?string $utm_campaign,
        ?string $utm_source,
        ?string $utm_term,
        ?string $utm_content
    ) {

        $this->utm_medium = $utm_medium;
        $this->utm_campaign = $utm_campaign;
        $this->utm_source = $utm_source;
        $this->utm_term = $utm_term;
        $this->utm_content = $utm_content;
    }

    /**
     * @return string|null
     */
    public function getUtmMedium(): ?string
    {
        return $this->utm_medium;
    }

    /**
     * @return string|null
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utm_campaign;
    }

    /**
     * @return string|null
     */
    public function getUtmSource(): ?string
    {
        return $this->utm_source;
    }

    /**
     * @return string|null
     */
    public function getUtmTerm(): ?string
    {
        return $this->utm_term;
    }

    /**
     * @return string|null
     */
    public function getUtmContent(): ?string
    {
        return $this->utm_content;
    }

    public function toArray()
    {
        return [
            self::UTM_MEDIUM => $this->utm_medium,
            self::UTM_CAMPAIGN => $this->utm_campaign,
            self::UTM_SOURCE => $this->utm_source,
            self::UTM_TERM => $this->utm_term,
            self::UTM_CONTENT => $this->utm_content
        ];
    }

    static public function fromArray(array $data)
    {
        $utm_medium = $data[self::UTM_MEDIUM] ?? null;
        $utm_campaign = $data[self::UTM_CAMPAIGN] ?? null;
        $utm_source = $data[self::UTM_SOURCE] ?? null;
        $utm_term = $data[self::UTM_TERM] ?? null;
        $utm_content = $data[self::UTM_CONTENT] ?? null;

        return new self($utm_medium, $utm_campaign, $utm_source, $utm_term, $utm_content);
    }
}
