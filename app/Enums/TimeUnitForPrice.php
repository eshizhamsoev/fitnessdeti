<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class TimeUnitForPrice extends Enum implements LocalizedEnum
{
    public const MONTH = 'month';
    public const LESSON = 'lesson';
}
