<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static SCHOOL()
 * @method static static COACH()
 * @method static static PAGE()
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class EntityType extends Enum implements LocalizedEnum
{
    const SCHOOL = 'school';
    const COACH = 'coach';
    const PAGE = 'page';
    const BLOG_POST = 'blog-post';
}
