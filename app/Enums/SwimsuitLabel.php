<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static HIT()
 * @method static static SALE()
 * @method static static TEN_PERCENT()
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SwimsuitLabel extends Enum implements LocalizedEnum
{
    const HIT = 'хит';
    const SALE =   'sale';
    const TEN_PERCENT = '10%';
}
