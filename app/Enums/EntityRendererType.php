<?php

namespace App\Enums;

use App\Models\Data\Coach;
use App\Models\Data\School;
use App\Models\Website\Page;
use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static page()
 * @method static static school()
 * @method static static coach()
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class EntityRendererType extends Enum implements LocalizedEnum
{
    const page = Page::class;
    const school = School::class;
    const coach = Coach::class;
}
