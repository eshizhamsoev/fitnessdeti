<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

class TrainingGroupType extends Enum
{
    public const GYMNASTIC = 'gymnastic';
    public const ACROBATIC = 'acrobatic';
    public const PERSONAL_ONE_HOUR = 'personal_one_hour';
    public const PERSONAL_ONE_AND_HALF_HOUR = 'personal_one_and_half_hour';
    public const COMMON = 'common';
}
