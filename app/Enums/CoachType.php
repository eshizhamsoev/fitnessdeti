<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static Senior()
 * @method static static Ordinary()
 */
final class CoachType extends Enum implements LocalizedEnum
{
    const Senior = 'senior';
    const Ordinary = 'ordinary';
}
