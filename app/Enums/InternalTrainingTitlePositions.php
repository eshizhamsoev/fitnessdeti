<?php

namespace App\Enums;

use BenSampo\Enum\Enum;



/**
 * @method static static LEFT()
 * @method static static CENTER()
 */
final class InternalTrainingTitlePositions extends Enum
{
    const LEFT =   'left';
    const CENTER =   'center';
}
