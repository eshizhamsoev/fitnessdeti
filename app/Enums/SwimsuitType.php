<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static BUY()
 * @method static static TAILORING()
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SwimsuitType extends Enum implements LocalizedEnum
{
    const BUY = 'buy';
    const TAILORING =   'tailoring';
}
