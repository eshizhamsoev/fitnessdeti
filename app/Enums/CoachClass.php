<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static Unknown()
 * @method static static KMS()
 * @method static static MS()
 * @method static static ZMS()
 * @method static static MSMK()
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class CoachClass extends Enum implements LocalizedEnum
{
    const Unknown = 'unknown';
    const KMS = 'kms';
    const MS = 'ms';
    const ZMS = 'zms';
    const MSMK = 'msmk';
}
