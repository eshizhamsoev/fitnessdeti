<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class InfoBannerSides extends Enum
{
    const LEFT = 'left';
    const RIGHT = 'right';
}
