<?php

namespace App\Enums\Website\Forms;

use BenSampo\Enum\Enum;

/**
 * @method static static TEXT()
 * @method static static NUMBER()
 */
class TextFieldType extends Enum
{
    public const TEXT = 'text';
    public const NUMBER = 'number';
    public const TEL = 'tel';
}
