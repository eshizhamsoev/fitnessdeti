<?php

namespace App\Enums\Website\Kpi;

use BenSampo\Enum\Enum;

/**
 * @method static static PURPLE()
 * @method static static RED()
 * @method static static GREEN()
 */
class KpiColor extends Enum
{
    public const PURPLE = 'purple';
    public const RED = 'red';
    public const GREEN = 'green';
}
