<?php

namespace App\Enums\Website\Kpi;

use BenSampo\Enum\Enum;

class KpiItemSize extends Enum
{
    const SMALL = 'small';
}
