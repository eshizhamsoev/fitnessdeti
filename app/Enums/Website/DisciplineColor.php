<?php

namespace App\Enums\Website;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static PINK()
 * @method static static BLUE()
 */
class DisciplineColor extends Enum implements LocalizedEnum
{
    public const PINK = 'pink';
    public const BLUE = 'blue';
}
