<?php

namespace App\Enums\Website;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;


final class ButtonType extends Enum implements LocalizedEnum
{
    const RedFill = 'btn_type_accent';
    const GreenFill = 'btn_type_primary';
    const RedOutline = 'btn_type_accent-secondary';
}
