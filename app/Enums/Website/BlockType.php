<?php

namespace App\Enums\Website;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static KPI()
 * @method static static FEDERATION()
 * @method static static SCHOOL_LIST()
 * @method static static FAQ()
 */
class BlockType extends Enum implements LocalizedEnum
{
    public const KPI = 'kpi';
    public const FEDERATION = 'federation';
    public const SCHOOL_LIST = 'school_list';
    public const FAQ = 'faq';
    public const GALLERY = 'gallery';
    public const WIDE_BANNER = 'wide_banner';
    public const MEDAL = 'medal';
    public const TRAINING_GROUP = 'training-group';
    public const TRAINING_GROUP_WITH_SWITCH = 'training-group-with-switch';
    public const HTML_CODE = 'html-code';
    public const COACHES = 'coaches';
    public const SENIOR_COACHES = 'senior-coaches';
    public const TIMETABLE = 'timetable';
    public const MARKDOWN = 'markdown';
    public const BREADCRUMBS = 'breadcrumbs';
    public const INFO_BANNER = 'info-banner';
    public const BLOG = 'blog';
    public const SENIOR_TRAINERS_PROPERTIES = 'senior-trainers-properties';
    public const INTERNAL_TRAINING = 'internal-training';
    public const COACHING_STAFF = 'coaching-staff';
    public const CERTIFIED_COACHES = 'certified-coaches';
    public const REVIEWS_BLOCK = 'reviews-block';
    public const REVIEWS_SLIDER = 'reviews-slider';
    public const CAMPS = 'camps';
    public const PHOTSHOOT = 'photoshoot';
    public const MASTER_CLASSES = 'master-classes';
    public const CALENDAR = 'calendar';
    public const STUDENTS_BLOCK = 'students-block';
    public const TEAM = 'team';
    public const VACANCY_FORM = 'vacancy-form';
    public const MAIN_BANNER = 'main-banner';
    public const PURPOSE_BANNER = 'purpose-banner';
}
