<?php

namespace App\Enums\Website;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;


final class GalleryType extends Enum implements LocalizedEnum
{
    const OUR_SERVICES = 'our-services';
    const THREE_CARD = 'three-card';
    const ADDITIONAL_SERVICE_CAROUSEL = 'additional-service-carousel';
    const CLASSES = 'classes';
    const SERVICES = 'services';
    const ABOUT_COMPANY = 'about-company';
}
