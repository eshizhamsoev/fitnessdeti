<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

/**
 * @method static static DANCER()
 * @method static static GYMNAST()
 * @method static static MEDAL()
 * @method static static RIBBON()
 * @method static static CHILDREN()
 * @method static static LABEL_CERTIFICATE()
 * @method static static TELEGRAM()
 * @method static static WHATSAPP()
 * @method static static BUILDING()
 * @method static static METRO()
 * @method static static ARROW()
 * @method static static ARROW_RIGHT()
 * @method static static TROPHY_COLOR()
 * @method static static THUMBS_UP()
 * @method static static CERTIFICATE_COLOR()
 * @method static static FRIEND()
 * @method static static MEDICAL_INSURANCE()
 * @method static static TROPHY_2()
 * @method static static PLAY()
 * @method static static WATCH_UP()
 */
class SvgIconName extends Enum
{
    public const DANCER = 'dancer';
    public const GYMNAST = 'gymnast';

    public const MEDAL = 'medal';
    public const RIBBON = 'ribbon';
    public const CHILDREN = 'children';

    public const LABEL_CERTIFICATE = 'label-certificate';
    public const LABEL_CALENDAR = 'label-calendar';
    public const LABEL_RANK = 'label-rank';

    public const TELEPHONE = 'telephone';
    public const EMAIL = 'email';
    public const TELEGRAM = 'telegram';
    public const WHATSAPP = 'whatsapp';

    public const YOUTUBE = 'youtube';
    public const INSTAGRAM = 'instagram';
    public const FACEBOOK = 'facebook';
    public const VK = 'vk';
    public const TIKTOK = 'tik-tok';

    public const BUILDING = 'building';
    public const METRO = 'metro';

    public const ARROW = 'arrow';
    public const ARROW_RIGHT = 'arrow-right';


    public const CALENDAR = 'calendar';
    public const ARTISTIC_GYMNAST = 'artistic-gymnast';
    public const GROUP = 'group';

    public const NAVIGATION = 'navigation';
    public const PIN = 'pin';
    public const PLACEHOLDER = 'placeholder';
    public const EDIT = 'edit';
    public const GRAPH = 'graph';
    public const PLAY = 'play';

    public const TROPHY_COLOR = 'trophy-color';
    public const THUMBS_UP = 'thumbs-up';
    public const CERTIFICATE_COLOR = 'certificate-color';

    public const FRIEND = 'friend';
    public const MEDICAL_INSURANCE = 'medical-insurance';
    public const TROPHY_2 = 'trophy-2';

    public const WATCH_UP = 'watch-up';
}
