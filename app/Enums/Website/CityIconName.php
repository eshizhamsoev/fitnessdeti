<?php

namespace App\Enums\Website;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class CityIconName extends Enum implements LocalizedEnum
{
    const EKB = 'ekb';
    const MSK = 'msk';
    const NN = 'nn';
    const SAMARA = 'samara';
    const SOCHI = 'sochi';
    const SPB = 'spb';
    const TUMEN = 'tumen';
}
