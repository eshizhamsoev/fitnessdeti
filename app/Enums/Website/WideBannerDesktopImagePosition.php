<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

class WideBannerDesktopImagePosition extends Enum
{
    const POSITION_LEFT = 'left';
    const POSITION_CENTER = 'center';
}
