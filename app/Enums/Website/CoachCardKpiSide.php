<?php

namespace App\Enums\Website;

use BenSampo\Enum\Enum;

class CoachCardKpiSide extends Enum
{
    const LEFT = 'left';
    const RIGHT = 'right';
}
