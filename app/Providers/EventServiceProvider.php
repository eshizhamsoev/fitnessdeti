<?php

namespace App\Providers;

use App\Domain\Leads\Events\LeadCreated;
use App\Domain\Leads\Events\VacancyApplicationCreated;
use App\Listeners\NewLeadListener;
use App\Listeners\NewVacancyApplicationListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        LeadCreated::class => [
            NewLeadListener::class,
        ],
        VacancyApplicationCreated::class => [
            NewVacancyApplicationListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
