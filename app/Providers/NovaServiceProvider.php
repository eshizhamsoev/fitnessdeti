<?php

namespace App\Providers;

use App\Models\Data\City;
use App\Models\Website\Page;
use App\Services\SettingsFields;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use NovaItemsField\Items;
use OptimistDigital\NovaSettings\NovaSettings;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        NovaSettings::addSettingsFields([
            Select::make('School Page', SettingsFields::SCHOOL)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('Coach Page', SettingsFields::COACH)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('Blog Page', SettingsFields::BLOG)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('School template page', SettingsFields::SCHOOL_TEMPLATE_PAGE)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('Reviews Page', SettingsFields::REVIEWS)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),

            Textarea::make('Code in <head>', SettingsFields::CODE_IN_HEAD),
            Textarea::make('Code after <body>', SettingsFields::CODE_IN_BODY_START),
            Textarea::make('Code before </body>', SettingsFields::CODE_IN_BODY_END),
        ], [], 'System settings');

        NovaSettings::addSettingsFields([
            Select::make('Privacy Page', SettingsFields::PRIVACY_PAGE)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('Safety rules', SettingsFields::SAFETY_RULES_PAGE)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('Offer and Acceptance', SettingsFields::OFFER_AND_ACCEPTANCE)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('LLC', SettingsFields::LLC)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
            Select::make('User Agreement', SettingsFields::USER_AGREEMENT)
                ->options(Page::all()->pluck('name', 'id'))
                ->nullable(),
        ], [], 'Footer pages');

        NovaSettings::addSettingsFields([
            Text::make('Email', SettingsFields::EMAIL),
            Text::make('Instagram', SettingsFields::SOCIAL_INSTAGRAM),
            Text::make('Youtube', SettingsFields::SOCIAL_YOUTUBE),
            Text::make('Facebook', SettingsFields::SOCIAL_FACEBOOK),
            Text::make('Vk', SettingsFields::SOCIAL_VK),
            Text::make('TikTok', SettingsFields::SOCIAL_TIKTOK),
        ], [], 'Social and contacts');

        NovaSettings::addSettingsFields([
            Items::make('Notify about new leads', SettingsFields::EMAIL_NOTIFICATION_LEAD)->rules([
                SettingsFields::EMAIL_NOTIFICATION_LEAD . '.*' => 'email'
            ]),
            Items::make('Notify about new vacancy applications', SettingsFields::VACANCY_APPLICATION_EMAIL)->rules([
                SettingsFields::VACANCY_APPLICATION_EMAIL . '.*' => 'email'
            ])
        ], [
            SettingsFields::EMAIL_NOTIFICATION_LEAD => 'array',
            SettingsFields::VACANCY_APPLICATION_EMAIL => 'array'
        ], 'Notification settings');

        NovaSettings::addSettingsFields([
            Select::make('City', SettingsFields::DEFAULT_CITY)
            ->options(City::all()->pluck('name', 'id'))
            ->nullable(),
        ], [], 'Default City');
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {

            return $user->is_admin === 1;
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new Help,
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            new NovaSettings
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
