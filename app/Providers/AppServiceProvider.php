<?php

namespace App\Providers;

use App\Models\Website\BlogPost;
use Illuminate\Support\Facades\URL;
use App\Enums\EntityType;
use App\Models\Data\Coach;
use App\Models\Data\School;
use App\Models\Website\Page;
use App\Services\CurrentCityRepository;
use App\Services\CurrentUrlRepository;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Upline\BemClassname\BemFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CurrentCityRepository::class);
        $this->app->singleton(CurrentUrlRepository::class);

        $this->app->singleton(BemFactory::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            EntityType::SCHOOL => School::class,
            EntityType::COACH => Coach::class,
            EntityType::PAGE => Page::class,
            EntityType::BLOG_POST => BlogPost::class,
        ]);
    }
}
