<?php

namespace App\Nova\Data;

use App\Nova\Data\Metro\MetroStation;
use App\Nova\Resource;
use App\Nova\Website\Url;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class School extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Data\School::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Boolean::make('Is Enabled?', 'is_enabled')->default(true)->required(),
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make('City', 'city', City::class)->sortable(),
            MorphOne::make('Url', 'url', Url::class),
            BelongsToMany::make('MetroStation', 'metroStations', MetroStation::class),
            Text::make('Name')->sortable()->required(),
            Text::make('Short Name')->sortable()->nullable(),
            Text::make('Phone'),
            Text::make('Whatsapp'),
            Text::make('Telegram'),
            Textarea::make('Work Hours', 'work_hours'),
            Textarea::make('How To Get', 'how_to_get'),
            Textarea::make('Address'),
            Number::make('Latitude')->step(1e-8),
            Number::make('Longitude')->step(1e-8),
            BelongsToMany::make('Coaches'),

            Images::make('Gallery', \App\Models\Data\School::IMAGE_COLLECTION_GALLERY)
                ->conversionOnIndexView('thumb'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
