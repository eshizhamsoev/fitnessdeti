<?php

namespace App\Nova\Data;

use App\Models\Data\Student as StudentModel;
use App\Nova\Resource;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class Student extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = StudentModel::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'fullName';
    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Boolean::make('Is Enabled?', 'is_enabled')->default(true)->required(),
            Boolean::make('Is In Team?', 'is_in_team')->default(false)->required(),
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Lastname'))->sortable()->required(),
            Text::make(__('Firstname'))->sortable()->required(),
            Text::make(__('Patronymic'))->sortable(),
            BelongsTo::make('Discipline', 'discipline', Discipline::class)->sortable()->nullable(),
            Text::make(__('Rank')),
            Text::make(__('Title')),
            Images::make('Main image', StudentModel::IMAGE_COLLECTION_MAIN)
                ->conversionOnIndexView('main'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
