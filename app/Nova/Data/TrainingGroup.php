<?php

namespace App\Nova\Data;

use App\Enums\TrainingGroupType;
use App\Enums\TimeUnitForPrice;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use SimpleSquid\Nova\Fields\Enum\Enum;

class TrainingGroup extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Data\TrainingGroup::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Enum::make('Type')->attach(TrainingGroupType::class),
            Text::make('Name')->sortable()->required(),
            Textarea::make('Description')->required(),
            Number::make('Base Price')->required(),
            Enum::make('Time unit for price')->attach(TimeUnitForPrice::class),

            Text::make('Age')->nullable(),
            Text::make('Capacity')->nullable(),
            Text::make('Price per hour')->nullable(),
            Textarea::make('Coaches')->nullable(),

            Number::make('Priority')->default(0),

            BelongsTo::make('City', 'city', City::class)
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
