<?php

namespace App\Nova\Data;

use App\Enums\CoachClass;
use App\Enums\CoachType;
use App\Nova\Resource;
use App\Nova\Website\Url;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\KeyValue;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Media24si\NovaYoutubeField\Youtube;
use SimpleSquid\Nova\Fields\Enum\Enum;

class Coach extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Data\Coach::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Boolean::make('Is Enabled?', 'is_enabled')->default(true)->required(),
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->sortable()->required(),
            MorphOne::make('Url', 'url', Url::class),
            Textarea::make('Description')->nullable(),
            BelongsTo::make('Discipline', 'discipline', Discipline::class)->sortable()->required(),
            BelongsTo::make('City', 'city', City::class)->sortable()->nullable(),
            Youtube::make('Youtube', 'youtube_code')->nullable(),
            Number::make('Medals', 'medal_count')->nullable(),
            Number::make('Children', 'child_count')->nullable(),
            Number::make('Sport Categories', 'sport_category_count')->nullable(),
            Enum::make('Coach Type')->attach(CoachType::class),
            Enum::make('Coach Class')->attach(CoachClass::class),
            KeyValue::make('Resume')->rules('json'),

            Images::make('Main image', \App\Models\Data\Coach::IMAGE_COLLECTION_MAIN)
                ->conversionOnIndexView('main'),
            Images::make('Gallery', \App\Models\Data\Coach::IMAGE_COLLECTION_GALLERY)
                ->conversionOnIndexView('thumb'),

            BelongsToMany::make('Schools'),
            BelongsToMany::make('Students'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
