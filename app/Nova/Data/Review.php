<?php

namespace App\Nova\Data;

use App\Models\Data\Review as ReviewModel;
use App\Nova\Resource;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Media24si\NovaYoutubeField\Youtube;

class Review extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Data\Review::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Data';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->required()->sortable(),
            Textarea::make('Announce')->required(),
            Images::make('Avatar', ReviewModel::IMAGE_COLLECTION_AVATAR)
                ->conversionOnIndexView(ReviewModel::IMAGE_CONVERSION_AVATAR),
            Images::make('Review Image', ReviewModel::IMAGE_COLLECTION_REVIEW)
                ->conversionOnIndexView(ReviewModel::IMAGE_CONVERSION_REVIEW),
            Textarea::make('Text')->required(),
            Youtube::make('Youtube', 'youtube_code')->nullable(),
            BelongsTo::make('Discipline', 'discipline', Discipline::class)->nullable()->sortable(),
            BelongsTo::make('Coach', 'coach', Coach::class)->nullable()->sortable(),
            BelongsTo::make('School', 'school', School::class)->nullable()->sortable(),
            BelongsTo::make('City', 'city', City::class)->nullable()->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
