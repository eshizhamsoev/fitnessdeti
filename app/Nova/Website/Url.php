<?php

namespace App\Nova\Website;

use App\Nova\Data\City;
use App\Nova\Data\Coach;
use App\Nova\Data\School;
use App\Nova\Resource;
use App\Services\Transliterator\Transliterator;
use App\Services\UrlGenerator\UrlGenerator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use NovaAttachMany\AttachMany;

class Url extends Resource
{
    private const RELATED_RESOURCES = [School::class, Coach::class, Page::class, BlogPost::class];
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Website\Url::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'url';
    public static $group = 'Website';
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'url',
    ];


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Url')->default(function () use($request) {
                $urlGenerator = resolve(UrlGenerator::class);
                $parentModel = $request->findParentModel();
                if ($parentModel) {
                    return $urlGenerator->makeUrl($parentModel);
                }
                return '';
            })
                ->creationRules('unique:urls,url')
                ->rules('unique:urls,url,{{resourceId}}'),
            MorphTo::make('Entity')->types(self::RELATED_RESOURCES),
            AttachMany::make('City', 'cities', City::class),
            BelongsToMany::make('City', 'cities', City::class),
            HasMany::make('Seo Data', 'seoData', SeoData::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public function __construct($resource)
    {
        parent::__construct($resource);
    }
}
