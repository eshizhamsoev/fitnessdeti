<?php

namespace App\Nova\Website;

use App\ConfigurableBlockSettings\SettingResolver;
use App\Enums\Website\BlockType;
use App\Models\Website\ConfigurableBlock;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use OptimistDigital\MultiselectField\Multiselect;
use OptimistDigital\NovaSortable\Traits\HasSortableManyToManyRows;
use R64\NovaFields\Boolean;
use R64\NovaFields\JSON;
use R64\NovaFields\Text;
use SimpleSquid\Nova\Fields\Enum\Enum;
use function __;
use function app;

class ConfigurableNovaBlock extends Resource
{
    use HasSortableManyToManyRows;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = ConfigurableBlock::class;
    public static $perPageViaRelationship = 100;
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static $group = 'Website';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $resolver = app(SettingResolver::class);
        $fields = [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Name')->sortable(),
        ];
        if ($this->model()->type) {
            $settings = $resolver->resolve($this->model()->type);
            $fields[] = JSON::make('Settings', $settings->getSettings())->flatten();
            $fields[] = Enum::make('Type')->attach(BlockType::class)->exceptOnForms();
        } else {
            $fields[] = Enum::make('Type')->attach(BlockType::class);
        }


        $fields[] = Boolean::make('Is grey', 'pivot.is_grey')
            ->onlyOnIndex()
            ->showOnIndex(fn() => isset($this->pivot))
            ->displayUsing(fn() => isset($this->pivot) ? $this->pivot->is_grey : null);

        $fields[] = Multiselect::make('Cities', 'pivot.cities')
            ->onlyOnIndex()
            ->showOnIndex(fn() => isset($this->pivot))
            ->displayUsing(fn() => isset($this->pivot) ? $this->pivot->cities : null);

        $fields[] = BelongsToMany::make('Pages');
        $fields[] = Text::make('pages', fn() => $this->pages->count())->onlyOnIndex();
        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
