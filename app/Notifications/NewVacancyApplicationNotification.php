<?php

namespace App\Notifications;

use App\Models\Data\VacancyApplication;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewVacancyApplicationNotification extends Notification
{

    private VacancyApplication $vacancyApplication;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(VacancyApplication $vacancyApplication)
    {
        $this->vacancyApplication = $vacancyApplication;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Новая заявка на вакансию: №' . $this->vacancyApplication->id)
            ->view('mail.admin-vacancy', ['application' => $this->vacancyApplication]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
