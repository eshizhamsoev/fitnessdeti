<?php

namespace App\Notifications;

use App\Domain\Leads\Models\Lead;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NewLeadNotification extends Notification
{
    private Lead $lead;

    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    public function via($notifiable): array
    {
        return ['mail'];
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Новая заявка на сайте: №' . $this->lead->id)
            ->view('mail.admin-lead', ['lead' => $this->lead]);
    }

    public function toArray($notifiable): array
    {
        return [
            //
        ];
    }
}
