<?php

namespace App\Models\Data;

use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\SeoDataService\WithTitle;
use App\Services\UrlGenerator\Urlable;
use App\Models\Data\Metro\MetroStation;
use App\Models\Website\Page;
use App\Models\Website\Url;
use App\Services\SettingsFields;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class School extends Model implements Urlable, HasMedia, BreadcrumbsVisible, WithTitle
{
    use HasFactory;
    use InteractsWithMedia;

    public const IMAGE_COLLECTION_GALLERY = 'gallery';

    public function metroStations()
    {
        return $this->belongsToMany(MetroStation::class, 'school_to_metro', 'school_id', 'station_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function coaches()
    {
        return $this->belongsToMany(Coach::class, 'coach_to_school');
    }

    public function getParent(): ?Page
    {
        return Page::find(nova_get_setting(SettingsFields::SCHOOL));
    }

    public function getParentUrl(): ?Url
    {
        $parent = $this->getParent();
//        dd($parent);
        return $parent !== null ? $parent->url : null;
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->name;
    }

    public function getStringForUrl(): string
    {
        return $this->name;
    }

    public function getTitle(): string
    {
        return $this->name;
    }

    public function scopeByCity($query, City $city)
    {
        return $query->where('city_id', '=', $city->id);
    }

    public function getMapRouteLinkAttribute()
    {
        if (!$this->latitude || !$this->longitude) {
            return null;
        }
        return "https://yandex.ru/maps/?rtext=~{$this->latitude},{$this->longitude}&rtt=auto";
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 294 * 2, 240 * 2)
            ->optimize()
            ->withResponsiveImages()
            ->performOnCollections(self::IMAGE_COLLECTION_GALLERY);



        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::IMAGE_COLLECTION_GALLERY);
    }
}
