<?php

namespace App\Models\Data;

use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Review extends Model implements HasMedia
{
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    public const IMAGE_COLLECTION_AVATAR = 'avatar';
    public const IMAGE_CONVERSION_AVATAR = 'avatar';

    public const IMAGE_COLLECTION_REVIEW = 'review';
    public const IMAGE_CONVERSION_REVIEW = 'review';


    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function discipline()
    {
        return $this->belongsTo(Discipline::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {

        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_AVATAR)
            ->performOnCollections(self::IMAGE_COLLECTION_AVATAR);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_AVATAR, 64, 64);

        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_REVIEW)
            ->performOnCollections(self::IMAGE_COLLECTION_REVIEW);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_REVIEW, 500, 300);
        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_AVATAR)
            ->singleFile();
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_REVIEW)
            ->singleFile();
    }
}
