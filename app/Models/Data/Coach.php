<?php

namespace App\Models\Data;

use App\Enums\CoachClass;
use App\Enums\CoachType;
use App\Enums\Website\Kpi\KpiColor;
use App\Enums\Website\SvgIconName;
use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\SeoDataService\WithTitle;
use App\Services\UrlGenerator\Urlable;
use App\Models\Website\Page;
use App\Models\Website\Url;
use App\Services\SettingsFields;
use App\View\Data\Common\KpiItemData;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Coach extends Model implements Urlable, HasMedia, BreadcrumbsVisible, WithTitle
{
    use HasFactory;
    use InteractsWithMedia;

    public const IMAGE_COLLECTION_MAIN = 'main';
    public const IMAGE_COLLECTION_GALLERY = 'gallery';

    protected $casts = [
        'coach_class' => CoachClass::class,
        'coach_type' => CoachType::class,
        'resume' => 'json'
    ];

    public function parent()
    {
        return $this->belongsTo(Coach::class);
    }

    public function schools()
    {
        return $this->belongsToMany(School::class, 'coach_to_school');
    }

    public function discipline()
    {
        return $this->belongsTo(Discipline::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'coach_to_student');
    }

    public function getStringForUrl(): string
    {
        return $this->name;
    }

    public function getParent(): ?Page
    {
        return Page::find(nova_get_setting(SettingsFields::COACH));
    }

    public function getParentUrl(): ?Url
    {
        $parent = $this->getParent();
        return $parent !== null ? $parent->url : null;
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->name;
    }

    public function getTitle(): string
    {
        return $this->name;
    }

    public function scopeByType($query, CoachType $coachType)
    {
        return $query->where('coach_type', $coachType->value);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 294 * 2, 240 * 2)
            ->optimize()
            ->withResponsiveImages()
            ->performOnCollections(self::IMAGE_COLLECTION_GALLERY);

        $this->addMediaConversion('main')
            ->fit(Manipulations::FIT_CROP, 376 * 2, 500 * 2)
            ->optimize()->withResponsiveImages()
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);

        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN)
            ->singleFile();
        $this->addMediaCollection(self::IMAGE_COLLECTION_GALLERY);
    }

    public function getKpi()
    {

        $kpiItems = [];
        if ($this->medal_count) {
            $kpiItems[] = new KpiItemData(
                KpiColor::fromValue(KpiColor::RED),
                SvgIconName::MEDAL(),
                $this->medal_count,
                'Медалей'
            );
        }
        if ($this->sport_category_count) {
            $kpiItems[] = new KpiItemData(
                KpiColor::fromValue(KpiColor::GREEN),
                SvgIconName::RIBBON(),
                $this->sport_category_count,
                'Разрядов'
            );
        }
        if ($this->child_count) {
            $kpiItems[] = new KpiItemData(
                KpiColor::fromValue(KpiColor::PURPLE),
                SvgIconName::CHILDREN(),
                $this->child_count,
                'Детей'
            );
        }
        return $kpiItems;

    }
}
