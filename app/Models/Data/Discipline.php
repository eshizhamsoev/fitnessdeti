<?php

namespace App\Models\Data;

use App\Enums\Website\DisciplineColor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    use HasFactory;

    protected $casts = [
        'color' => DisciplineColor::class
    ];
}
