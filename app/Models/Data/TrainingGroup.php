<?php

namespace App\Models\Data;

use App\Enums\TimeUnitForPrice;
use App\Enums\TrainingGroupType;
use Illuminate\Database\Eloquent\Model;

class TrainingGroup extends Model
{
    protected $casts = [
        'time_unit_for_price' => TimeUnitForPrice::class
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function scopeByType($query, TrainingGroupType $trainingGroupType)
    {
        return $query->where('type', '=', $trainingGroupType->value);
    }

    public function scopeByCity($query, City $city)
    {
        return $query->where('city_id', '=', $city->id);
    }
}
