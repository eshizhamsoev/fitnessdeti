<?php

namespace App\Models\Data\Metro;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetroStation extends Model
{
    use HasFactory;

    public function line() {
        return $this->belongsTo(MetroLine::class);
    }
}
