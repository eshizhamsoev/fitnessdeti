<?php

namespace App\Models\Data\Metro;

use App\Models\Data\City;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetroLine extends Model
{
    use HasFactory;

    public function city() {
        return $this->belongsTo(City::class);
    }
}
