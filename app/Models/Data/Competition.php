<?php

namespace App\Models\Data;

use App\Enums\CompetitionType;
use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Competition extends Model implements HasMedia
{
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    const IMAGE_COLLECTION_MAIN = 'main';
    const IMAGE_CONVERSION_MAIN = 'main';

    protected $casts = [
        'date_start' => 'date',
        'date_end' => 'date',
        'competition_type' => CompetitionType::class,
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_MAIN)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN,343, 236);

        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN)
            ->singleFile();
    }
}
