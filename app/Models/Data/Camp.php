<?php

namespace App\Models\Data;

use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Camp extends Model implements HasMedia
{
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    const IMAGE_COLLECTION_MAIN = 'main';
    const IMAGE_CONVERSION_MAIN = 'main';

    protected $casts = [
        'date_from' => 'date',
        'date_to' => 'date'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_MAIN)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN,272, 235);

        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN)
            ->singleFile();
    }

}
