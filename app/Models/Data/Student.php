<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Student extends Model implements HasMedia
{
    use InteractsWithMedia;

    const IMAGE_COLLECTION_MAIN = 'main';

    public function discipline()
    {
        return $this->belongsTo(Discipline::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('main')
            ->fit(Manipulations::FIT_CROP, 376 * 2, 435 * 2)
            ->optimize()->withResponsiveImages()
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);

        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::IMAGE_COLLECTION_MAIN)->singleFile();
    }

    public function getFullNameAttribute(): string
    {
        return sprintf('%s %s %s', $this->lastname, $this->firstname, $this->patronymic);
    }
}
