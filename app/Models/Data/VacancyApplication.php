<?php

namespace App\Models\Data;

use App\Domain\Leads\Events\VacancyApplicationCreated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacancyApplication extends Model
{
    use HasFactory;

    protected $dispatchesEvents = [
        'created' => VacancyApplicationCreated::class,
    ];

    protected $casts = [
        'utm' => 'json'
    ];
}
