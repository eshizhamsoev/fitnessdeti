<?php

namespace App\Models\Website;

use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\SeoDataService\WithTitle;
use App\Services\SettingsFields;
use App\Services\UrlGenerator\Urlable;
use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class BlogPost extends Model implements Urlable, HasMedia, BreadcrumbsVisible, WithTitle
{
    use InteractsWithMedia;
    use CroppedResponsiveImage;

    public const IMAGE_COLLECTION_MAIN = 'main';
    public const IMAGE_CONVERSION_MAIN = 'main';
    protected $dates = ['date'];

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function getStringForUrl(): string
    {
        return $this->name;
    }

    public function getParent(): ?Page
    {
        return Page::find(nova_get_setting(SettingsFields::BLOG));
    }

    public function getParentUrl(): ?Url
    {
        $parent = $this->getParent();
        return $parent !== null ? $parent->url : null;
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->name;
    }

    public function getTitle(): string
    {
        return $this->name;
    }

    public function registerMediaConversions(Media $media = null): void
    {

        $conversion = $this
            ->addMediaConversion(self::IMAGE_CONVERSION_MAIN)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);
        $this->responsiveCrop($conversion, $media, self::IMAGE_COLLECTION_MAIN, 504, 306);


        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(self::IMAGE_COLLECTION_MAIN)
            ->singleFile();
    }
}
