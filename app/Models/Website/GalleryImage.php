<?php

namespace App\Models\Website;

use App\Support\CroppedResponsiveImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\ImageFactory;

class GalleryImage extends Model implements HasMedia
{
    use InteractsWithMedia, CroppedResponsiveImage;

    const IMAGE_COLLECTION_MAIN = 'main';
    const IMAGE_COLLECTION_MOBILE = 'mobile';
    const IMAGE_MAIN_CONVERSION = 'main';

    use HasFactory;

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {

        $mainConversion = $this
            ->addMediaConversion(self::IMAGE_MAIN_CONVERSION)
            ->performOnCollections(self::IMAGE_COLLECTION_MAIN);

        if (
            !$media->hasGeneratedConversion(self::IMAGE_MAIN_CONVERSION)
            && $media
            && $media->model
            && $media->model->gallery
            && $media->model->gallery->width
            && $media->model->gallery->height
        ) {
            $this->responsiveCrop(
                $mainConversion,
                $media,
                self::IMAGE_COLLECTION_MAIN,
                $media->model->gallery->width,
                $media->model->gallery->height
            );
        }

        $mobileConversion = $this
            ->addMediaConversion(self::IMAGE_MAIN_CONVERSION)
            ->performOnCollections(self::IMAGE_COLLECTION_MOBILE);

        if (
            !$media->hasGeneratedConversion(self::IMAGE_MAIN_CONVERSION)
            && $media
            && $media->model
            && $media->model->gallery
            && $media->model->gallery->mobile_width
            && $media->model->gallery->mobile_height
        ) {
            $this->responsiveCrop(
                $mobileConversion,
                $media,
                self::IMAGE_COLLECTION_MOBILE,
                $media->model->gallery->mobile_width,
                $media->model->gallery->mobile_height
            );
        }

        $this->addMediaConversion('original')
            ->fit(Manipulations::FIT_MAX, 1600, 1600)
            ->optimize();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::IMAGE_COLLECTION_MAIN)->singleFile();
        $this->addMediaCollection(self::IMAGE_COLLECTION_MOBILE)->singleFile();
    }

}
