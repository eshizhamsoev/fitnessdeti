<?php

namespace App\Models\Website;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class PageBlock extends Pivot implements Sortable
{
    use SortableTrait;

    public $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'page_blocks';

    public $sortable = [
        'order_column_name' => 'sort_order',
        'sort_when_creating' => true,
    ];

    public function buildSortQuery()
    {
        return static::query()
            ->where('page_id', $this->page_id);
    }
}
