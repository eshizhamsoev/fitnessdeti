<?php

namespace App\Models\Website;

use App\Services\BreadcrumbsService\BreadcrumbsVisible;
use App\Services\SeoDataService\WithTitle;
use App\Services\UrlGenerator\Urlable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model implements Urlable, BreadcrumbsVisible, WithTitle
{
    use HasFactory;

    public function parent()
    {
        return $this->belongsTo(Page::class);
    }

    public function url()
    {
        return $this->morphOne(Url::class, 'entity');
    }

    public function blocks()
    {
        return $this->belongsToMany(ConfigurableBlock::class, 'page_blocks')
            ->orderBy('page_blocks.sort_order')
            ->withPivot(['sort_order', 'is_grey', 'cities'])
            ->using(PageBlock::class);
    }

//    public function page_content()
//    {
//        return $this->hasOne(PageContent::class);
//    }

    public function getStringForUrl(): string
    {
        return $this->name;
    }

    public function getParentUrl(): ?Url
    {
        return $this->parent ? $this->parent->url : null;
    }

    public function getNameForBreadcrumbs(): string
    {
        return $this->name;
    }

    public function getTitle(): string
    {
        return $this->name;
    }
}
