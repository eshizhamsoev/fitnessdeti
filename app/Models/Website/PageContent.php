<?php

namespace App\Models\Website;

use App\Models\Data\City;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    use HasFactory;

    public function city() {
        return $this->belongsTo(City::class);
    }

    public function page() {
        return $this->belongsTo(Page::class);
    }
}
