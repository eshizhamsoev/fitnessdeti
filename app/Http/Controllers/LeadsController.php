<?php

namespace App\Http\Controllers;

use App\Domain\Leads\Services\LeadService;
use App\Http\Requests\LeadRequest;

class LeadsController
{
    public function store(LeadRequest $request, LeadService $leadService)
    {

        $leadService->createLead($request->getData());
    }
}
