<?php

namespace App\Http\Controllers;

use App\Enums\CompetitionType;
use App\Enums\Website\SvgIconName;
use App\Models\Data\Competition;
use App\View\Data\Content\Competition\CompetitionModalItem;

class CompetitionsController extends Controller
{


    public function index($date)
    {
        $competitions = Competition::whereDate('date_start', $date)->get()->map(function (Competition $competition) {
            return new CompetitionModalItem($competition);
        });


        return response()->json([
            'html' => view('components.content.date-competition-events', ['competitions' => $competitions])->render()
        ]);
    }
}
