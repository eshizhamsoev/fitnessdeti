<?php

namespace App\Http\Controllers;

use App\Domain\Leads\Services\VacancyApplicationService;
use Illuminate\Http\Request;

class VacancyApplicationsController extends Controller
{
    public function send(Request $request)
    {
        VacancyApplicationService::createApplication($request);
    }
}
