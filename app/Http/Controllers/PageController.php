<?php

namespace App\Http\Controllers;

use App\EntityRenderers\BlogPostRenderer;
use App\EntityRenderers\CoachRenderer;
use App\EntityRenderers\PageRenderer;
use App\EntityRenderers\SchoolRenderer;
use App\Enums\EntityType;
use App\Models\Website\Url;
use App\Services\CurrentCityRepository;
use App\Services\CurrentUrlRepository;
use App\Services\SeoDataService\SeoDataService;

class PageController extends Controller
{

    public $renderers = [
        EntityType::SCHOOL => SchoolRenderer::class,
        EntityType::COACH => CoachRenderer::class,
        EntityType::PAGE => PageRenderer::class,
        EntityType::BLOG_POST => BlogPostRenderer::class,
    ];

    public function index(
        CurrentCityRepository $currentCityRepository,
        CurrentUrlRepository $currentUrlRepository,
        SeoDataService $seoService,
        ?string $fullUrl = null
    ) {
        $url = Url::where('url', '=', '/' . $fullUrl)->whereHas('cities',
            function ($query) use ($currentCityRepository) {
                return $query->where('id', $currentCityRepository->getCurrentCity()->id);
            })->firstOrFail();

        if (!$url->entity) {
            abort(404);
        }

        $currentUrlRepository->init($url);
        $renderer = new $this->renderers[$url->entity_type];
        return view('page',
            [
                'content' => $renderer->render($url->entity),
                'seoData' => $seoService->getSeoData($url)
            ]
        );
    }
}
