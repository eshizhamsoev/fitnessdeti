<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetUtmData
{

    public const UTM_FIELDS = [
        "utm_term",
        "utm_medium",
        "utm_source",
        "utm_content",
        "utm_campaign",
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        foreach (self::UTM_FIELDS as $utmField) {
            if ($request->input($utmField)) {
                session([$utmField => $request->input($utmField)]);
            }
        }
        return $next($request);
    }
}
