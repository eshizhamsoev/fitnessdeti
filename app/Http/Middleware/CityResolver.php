<?php

namespace App\Http\Middleware;

use App\Models\Data\City;
use App\Services\CurrentCityRepository;
use App\Services\SettingsFields;
use Closure;
use Illuminate\Http\Request;

class CityResolver
{
    private CurrentCityRepository $cityRepository;

    public function __construct(CurrentCityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$request->getHost()) {
            abort(404);
        }

        $city = City::where('domain', '=', $request->getHost())->where('is_enabled', 1)->first();

        if ($city) {
            $this->cityRepository->init($city);
            return $next($request);
        }

        $defaultCity = City::find(nova_get_setting(SettingsFields::DEFAULT_CITY)) ?: City::enabled()->first();

        $this->cityRepository->init($defaultCity);

        abort(404);
    }
}
