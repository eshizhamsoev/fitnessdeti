<?php

namespace App\Http\Requests;

use App\Domain\Leads\Data\LeadData;
use App\Domain\Leads\Data\UTMData;
use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    const UTM_MEDIUM = 'utm_medium';
    const UTM_CAMPAIGN = 'utm_campaign';
    const UTM_SOURCE = 'utm_source';
    const UTM_TERM = 'utm_term';
    const UTM_CONTENT = 'utm_content';

    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string'
            ],
            'phone' => [
                'required',
                'string',
                'min:10'
            ],
            'agreement' => [
                'required',
                'in:1'
            ],
            'source' => [
                'nullable',
                'string'
            ],
            self::UTM_MEDIUM => [
                'nullable',
                'string'
            ],
            self::UTM_CAMPAIGN => [
                'nullable',
                'string'
            ],
            self::UTM_SOURCE => [
                'nullable',
                'string'
            ],
            self::UTM_TERM => [
                'nullable',
                'string'
            ],
            self::UTM_CONTENT => [
                'nullable',
                'string'
            ]
        ];
    }

    public function getData()
    {
        $data = $this->validated();

        return new LeadData(
            $data['name'],
            $data['phone'],
            $data['source'] ?? '',
            url()->previous(),
            new UTMData(
                $this->session()->get(self::UTM_MEDIUM),
                $this->session()->get(self::UTM_CAMPAIGN),
                $this->session()->get(self::UTM_SOURCE),
                $this->session()->get(self::UTM_TERM),
                $this->session()->get(self::UTM_CONTENT),
            )
        );
    }

    public function authorize(): bool
    {
        return true;
    }
}
