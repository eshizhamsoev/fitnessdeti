<?php


namespace App\EntityRenderers;


use App\Models\Website\Page;
use App\Services\CurrentCityRepository;

class PageRenderer
{
    public function render(Page $entity) {

        $currentCity = app(CurrentCityRepository::class)->getCurrentCity()->id;

        $filteredBlocks = $entity->blocks->filter(function ($block) use ($currentCity) {

            if ($block->pivot->cities) {
                return in_array($currentCity, json_decode($block->pivot->cities));
            }
            return true;
        });

        return view('entities.page', [
            'page' => $entity,
            'blocks' => $filteredBlocks
        ]);
    }
}
