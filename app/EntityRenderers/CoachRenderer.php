<?php


namespace App\EntityRenderers;


use App\Models\Data\Coach;
use App\View\Data\Gallery\GalleryData;

class CoachRenderer
{
    public function render(Coach $coach)
    {
        return view('entities.coach', [
            'coach' => $coach,
            'coachWorkGallery' => $this->getCoachWorkGallery($coach),
            'kpi' => $coach->getKpi()
        ]);
    }

    private function getCoachWorkGallery(Coach $coach): ?GalleryData
    {
        $images = $coach->getMedia(Coach::IMAGE_COLLECTION_GALLERY);
        if ($images->isEmpty()) {
            return null;
        }
        $gallery = new GalleryData('Тренер в работе');
        foreach ($images as $image) {
            $gallery->addPhoto($image);
        }
        return $gallery;
    }
}
