<?php


namespace App\EntityRenderers;


use App\Models\Data\School;
use App\Models\Website\Page;
use App\Services\SettingsFields;
use App\View\Data\Gallery\GalleryData;

class SchoolRenderer
{
    public function render(School $entity)
    {
        $blocks = [];
        $templatePage = Page::find(nova_get_setting(SettingsFields::SCHOOL_TEMPLATE_PAGE));
        if ($templatePage) {
            $blocks = $templatePage->blocks->all();
        }

        return view('entities.school', [
            'school' => $entity,
            'source' => 'Школа ' . $entity->name,
            'insideGallery' => $this->getSchoolWorkGallery($entity),
            'coaches' => $this->getSchoolCoaches($entity),
            'blocks' => $blocks
        ]);
    }

    private function getSchoolWorkGallery(School $coach): ?GalleryData
    {
        $images = $coach->getMedia(School::IMAGE_COLLECTION_GALLERY);
        if ($images->isEmpty()) {
            return null;
        }
        $gallery = new GalleryData('Фото отделения/зала');
        foreach ($images as $image) {
            $gallery->addPhoto($image);
        }
        return $gallery;
    }

    private function getSchoolCoaches(School $school): ?array {
        return $school->coaches()->with(['discipline', 'media', 'url'])->get()->all();
    }
}
