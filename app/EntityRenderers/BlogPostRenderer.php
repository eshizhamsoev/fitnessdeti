<?php


namespace App\EntityRenderers;


use App\Models\Website\BlogPost;

class BlogPostRenderer
{
    public function render(BlogPost $entity) {
        return view('entities.blog-post', [
            'blogPost' => $entity
        ]);
    }
}
